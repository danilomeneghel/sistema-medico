# Agenda Cirurgica

Microsserviço de Agenda Cirurgica

## Requisitos

* Java OpenJDK 8
* Maven (Opcional)
* Postman (Opcional)
* Oracle Database 19c

## Tecnologias

* Java
* Maven
* Spring Boot
* Spring Web
* Spring Data JPA
* Validation
* Lombok

Para carregar o projeto, digite no terminal:

```
$ ./mvnw spring-boot:run
```

Aguarde carregar todo o serviço web.

Após concluído, digite o endereço abaixo no Postman para listar os dados.

OBS: É necessário antes gerar o Token para autenticar o acesso ao endpoint.

http://localhost:8081/srv/agenda-cirurgica-service/agenda-cirurgica?pageNumber=0&pageSize=20&sortBy=titulo&sortDirection=asc

## Swagger

http://localhost:8081/srv/agenda-cirurgica-service/swagger-ui.html
