package br.com.gndi.agendacirurgica;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(classes = Application.class)
@TestPropertySource(properties = {"spring.config.location = classpath:application-test.properties"})
public class ApplicationTests {

    @Test
    public void contextLoads() {
    }

}