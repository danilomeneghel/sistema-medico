package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.entity.AgendaProcedimentoEntity;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.mapper.PaginationMapper;
import br.com.gndi.agendacirurgica.model.AgendaProcedimento;
import br.com.gndi.agendacirurgica.model.errors.AgendaCirurgicaErrors;
import br.com.gndi.agendacirurgica.model.errors.AgendaProcedimentoErrors;
import br.com.gndi.agendacirurgica.model.filter.AgendaProcedimentoFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.repository.AgendaCirurgicaRepository;
import br.com.gndi.agendacirurgica.repository.AgendaProcedimentoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaProcedimentoService {

    @Autowired
    private AgendaProcedimentoRepository agendaProcedimentoRepository;

    @Autowired
    private AgendaCirurgicaRepository agendaCirurgicaRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaProcedimento localizaProcedimento(Long id) {
        Optional<AgendaProcedimentoEntity> procedimentoExistente = agendaProcedimentoRepository.findById(id);
        if (!procedimentoExistente.isPresent()) {
            throw new ModelException(AgendaProcedimentoErrors.NOT_FOUND);
        }
        return mapper.map(procedimentoExistente.get(), AgendaProcedimento.class);
    }

    public PaginationResponse<AgendaProcedimento> pesquisaProcedimento(AgendaProcedimentoFilter agendaProcedimentoFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaProcedimentoFilter);
        Page<AgendaProcedimentoEntity> pageResult = agendaProcedimentoRepository.findAll(AgendaProcedimentoRepository.filtraAgendaProcedimento(agendaProcedimentoFilter), pageable);
        List<AgendaProcedimento> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaProcedimento.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaProcedimento criaProcedimento(AgendaProcedimento agendaProcedimento) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(agendaProcedimento.getIdAgendaCirurgica());
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        Optional<AgendaProcedimentoEntity> procedimentoExistente = agendaProcedimentoRepository.findByCodProcedimentoAndAgendaCirurgica(agendaProcedimento.getCodProcedimento(), agendaExistente.get());
        if (procedimentoExistente.isPresent()) {
            throw new ModelException(AgendaProcedimentoErrors.DUPLICATE_RECORD);
        }
        AgendaProcedimentoEntity AgendaProcedimentoEntity = mapper.map(agendaProcedimento, AgendaProcedimentoEntity.class);
        AgendaProcedimentoEntity = agendaProcedimentoRepository.save(AgendaProcedimentoEntity);
        return mapper.map(AgendaProcedimentoEntity, AgendaProcedimento.class);
    }

    public AgendaProcedimento alteraProcedimento(Long id, AgendaProcedimento agendaProcedimento) {
        Optional<AgendaProcedimentoEntity> procedimentoExistente = agendaProcedimentoRepository.findById(id);
        if (!procedimentoExistente.isPresent()) {
            throw new ModelException(AgendaProcedimentoErrors.NOT_FOUND);
        }
        AgendaProcedimentoEntity AgendaProcedimentoEntity = procedimentoExistente.get();
        AgendaProcedimentoEntity.setCodProcedimento(agendaProcedimento.getCodProcedimento());
        AgendaProcedimentoEntity.setCodLateralidade(agendaProcedimento.getCodLateralidade());
        AgendaProcedimentoEntity.setPrincipal(agendaProcedimento.getPrincipal());
        AgendaProcedimentoEntity = agendaProcedimentoRepository.save(AgendaProcedimentoEntity);
        return mapper.map(AgendaProcedimentoEntity, AgendaProcedimento.class);
    }

    public String excluiProcedimento(Long id) {
        Optional<AgendaProcedimentoEntity> procedimentoExistente = agendaProcedimentoRepository.findById(id);
        if (!procedimentoExistente.isPresent()) {
            throw new ModelException(AgendaProcedimentoErrors.NOT_FOUND);
        }
        try {
            agendaProcedimentoRepository.deleteById(id);
            return "Procedimento excluído com sucesso";
        } catch (Exception e) {
            return "Erro ao excluir o Procedimento. %s\n" + e;
        }
    }

}
