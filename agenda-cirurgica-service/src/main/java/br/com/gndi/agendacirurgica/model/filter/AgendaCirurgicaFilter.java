package br.com.gndi.agendacirurgica.model.filter;

import br.com.gndi.agendacirurgica.model.AgendaCirurgica;
import br.com.gndi.agendacirurgica.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaCirurgicaFilter extends PaginationRequest<AgendaCirurgica> {

    @ApiModelProperty(value = "Código Reserva Leito", example = "9")
    private String codReservaLeito;

    @ApiModelProperty(value = "CNPJ Estabelecimento", example = "999.999.999-99")
    @CNPJ
    private String cnpjEstabelecimento;

    @ApiModelProperty(value = "CPF Paciente", example = "999.999.999-99")
    @CPF
    private String cpfPaciente;

    @ApiModelProperty(value = "CPF Médico", example = "999.999.999-99")
    @CPF
    private String cpfMedico;

    @ApiModelProperty(value = "Título", example = "xxxxxxxxxxxxxxxxxx")
    private String titulo;

    @ApiModelProperty(value = "Diária Prevista", example = "9")
    private Integer diariaPrevista;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dataHoraInicial;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dataHoraFinal;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdTo;

}
