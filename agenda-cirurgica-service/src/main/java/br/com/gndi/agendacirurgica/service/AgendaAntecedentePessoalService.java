package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.entity.AgendaAntecedentePessoalEntity;
import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.mapper.PaginationMapper;
import br.com.gndi.agendacirurgica.model.AgendaAntecedentePessoal;
import br.com.gndi.agendacirurgica.model.errors.AgendaAntecedentePessoalErrors;
import br.com.gndi.agendacirurgica.model.errors.AgendaCirurgicaErrors;
import br.com.gndi.agendacirurgica.model.filter.AgendaAntecedentePessoalFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.repository.AgendaAntecedentePessoalRepository;
import br.com.gndi.agendacirurgica.repository.AgendaCirurgicaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaAntecedentePessoalService {

    @Autowired
    private AgendaAntecedentePessoalRepository agendaAntecedentePessoalRepository;

    @Autowired
    private AgendaCirurgicaRepository agendaCirurgicaRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaAntecedentePessoal localizaAntecedentePessoal(Long id) {
        Optional<AgendaAntecedentePessoalEntity> AntecedentePessoalExistente = agendaAntecedentePessoalRepository.findById(id);
        if (!AntecedentePessoalExistente.isPresent()) {
            throw new ModelException(AgendaAntecedentePessoalErrors.NOT_FOUND);
        }
        return mapper.map(AntecedentePessoalExistente.get(), AgendaAntecedentePessoal.class);
    }

    public PaginationResponse<AgendaAntecedentePessoal> pesquisaAntecedentePessoal(AgendaAntecedentePessoalFilter agendaAntecedentePessoalFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaAntecedentePessoalFilter);
        Page<AgendaAntecedentePessoalEntity> pageResult = agendaAntecedentePessoalRepository.findAll(AgendaAntecedentePessoalRepository.filtraAgendaAntecedentePessoal(agendaAntecedentePessoalFilter), pageable);
        List<AgendaAntecedentePessoal> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaAntecedentePessoal.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaAntecedentePessoal criaAntecedentePessoal(AgendaAntecedentePessoal agendaAntecedentePessoal) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(agendaAntecedentePessoal.getIdAgendaCirurgica());
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        Optional<AgendaAntecedentePessoalEntity> antecedentePessoalExistente = agendaAntecedentePessoalRepository.findByCodGrauParentescoAndAgendaCirurgica(agendaAntecedentePessoal.getCodGrauParentesco(), agendaExistente.get());
        if (antecedentePessoalExistente.isPresent()) {
            throw new ModelException(AgendaAntecedentePessoalErrors.DUPLICATE_RECORD);
        }
        AgendaAntecedentePessoalEntity AgendaAntecedentePessoalEntity = mapper.map(agendaAntecedentePessoal, AgendaAntecedentePessoalEntity.class);
        AgendaAntecedentePessoalEntity = agendaAntecedentePessoalRepository.save(AgendaAntecedentePessoalEntity);
        return mapper.map(AgendaAntecedentePessoalEntity, AgendaAntecedentePessoal.class);
    }

    public AgendaAntecedentePessoal alteraAntecedentePessoal(Long id, AgendaAntecedentePessoal agendaAntecedentePessoal) {
        Optional<AgendaAntecedentePessoalEntity> antecedentePessoalExistente = agendaAntecedentePessoalRepository.findById(id);
        if (!antecedentePessoalExistente.isPresent()) {
            throw new ModelException(AgendaAntecedentePessoalErrors.NOT_FOUND);
        }
        AgendaAntecedentePessoalEntity AgendaAntecedentePessoalEntity = antecedentePessoalExistente.get();
        AgendaAntecedentePessoalEntity.setCodGrauParentesco(agendaAntecedentePessoal.getCodGrauParentesco());
        AgendaAntecedentePessoalEntity.setDoenca(agendaAntecedentePessoal.getDoenca());
        AgendaAntecedentePessoalEntity = agendaAntecedentePessoalRepository.save(AgendaAntecedentePessoalEntity);
        return mapper.map(AgendaAntecedentePessoalEntity, AgendaAntecedentePessoal.class);
    }

    public String excluiAntecedentePessoal(Long id) {
        Optional<AgendaAntecedentePessoalEntity> antecedentePessoalExistente = agendaAntecedentePessoalRepository.findById(id);
        if (!antecedentePessoalExistente.isPresent()) {
            throw new ModelException(AgendaAntecedentePessoalErrors.NOT_FOUND);
        }
        try {
            agendaAntecedentePessoalRepository.deleteById(id);
            return "Antecedente Pessoal excluído com sucesso";
        } catch (Exception e) {
            return "Erro ao excluir o Antecedente Pessoal. %s\n" + e;
        }
    }

}
