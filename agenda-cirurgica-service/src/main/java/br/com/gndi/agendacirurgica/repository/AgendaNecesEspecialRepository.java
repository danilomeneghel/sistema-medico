package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.entity.AgendaNecesEspecialEntity;
import br.com.gndi.agendacirurgica.model.AgendaNecesEspecial;
import br.com.gndi.agendacirurgica.model.filter.AgendaNecesEspecialFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaNecesEspecialRepository extends JpaRepository<AgendaNecesEspecialEntity, Long> {

    Optional<AgendaNecesEspecialEntity> findById(Long id);

    Optional<AgendaNecesEspecialEntity> findByCodHemocomponenteAndAgendaCirurgica(String codHemocomponente, AgendaCirurgicaEntity agendaCirurgica);

    Page<AgendaNecesEspecialEntity> findAll(Specification<AgendaNecesEspecial> specification, Pageable pageable);

    static Specification<AgendaNecesEspecial> filtraAgendaNecesEspecial(AgendaNecesEspecialFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdAgendaCirurgica() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaCirurgica"), filter.getIdAgendaCirurgica()));
            }

            if (filter.getCodHemocomponente() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codHemocomponente"), filter.getCodHemocomponente()));
            }

            if (filter.getBiopsia() != null) {
                predicates.add(criteriaBuilder.like(root.get("biopsia"), "%" + filter.getBiopsia() + "%"));
            }

            if (filter.getTipagemSanguinea() != null) {
                predicates.add(criteriaBuilder.like(root.get("tipagemSanguinea"), "%" + filter.getTipagemSanguinea() + "%"));
            }

            if (filter.getQuantidade() != null) {
                predicates.add(criteriaBuilder.equal(root.get("quantidade"), filter.getQuantidade()));
            }

            if (filter.getObservacao() != null) {
                predicates.add(criteriaBuilder.like(root.get("observacao"), "%" + filter.getObservacao() + "%"));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
