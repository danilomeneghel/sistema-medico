package br.com.gndi.agendacirurgica.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaProcedimento {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Código Procedimento", example = "9")
    @NotBlank(message = "Campo Obrigatório")
    private String codProcedimento;

    @ApiModelProperty(value = "Código Lateralidade", example = "9")
    @NotBlank(message = "Campo Obrigatório")
    private String codLateralidade;

    @ApiModelProperty(value = "ID Agenda Cirurgica", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idAgendaCirurgica;

    @ApiModelProperty(value = "É Principal?", example = "true")
    @NotNull(message = "Campo Obrigatório")
    private Boolean principal;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updatedAt;

}
