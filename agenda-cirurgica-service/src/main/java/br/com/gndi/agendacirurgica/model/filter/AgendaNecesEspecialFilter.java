package br.com.gndi.agendacirurgica.model.filter;

import br.com.gndi.agendacirurgica.model.AgendaNecesEspecial;
import br.com.gndi.agendacirurgica.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaNecesEspecialFilter extends PaginationRequest<AgendaNecesEspecial> {

    @ApiModelProperty(value = "ID Agenda Cirurgica", example = "9")
    private Long idAgendaCirurgica;

    @ApiModelProperty(value = "Código Hemocomponente", example = "9")
    private String codHemocomponente;

    @ApiModelProperty(value = "Biópsia", example = "9")
    private String biopsia;

    @ApiModelProperty(value = "Tipagem Sanguinea", example = "xxx")
    private String tipagemSanguinea;

    @ApiModelProperty(value = "Quantidade", example = "9")
    private Integer quantidade;

    @ApiModelProperty(value = "Observação", example = "xxxxxxxxxxx")
    private String observacao;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdTo;

}
