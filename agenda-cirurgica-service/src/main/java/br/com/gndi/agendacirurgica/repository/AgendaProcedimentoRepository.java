package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.entity.AgendaProcedimentoEntity;
import br.com.gndi.agendacirurgica.model.AgendaProcedimento;
import br.com.gndi.agendacirurgica.model.filter.AgendaProcedimentoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaProcedimentoRepository extends JpaRepository<AgendaProcedimentoEntity, Long> {

    Optional<AgendaProcedimentoEntity> findById(Long id);

    Optional<AgendaProcedimentoEntity> findByCodProcedimentoAndAgendaCirurgica(String codProcedimento, AgendaCirurgicaEntity agendaCirurgica);

    Page<AgendaProcedimentoEntity> findAll(Specification<AgendaProcedimento> specification, Pageable pageable);

    static Specification<AgendaProcedimento> filtraAgendaProcedimento(AgendaProcedimentoFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodProcedimento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codProcedimento"), filter.getCodProcedimento()));
            }

            if (filter.getCodLateralidade() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codLateralidade"), filter.getCodLateralidade()));
            }

            if (filter.getIdAgendaCirurgica() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaCirurgica"), filter.getIdAgendaCirurgica()));
            }

            if (filter.getPrincipal() != null) {
                predicates.add(criteriaBuilder.equal(root.get("principal"), filter.getPrincipal()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
