package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.entity.AgendaEquipamentoEntity;
import br.com.gndi.agendacirurgica.model.AgendaEquipamento;
import br.com.gndi.agendacirurgica.model.filter.AgendaEquipamentoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaEquipamentoRepository extends JpaRepository<AgendaEquipamentoEntity, Long> {

    Optional<AgendaEquipamentoEntity> findById(Long id);

    Optional<AgendaEquipamentoEntity> findByCodEquipamentoAndAgendaCirurgica(String codEquipamento, AgendaCirurgicaEntity agendaCirurgica);

    Page<AgendaEquipamentoEntity> findAll(Specification<AgendaEquipamento> specification, Pageable pageable);

    static Specification<AgendaEquipamento> filtraAgendaEquipamento(AgendaEquipamentoFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodEquipamento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codEquipamento"), filter.getCodEquipamento()));
            }

            if (filter.getIdAgendaCirurgica() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaCirurgica"), filter.getIdAgendaCirurgica()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
