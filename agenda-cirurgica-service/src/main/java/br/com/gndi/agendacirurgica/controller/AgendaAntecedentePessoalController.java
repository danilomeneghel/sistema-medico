package br.com.gndi.agendacirurgica.controller;

import br.com.gndi.agendacirurgica.model.AgendaAntecedentePessoal;
import br.com.gndi.agendacirurgica.model.filter.AgendaAntecedentePessoalFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.service.AgendaAntecedentePessoalService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-antecedente-pessoal")
@Api(value = "Antecedente Pessoal", tags = "Cadastro de Antecedente Pessoal")
@RequiredArgsConstructor
public class AgendaAntecedentePessoalController {

    @Autowired
    private AgendaAntecedentePessoalService agendaAntecedentePessoalService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaAntecedentePessoal> localizaAntecedentePessoal(@PathVariable Long id) {
        return new ResponseEntity<>(agendaAntecedentePessoalService.localizaAntecedentePessoal(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaAntecedentePessoal> pesquisaAntecedentePessoal(@Valid @ModelAttribute AgendaAntecedentePessoalFilter pesquisaAntecedentePessoal) {
        return agendaAntecedentePessoalService.pesquisaAntecedentePessoal(pesquisaAntecedentePessoal);
    }

    @PostMapping
    public ResponseEntity<AgendaAntecedentePessoal> criaAntecedentePessoal(@Valid @RequestBody AgendaAntecedentePessoal agendaAntecedentePessoal) {
        return new ResponseEntity<>(agendaAntecedentePessoalService.criaAntecedentePessoal(agendaAntecedentePessoal), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<AgendaAntecedentePessoal> alteraAntecedentePessoal(@PathVariable Long id, @Valid @RequestBody AgendaAntecedentePessoal agendaAntecedentePessoal) {
        return new ResponseEntity<>(agendaAntecedentePessoalService.alteraAntecedentePessoal(id, agendaAntecedentePessoal), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiAntecedentePessoal(@PathVariable Long id) {
        return new ResponseEntity<>(agendaAntecedentePessoalService.excluiAntecedentePessoal(id), HttpStatus.OK);
    }

}
