package br.com.gndi.agendacirurgica.model.filter;

import br.com.gndi.agendacirurgica.model.AgendaAntecedentePessoal;
import br.com.gndi.agendacirurgica.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaAntecedentePessoalFilter extends PaginationRequest<AgendaAntecedentePessoal> {

    @ApiModelProperty(value = "Código Grau Parentesco", example = "9")
    private String codGrauParentesco;

    @ApiModelProperty(value = "ID Agenda Cirurgica", example = "9")
    private Long idAgendaCirurgica;

    @ApiModelProperty(value = "Doença", example = "9")
    private String doenca;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdTo;

}
