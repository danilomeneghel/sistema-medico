package br.com.gndi.agendacirurgica.model;

import lombok.Data;

@Data
public class AgendaDocumento {

    private String nome;

    private String tipo;

    private long tamanho;

    private String url;
}
