package br.com.gndi.agendacirurgica.controller;

import br.com.gndi.agendacirurgica.model.AgendaCirurgica;
import br.com.gndi.agendacirurgica.model.filter.AgendaCirurgicaFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.service.AgendaCirurgicaService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-cirurgica")
@Api(value = "Agenda Cirurgica", tags = "Agendamento das cirurgias")
@RequiredArgsConstructor
public class AgendaCirurgicaController {

    @Autowired
    private AgendaCirurgicaService agendaCirurgicaService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaCirurgica> localizaAgenda(@PathVariable Long id) {
        return new ResponseEntity<>(agendaCirurgicaService.localizaAgenda(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaCirurgica> pesquisaAgenda(@Valid @ModelAttribute AgendaCirurgicaFilter pesquisaAgenda) {
        return agendaCirurgicaService.pesquisaAgenda(pesquisaAgenda);
    }

    @PostMapping
    public ResponseEntity<AgendaCirurgica> criaAgenda(@Valid @RequestBody AgendaCirurgica agendaCirurgica) {
        return new ResponseEntity<>(agendaCirurgicaService.criaAgenda(agendaCirurgica), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<AgendaCirurgica> alteraAgenda(@PathVariable Long id, @Valid @RequestBody AgendaCirurgica agendaCirurgica) {
        return new ResponseEntity<>(agendaCirurgicaService.alteraAgenda(id, agendaCirurgica), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiAgenda(@PathVariable Long id) {
        return new ResponseEntity<>(agendaCirurgicaService.excluiAgenda(id), HttpStatus.OK);
    }

}
