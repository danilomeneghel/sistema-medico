package br.com.gndi.agendacirurgica.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaNecesEspecial {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "ID Agenda Cirurgica", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idAgendaCirurgica;

    @ApiModelProperty(value = "Código Hemocomponente", example = "9")
    @NotBlank(message = "Campo Obrigatório")
    private String codHemocomponente;

    @ApiModelProperty(value = "Biópsia", example = "9")
    @NotBlank(message = "Campo Obrigatório")
    private String biopsia;

    @ApiModelProperty(value = "Tipagem Sanguinea", example = "xxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipagemSanguinea;

    @ApiModelProperty(value = "Quantidade", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer quantidade;

    @ApiModelProperty(value = "Observação", example = "xxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String observacao;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updatedAt;

}
