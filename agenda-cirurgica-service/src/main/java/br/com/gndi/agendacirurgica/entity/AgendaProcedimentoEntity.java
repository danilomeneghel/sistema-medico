package br.com.gndi.agendacirurgica.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "agenda_procedimento")
@Data
public class AgendaProcedimentoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String codProcedimento;

    private String codLateralidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_agenda_cirurgica", nullable = false)
    private AgendaCirurgicaEntity agendaCirurgica;

    private Boolean principal;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
