package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.entity.AgendaNecesEspecialEntity;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.mapper.PaginationMapper;
import br.com.gndi.agendacirurgica.model.AgendaNecesEspecial;
import br.com.gndi.agendacirurgica.model.errors.AgendaCirurgicaErrors;
import br.com.gndi.agendacirurgica.model.errors.AgendaNecesEspecialErrors;
import br.com.gndi.agendacirurgica.model.filter.AgendaNecesEspecialFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.repository.AgendaCirurgicaRepository;
import br.com.gndi.agendacirurgica.repository.AgendaNecesEspecialRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaNecesEspecialService {

    @Autowired
    private AgendaNecesEspecialRepository agendaNecesEspecialRepository;

    @Autowired
    private AgendaCirurgicaRepository agendaCirurgicaRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaNecesEspecial localizaNecesEspecial(Long id) {
        Optional<AgendaNecesEspecialEntity> necesEspecialExistente = agendaNecesEspecialRepository.findById(id);
        if (!necesEspecialExistente.isPresent()) {
            throw new ModelException(AgendaNecesEspecialErrors.NOT_FOUND);
        }
        return mapper.map(necesEspecialExistente.get(), AgendaNecesEspecial.class);
    }

    public PaginationResponse<AgendaNecesEspecial> pesquisaNecesEspecial(AgendaNecesEspecialFilter agendaNecesEspecialFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaNecesEspecialFilter);
        Page<AgendaNecesEspecialEntity> pageResult = agendaNecesEspecialRepository.findAll(AgendaNecesEspecialRepository.filtraAgendaNecesEspecial(agendaNecesEspecialFilter), pageable);
        List<AgendaNecesEspecial> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaNecesEspecial.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaNecesEspecial criaNecesEspecial(AgendaNecesEspecial agendaNecesEspecial) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(agendaNecesEspecial.getIdAgendaCirurgica());
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        Optional<AgendaNecesEspecialEntity> necesEspecialExistente = agendaNecesEspecialRepository.findByCodHemocomponenteAndAgendaCirurgica(agendaNecesEspecial.getCodHemocomponente(), agendaExistente.get());
        if (necesEspecialExistente.isPresent()) {
            throw new ModelException(AgendaNecesEspecialErrors.DUPLICATE_RECORD);
        }
        AgendaNecesEspecialEntity AgendaNecesEspecialEntity = mapper.map(agendaNecesEspecial, AgendaNecesEspecialEntity.class);
        AgendaNecesEspecialEntity = agendaNecesEspecialRepository.save(AgendaNecesEspecialEntity);
        return mapper.map(AgendaNecesEspecialEntity, AgendaNecesEspecial.class);
    }

    public AgendaNecesEspecial alteraNecesEspecial(Long id, AgendaNecesEspecial agendaNecesEspecial) {
        Optional<AgendaNecesEspecialEntity> necesEspecialExistente = agendaNecesEspecialRepository.findById(id);
        if (!necesEspecialExistente.isPresent()) {
            throw new ModelException(AgendaNecesEspecialErrors.NOT_FOUND);
        }
        AgendaNecesEspecialEntity AgendaNecesEspecialEntity = necesEspecialExistente.get();
        AgendaNecesEspecialEntity.setCodHemocomponente(agendaNecesEspecial.getCodHemocomponente());
        AgendaNecesEspecialEntity.setBiopsia(agendaNecesEspecial.getBiopsia());
        AgendaNecesEspecialEntity.setTipagemSanguinea(agendaNecesEspecial.getTipagemSanguinea());
        AgendaNecesEspecialEntity.setQuantidade(agendaNecesEspecial.getQuantidade());
        AgendaNecesEspecialEntity.setObservacao(agendaNecesEspecial.getObservacao());
        AgendaNecesEspecialEntity = agendaNecesEspecialRepository.save(AgendaNecesEspecialEntity);
        return mapper.map(AgendaNecesEspecialEntity, AgendaNecesEspecial.class);
    }

    public String excluiNecesEspecial(Long id) {
        Optional<AgendaNecesEspecialEntity> necesEspecialExistente = agendaNecesEspecialRepository.findById(id);
        if (!necesEspecialExistente.isPresent()) {
            throw new ModelException(AgendaNecesEspecialErrors.NOT_FOUND);
        }
        try {
            agendaNecesEspecialRepository.deleteById(id);
            return "Necessidade Especial excluído com sucesso";
        } catch (Exception e) {
            return "Erro ao excluir o Necessidade Especial. %s\n" + e;
        }
    }

}
