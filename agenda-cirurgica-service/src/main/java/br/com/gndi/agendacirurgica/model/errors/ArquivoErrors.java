package br.com.gndi.agendacirurgica.model.errors;

import org.springframework.http.HttpStatus;

public class ArquivoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Arquivo não encontrado");

    public static final ErrorModel NOT_SELECT = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Nenhum arquivo selecionado");

    public static final ErrorModel MAX_LIMIT = new ErrorModel(HttpStatus.NOT_FOUND.value(), "400001", "Limite máximo de arquivos acima do permitido");

    public static final ErrorModel INVALID_EXT = new ErrorModel(HttpStatus.NOT_FOUND.value(), "400001", "Tipo de arquivo inválido");

}
