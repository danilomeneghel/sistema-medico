package br.com.gndi.agendacirurgica.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaNecesEspecialErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Necessidade Especial não encontrado");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Necessidade Especial já cadastrado");

}
