package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.model.AgendaCirurgica;
import br.com.gndi.agendacirurgica.model.filter.AgendaCirurgicaFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaCirurgicaRepository extends JpaRepository<AgendaCirurgicaEntity, Long> {

    Optional<AgendaCirurgicaEntity> findById(Long id);

    Optional<AgendaCirurgicaEntity> findByCpfPacienteAndDataHoraInicial(String cpfPaciente, LocalDateTime dataHoraInicial);

    Page<AgendaCirurgicaEntity> findAll(Specification<AgendaCirurgica> specification, Pageable pageable);

    static Specification<AgendaCirurgica> filtraAgendaCirurgica(AgendaCirurgicaFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodReservaLeito() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codReservaLeito"), filter.getCodReservaLeito()));
            }

            if (filter.getCnpjEstabelecimento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cnpjEstabelecimento"), filter.getCnpjEstabelecimento()));
            }

            if (filter.getCpfPaciente() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpfPaciente"), filter.getCpfPaciente()));
            }

            if (filter.getCpfMedico() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpfMedico"), filter.getCpfMedico()));
            }

            if (filter.getTitulo() != null) {
                predicates.add(criteriaBuilder.like(root.get("titulo"), "%" + filter.getTitulo() + "%"));
            }

            if (filter.getDiariaPrevista() != null) {
                predicates.add(criteriaBuilder.equal(root.get("diariaPrevista"), filter.getDiariaPrevista()));
            }

            if (filter.getDataHoraInicial() != null && filter.getDataHoraFinal() != null) {
                predicates.add(criteriaBuilder.between(root.get("dataHoraInicial"), filter.getDataHoraInicial(), filter.getDataHoraFinal()));
                predicates.add(criteriaBuilder.between(root.get("dataHoraFinal"), filter.getDataHoraInicial(), filter.getDataHoraFinal()));
            } else if (filter.getDataHoraInicial() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataHoraInicial"), filter.getDataHoraInicial()));
            } else if (filter.getDataHoraFinal() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataHoraFinal"), filter.getDataHoraFinal()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
