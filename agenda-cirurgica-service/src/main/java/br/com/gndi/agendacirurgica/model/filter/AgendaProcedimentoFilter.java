package br.com.gndi.agendacirurgica.model.filter;

import br.com.gndi.agendacirurgica.model.AgendaProcedimento;
import br.com.gndi.agendacirurgica.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaProcedimentoFilter extends PaginationRequest<AgendaProcedimento> {

    @ApiModelProperty(value = "Código Procedimento", example = "9")
    private String codProcedimento;

    @ApiModelProperty(value = "Código Lateralidade", example = "9")
    private String codLateralidade;

    @ApiModelProperty(value = "ID Agenda Cirurgica", example = "9")
    private Long idAgendaCirurgica;

    @ApiModelProperty(value = "É Principal?", example = "true")
    private Boolean principal;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdTo;

}
