package br.com.gndi.agendacirurgica.controller;

import br.com.gndi.agendacirurgica.model.AgendaEquipamento;
import br.com.gndi.agendacirurgica.model.filter.AgendaEquipamentoFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.service.AgendaEquipamentoService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-equipamento")
@Api(value = "Equipamento", tags = "Cadastro de Equipamento")
@RequiredArgsConstructor
public class AgendaEquipamentoController {

    @Autowired
    private AgendaEquipamentoService agendaEquipamentoService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaEquipamento> localizaEquipamento(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEquipamentoService.localizaEquipamento(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaEquipamento> pesquisaEquipamento(@Valid @ModelAttribute AgendaEquipamentoFilter pesquisaEquipamento) {
        return agendaEquipamentoService.pesquisaEquipamento(pesquisaEquipamento);
    }

    @PostMapping
    public ResponseEntity<AgendaEquipamento> criaEquipamento(@Valid @RequestBody AgendaEquipamento agendaEquipamento) {
        return new ResponseEntity<>(agendaEquipamentoService.criaEquipamento(agendaEquipamento), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<AgendaEquipamento> alteraEquipamento(@PathVariable Long id, @Valid @RequestBody AgendaEquipamento agendaEquipamento) {
        return new ResponseEntity<>(agendaEquipamentoService.alteraEquipamento(id, agendaEquipamento), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiEquipamento(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEquipamentoService.excluiEquipamento(id), HttpStatus.OK);
    }

}
