package br.com.gndi.agendacirurgica.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaCirurgica {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Código Reserva Leito", example = "9")
    private String codReservaLeito;

    @ApiModelProperty(value = "CNPJ Estabelecimento", example = "99.999.999/9999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CNPJ
    private String cnpjEstabelecimento;

    @ApiModelProperty(value = "CPF Paciente", example = "999.999.999-99")
    @CPF
    private String cpfPaciente;

    @ApiModelProperty(value = "CPF Médico", example = "999.999.999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CPF
    private String cpfMedico;

    @ApiModelProperty(value = "Título", example = "xxxxxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String titulo;

    @ApiModelProperty(value = "Diária Prevista", example = "9")
    private Integer diariaPrevista;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dataHoraInicial;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dataHoraFinal;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime updatedAt;

}
