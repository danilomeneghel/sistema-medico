package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.exception.FileNotFoundException;
import br.com.gndi.agendacirurgica.exception.FileStorageException;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.model.errors.ArquivoErrors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class ArmazenaArquivoService {

    @Value("${file.dir-image}")
    public String DIR_IMAGE;

    @Value("${file.extensions-image}")
    private String EXTENSIONS_IMAGE;

    public String arquivoArmazenamento(MultipartFile arquivo) {
        // Cria o diretório para enviar o arquivo, caso ainda não esteja criado
        try {
            Files.createDirectories(Paths.get(DIR_IMAGE));
        } catch (Exception ex) {
            throw new FileStorageException("Não foi possível criar o diretório em que os arquivos enviados serão armazenados.", ex);
        }

        // Gera um codigo aleatório para ser o nome do arquivo sem repetir
        String nomeArquivo = UUID.randomUUID().toString().replace("-", "");

        String extensaoArquivo = StringUtils.getFilenameExtension(arquivo.getOriginalFilename());
        Path novoNomeArquivo = Paths.get(nomeArquivo + "." + extensaoArquivo);

        // Verifica se o nome do arquivo contém caracteres inválidos
        if (nomeArquivo.contains("..")) {
            throw new FileStorageException("Desculpe! Nome do arquivo contém sequência de caminho inválido " + nomeArquivo);
        }

        // Verifica se a extensão do arquivo é válida
        if (EXTENSIONS_IMAGE.contains(extensaoArquivo)) {
            this.copyFile(DIR_IMAGE, novoNomeArquivo, arquivo);
        } else {
            throw new ModelException(ArquivoErrors.INVALID_EXT);
        }
        return novoNomeArquivo.getFileName().toString();
    }

    public Resource carregaArquivo(String nomeArquivo) {
        String extensaoArquivo = StringUtils.getFilenameExtension(nomeArquivo);
        // Verifica qual é a extensão do arquivo
        if (EXTENSIONS_IMAGE.contains(extensaoArquivo)) {
            return this.getFile(DIR_IMAGE, nomeArquivo);
        } else {
            throw new ModelException(ArquivoErrors.INVALID_EXT);
        }
    }

    public void copyFile(String dir, Path novoNomeArquivo, MultipartFile arquivo) {
        // Copia o arquivo para o local de destino
        try {
            Path targetLocation = Paths.get(dir).resolve(novoNomeArquivo);
            Files.copy(arquivo.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileStorageException("Não foi possivel armazenar o arquivo " + arquivo.getOriginalFilename() + ". Por favor tente novamente", ex);
        }
    }

    public Resource getFile(String dir, String nomeArquivo) {
        // Pega o arquivo do local de destino
        try {
            Path caminhoArquivo = Paths.get(dir).resolve(nomeArquivo).normalize();
            Resource resource = new UrlResource(caminhoArquivo.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("Arquivo não encontrado " + nomeArquivo);
            }
        } catch (IOException ex) {
            throw new FileStorageException("Não foi possivel localizar o arquivo " + nomeArquivo + ". Por favor tente novamente", ex);
        }
    }

}