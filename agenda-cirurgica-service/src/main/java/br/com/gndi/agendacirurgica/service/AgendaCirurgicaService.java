package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.mapper.PaginationMapper;
import br.com.gndi.agendacirurgica.model.AgendaCirurgica;
import br.com.gndi.agendacirurgica.model.errors.AgendaCirurgicaErrors;
import br.com.gndi.agendacirurgica.model.filter.AgendaCirurgicaFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.repository.AgendaCirurgicaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaCirurgicaService {

    @Autowired
    private AgendaCirurgicaRepository agendaCirurgicaRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaCirurgica localizaAgenda(Long id) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(id);
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        return mapper.map(agendaExistente.get(), AgendaCirurgica.class);
    }

    public PaginationResponse<AgendaCirurgica> pesquisaAgenda(AgendaCirurgicaFilter agendaCirurgicaFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaCirurgicaFilter);
        Page<AgendaCirurgicaEntity> pageResult = agendaCirurgicaRepository.findAll(AgendaCirurgicaRepository.filtraAgendaCirurgica(agendaCirurgicaFilter), pageable);
        List<AgendaCirurgica> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaCirurgica.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaCirurgica criaAgenda(AgendaCirurgica agendaCirurgica) {
        // Verifica se a mesma agenda já foi cadastrada
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.
                findByCpfPacienteAndDataHoraInicial(agendaCirurgica.getCpfPaciente(), agendaCirurgica.getDataHoraInicial());
        if (agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.DUPLICATE_RECORD);
        }
        AgendaCirurgicaEntity agendaCirurgicaEntity = mapper.map(agendaCirurgica, AgendaCirurgicaEntity.class);
        agendaCirurgicaEntity = agendaCirurgicaRepository.save(agendaCirurgicaEntity);
        return mapper.map(agendaCirurgicaEntity, AgendaCirurgica.class);
    }

    public AgendaCirurgica alteraAgenda(Long id, AgendaCirurgica agendaCirurgica) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(id);
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        AgendaCirurgicaEntity agendaCirurgicaEntity = agendaExistente.get();
        agendaCirurgicaEntity.setCodReservaLeito(agendaCirurgica.getCodReservaLeito());
        agendaCirurgicaEntity.setCpfPaciente(agendaCirurgica.getCpfPaciente());
        agendaCirurgicaEntity.setCpfMedico(agendaCirurgica.getCpfMedico());
        agendaCirurgicaEntity.setTitulo(agendaCirurgica.getTitulo());
        agendaCirurgicaEntity = agendaCirurgicaRepository.save(agendaCirurgicaEntity);
        return mapper.map(agendaCirurgicaEntity, AgendaCirurgica.class);
    }

    public String excluiAgenda(Long id) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(id);
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        try {
            agendaCirurgicaRepository.deleteById(id);
            return "Agenda Cirurgica excluída com sucesso";
        } catch (Exception e) {
            return "Erro ao excluir a Agenda Cirurgica. %s\n" + e;
        }
    }

}
