package br.com.gndi.agendacirurgica.controller;

import br.com.gndi.agendacirurgica.model.AgendaCid;
import br.com.gndi.agendacirurgica.model.filter.AgendaCidFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.service.AgendaCidService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-cid")
@Api(value = "CID", tags = "Cadastro de CID")
@RequiredArgsConstructor
public class AgendaCidController {

    @Autowired
    private AgendaCidService agendaCidService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaCid> localizaCid(@PathVariable Long id) {
        return new ResponseEntity<>(agendaCidService.localizaCid(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaCid> pesquisaCid(@Valid @ModelAttribute AgendaCidFilter pesquisaAgenda) {
        return agendaCidService.pesquisaCid(pesquisaAgenda);
    }

    @PostMapping
    public ResponseEntity<AgendaCid> criaCid(@Valid @RequestBody AgendaCid agendaCid) {
        return new ResponseEntity<>(agendaCidService.criaCid(agendaCid), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<AgendaCid> alteraCid(@PathVariable Long id, @Valid @RequestBody AgendaCid agendaCid) {
        return new ResponseEntity<>(agendaCidService.alteraCid(id, agendaCid), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiCid(@PathVariable Long id) {
        return new ResponseEntity<>(agendaCidService.excluiCid(id), HttpStatus.OK);
    }

}
