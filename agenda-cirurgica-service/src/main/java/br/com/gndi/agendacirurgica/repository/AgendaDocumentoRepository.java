package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaDocumentoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgendaDocumentoRepository extends JpaRepository<AgendaDocumentoEntity, Long> {

    Optional<AgendaDocumentoEntity> findById(Long id);

    void deleteByNome(String nome);

}
