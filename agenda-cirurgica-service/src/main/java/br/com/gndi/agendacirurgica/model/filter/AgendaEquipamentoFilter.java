package br.com.gndi.agendacirurgica.model.filter;

import br.com.gndi.agendacirurgica.model.AgendaEquipamento;
import br.com.gndi.agendacirurgica.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEquipamentoFilter extends PaginationRequest<AgendaEquipamento> {

    @ApiModelProperty(value = "Código Equipamento", example = "9")
    private String codEquipamento;

    @ApiModelProperty(value = "ID Agenda Cirurgica", example = "9")
    private Long idAgendaCirurgica;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate createdTo;

}
