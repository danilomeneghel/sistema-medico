package br.com.gndi.agendacirurgica.controller;

import br.com.gndi.agendacirurgica.model.AgendaProcedimento;
import br.com.gndi.agendacirurgica.model.filter.AgendaProcedimentoFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.service.AgendaProcedimentoService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-procedimento")
@Api(value = "Procedimento", tags = "Cadastro de Procedimento")
@RequiredArgsConstructor
public class AgendaProcedimentoController {

    @Autowired
    private AgendaProcedimentoService agendaProcedimentoService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaProcedimento> localizaProcedimento(@PathVariable Long id) {
        return new ResponseEntity<>(agendaProcedimentoService.localizaProcedimento(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaProcedimento> pesquisaProcedimento(@Valid @ModelAttribute AgendaProcedimentoFilter pesquisaAgenda) {
        return agendaProcedimentoService.pesquisaProcedimento(pesquisaAgenda);
    }

    @PostMapping
    public ResponseEntity<AgendaProcedimento> criaProcedimento(@Valid @RequestBody AgendaProcedimento agendaProcedimento) {
        return new ResponseEntity<>(agendaProcedimentoService.criaProcedimento(agendaProcedimento), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<AgendaProcedimento> alteraProcedimento(@PathVariable Long id, @Valid @RequestBody AgendaProcedimento agendaProcedimento) {
        return new ResponseEntity<>(agendaProcedimentoService.alteraProcedimento(id, agendaProcedimento), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiProcedimento(@PathVariable Long id) {
        return new ResponseEntity<>(agendaProcedimentoService.excluiProcedimento(id), HttpStatus.OK);
    }

}
