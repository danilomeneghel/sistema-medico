package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.entity.AgendaCidEntity;
import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.mapper.PaginationMapper;
import br.com.gndi.agendacirurgica.model.AgendaCid;
import br.com.gndi.agendacirurgica.model.errors.AgendaCidErrors;
import br.com.gndi.agendacirurgica.model.errors.AgendaCirurgicaErrors;
import br.com.gndi.agendacirurgica.model.filter.AgendaCidFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.repository.AgendaCidRepository;
import br.com.gndi.agendacirurgica.repository.AgendaCirurgicaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaCidService {

    @Autowired
    private AgendaCidRepository agendaCidRepository;

    @Autowired
    private AgendaCirurgicaRepository agendaCirurgicaRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaCid localizaCid(Long id) {
        Optional<AgendaCidEntity> cidExistente = agendaCidRepository.findById(id);
        if (!cidExistente.isPresent()) {
            throw new ModelException(AgendaCidErrors.NOT_FOUND);
        }
        return mapper.map(cidExistente.get(), AgendaCid.class);
    }

    public PaginationResponse<AgendaCid> pesquisaCid(AgendaCidFilter agendaCidFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaCidFilter);
        Page<AgendaCidEntity> pageResult = agendaCidRepository.findAll(AgendaCidRepository.filtraAgendaCid(agendaCidFilter), pageable);
        List<AgendaCid> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaCid.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaCid criaCid(AgendaCid agendaCid) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(agendaCid.getIdAgendaCirurgica());
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        Optional<AgendaCidEntity> cidExistente = agendaCidRepository.findByCodCidAndAgendaCirurgica(agendaCid.getCodCid(), agendaExistente.get());
        if (cidExistente.isPresent()) {
            throw new ModelException(AgendaCidErrors.DUPLICATE_RECORD);
        }
        AgendaCidEntity AgendaCidEntity = mapper.map(agendaCid, AgendaCidEntity.class);
        AgendaCidEntity = agendaCidRepository.save(AgendaCidEntity);
        return mapper.map(AgendaCidEntity, AgendaCid.class);
    }

    public AgendaCid alteraCid(Long id, AgendaCid agendaCid) {
        Optional<AgendaCidEntity> cidExistente = agendaCidRepository.findById(id);
        if (!cidExistente.isPresent()) {
            throw new ModelException(AgendaCidErrors.NOT_FOUND);
        }
        AgendaCidEntity AgendaCidEntity = cidExistente.get();
        AgendaCidEntity.setCodCid(agendaCid.getCodCid());
        AgendaCidEntity.setTempoEvolucao(agendaCid.getTempoEvolucao());
        AgendaCidEntity.setFormatoTempoEvolucao(agendaCid.getFormatoTempoEvolucao());
        AgendaCidEntity = agendaCidRepository.save(AgendaCidEntity);
        return mapper.map(AgendaCidEntity, AgendaCid.class);
    }

    public String excluiCid(Long id) {
        Optional<AgendaCidEntity> cidExistente = agendaCidRepository.findById(id);
        if (!cidExistente.isPresent()) {
            throw new ModelException(AgendaCidErrors.NOT_FOUND);
        }
        try {
            agendaCidRepository.deleteById(id);
            return "CID excluído com sucesso";
        } catch (Exception e) {
            return "Erro ao excluir o CID. %s\n" + e;
        }
    }

}
