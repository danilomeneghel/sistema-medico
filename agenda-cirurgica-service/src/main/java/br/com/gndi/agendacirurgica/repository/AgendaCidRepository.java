package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaCidEntity;
import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.model.AgendaCid;
import br.com.gndi.agendacirurgica.model.filter.AgendaCidFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaCidRepository extends JpaRepository<AgendaCidEntity, Long> {

    Optional<AgendaCidEntity> findById(Long id);

    Optional<AgendaCidEntity> findByCodCidAndAgendaCirurgica(String codCid, AgendaCirurgicaEntity agendaCirurgica);

    Page<AgendaCidEntity> findAll(Specification<AgendaCid> specification, Pageable pageable);

    static Specification<AgendaCid> filtraAgendaCid(AgendaCidFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodCid() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codCid"), filter.getCodCid()));
            }

            if (filter.getIdAgendaCirurgica() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaCirurgica"), filter.getIdAgendaCirurgica()));
            }

            if (filter.getTempoEvolucao() != null) {
                predicates.add(criteriaBuilder.equal(root.get("tempoEvolucao"), filter.getTempoEvolucao()));
            }

            if (filter.getFormatoTempoEvolucao() != null) {
                predicates.add(criteriaBuilder.like(root.get("formatoTempoEvolucao"), "%" + filter.getFormatoTempoEvolucao() + "%"));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
