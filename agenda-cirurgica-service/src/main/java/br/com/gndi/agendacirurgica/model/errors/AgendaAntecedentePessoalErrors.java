package br.com.gndi.agendacirurgica.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaAntecedentePessoalErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Antecedente Pessoal não encontrado");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Antecedente Pessoal já cadastrado");

}
