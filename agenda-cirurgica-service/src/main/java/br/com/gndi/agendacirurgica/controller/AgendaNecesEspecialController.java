package br.com.gndi.agendacirurgica.controller;

import br.com.gndi.agendacirurgica.model.AgendaNecesEspecial;
import br.com.gndi.agendacirurgica.model.filter.AgendaNecesEspecialFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.service.AgendaNecesEspecialService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-neces-especial")
@Api(value = "Necessidade Especial", tags = "Cadastro de Necessidade Especial")
@RequiredArgsConstructor
public class AgendaNecesEspecialController {

    @Autowired
    private AgendaNecesEspecialService agendaNecesEspecialService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaNecesEspecial> localizaNecesEspecial(@PathVariable Long id) {
        return new ResponseEntity<>(agendaNecesEspecialService.localizaNecesEspecial(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaNecesEspecial> pesquisaNecesEspecial(@Valid @ModelAttribute AgendaNecesEspecialFilter pesquisaAgenda) {
        return agendaNecesEspecialService.pesquisaNecesEspecial(pesquisaAgenda);
    }

    @PostMapping
    public ResponseEntity<AgendaNecesEspecial> criaNecesEspecial(@Valid @RequestBody AgendaNecesEspecial agendaNecesEspecial) {
        return new ResponseEntity<>(agendaNecesEspecialService.criaNecesEspecial(agendaNecesEspecial), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<AgendaNecesEspecial> alteraNecesEspecial(@PathVariable Long id, @Valid @RequestBody AgendaNecesEspecial agendaNecesEspecial) {
        return new ResponseEntity<>(agendaNecesEspecialService.alteraNecesEspecial(id, agendaNecesEspecial), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiNecesEspecial(@PathVariable Long id) {
        return new ResponseEntity<>(agendaNecesEspecialService.excluiNecesEspecial(id), HttpStatus.OK);
    }

}
