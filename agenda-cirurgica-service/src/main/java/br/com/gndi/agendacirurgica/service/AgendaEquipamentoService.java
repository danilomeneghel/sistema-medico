package br.com.gndi.agendacirurgica.service;

import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.entity.AgendaEquipamentoEntity;
import br.com.gndi.agendacirurgica.exception.ModelException;
import br.com.gndi.agendacirurgica.mapper.PaginationMapper;
import br.com.gndi.agendacirurgica.model.AgendaEquipamento;
import br.com.gndi.agendacirurgica.model.errors.AgendaCirurgicaErrors;
import br.com.gndi.agendacirurgica.model.errors.AgendaEquipamentoErrors;
import br.com.gndi.agendacirurgica.model.filter.AgendaEquipamentoFilter;
import br.com.gndi.agendacirurgica.model.pagination.PaginationResponse;
import br.com.gndi.agendacirurgica.repository.AgendaCirurgicaRepository;
import br.com.gndi.agendacirurgica.repository.AgendaEquipamentoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEquipamentoService {

    @Autowired
    private AgendaEquipamentoRepository agendaEquipamentoRepository;

    @Autowired
    private AgendaCirurgicaRepository agendaCirurgicaRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaEquipamento localizaEquipamento(Long id) {
        Optional<AgendaEquipamentoEntity> equipamentoExistente = agendaEquipamentoRepository.findById(id);
        if (!equipamentoExistente.isPresent()) {
            throw new ModelException(AgendaEquipamentoErrors.NOT_FOUND);
        }
        return mapper.map(equipamentoExistente.get(), AgendaEquipamento.class);
    }

    public PaginationResponse<AgendaEquipamento> pesquisaEquipamento(AgendaEquipamentoFilter agendaEquipamentoFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaEquipamentoFilter);
        Page<AgendaEquipamentoEntity> pageResult = agendaEquipamentoRepository.findAll(AgendaEquipamentoRepository.filtraAgendaEquipamento(agendaEquipamentoFilter), pageable);
        List<AgendaEquipamento> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEquipamento.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaEquipamento criaEquipamento(AgendaEquipamento agendaEquipamento) {
        Optional<AgendaCirurgicaEntity> agendaExistente = agendaCirurgicaRepository.findById(agendaEquipamento.getIdAgendaCirurgica());
        if (!agendaExistente.isPresent()) {
            throw new ModelException(AgendaCirurgicaErrors.NOT_FOUND);
        }
        Optional<AgendaEquipamentoEntity> equipamentoExistente = agendaEquipamentoRepository.findByCodEquipamentoAndAgendaCirurgica(agendaEquipamento.getCodEquipamento(), agendaExistente.get());
        if (equipamentoExistente.isPresent()) {
            throw new ModelException(AgendaEquipamentoErrors.DUPLICATE_RECORD);
        }
        AgendaEquipamentoEntity AgendaEquipamentoEntity = mapper.map(agendaEquipamento, AgendaEquipamentoEntity.class);
        AgendaEquipamentoEntity = agendaEquipamentoRepository.save(AgendaEquipamentoEntity);
        return mapper.map(AgendaEquipamentoEntity, AgendaEquipamento.class);
    }

    public AgendaEquipamento alteraEquipamento(Long id, AgendaEquipamento agendaEquipamento) {
        Optional<AgendaEquipamentoEntity> equipamentoExistente = agendaEquipamentoRepository.findById(id);
        if (!equipamentoExistente.isPresent()) {
            throw new ModelException(AgendaEquipamentoErrors.NOT_FOUND);
        }
        AgendaEquipamentoEntity AgendaEquipamentoEntity = equipamentoExistente.get();
        AgendaEquipamentoEntity.setCodEquipamento(agendaEquipamento.getCodEquipamento());
        AgendaEquipamentoEntity = agendaEquipamentoRepository.save(AgendaEquipamentoEntity);
        return mapper.map(AgendaEquipamentoEntity, AgendaEquipamento.class);
    }

    public String excluiEquipamento(Long id) {
        Optional<AgendaEquipamentoEntity> equipamentoExistente = agendaEquipamentoRepository.findById(id);
        if (!equipamentoExistente.isPresent()) {
            throw new ModelException(AgendaEquipamentoErrors.NOT_FOUND);
        }
        try {
            agendaEquipamentoRepository.deleteById(id);
            return "Equipamento excluído com sucesso";
        } catch (Exception e) {
            return "Erro ao excluir o Equipamento. %s\n" + e;
        }
    }

}
