package br.com.gndi.agendacirurgica.repository;

import br.com.gndi.agendacirurgica.entity.AgendaAntecedentePessoalEntity;
import br.com.gndi.agendacirurgica.entity.AgendaCirurgicaEntity;
import br.com.gndi.agendacirurgica.model.AgendaAntecedentePessoal;
import br.com.gndi.agendacirurgica.model.filter.AgendaAntecedentePessoalFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaAntecedentePessoalRepository extends JpaRepository<AgendaAntecedentePessoalEntity, Long> {

    Optional<AgendaAntecedentePessoalEntity> findById(Long id);

    Optional<AgendaAntecedentePessoalEntity> findByCodGrauParentescoAndAgendaCirurgica(String codGrauParentesco, AgendaCirurgicaEntity agendaCirurgica);

    Page<AgendaAntecedentePessoalEntity> findAll(Specification<AgendaAntecedentePessoal> specification, Pageable pageable);

    static Specification<AgendaAntecedentePessoal> filtraAgendaAntecedentePessoal(AgendaAntecedentePessoalFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodGrauParentesco() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codGrauParentesco"), filter.getCodGrauParentesco()));
            }

            if (filter.getIdAgendaCirurgica() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaCirurgica"), filter.getIdAgendaCirurgica()));
            }

            if (filter.getDoenca() != null) {
                predicates.add(criteriaBuilder.like(root.get("doenca"), "%" + filter.getDoenca() + "%"));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
