package br.com.gndi.email.controller;

import br.com.gndi.email.model.EmailModel;
import br.com.gndi.email.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class EmailController {

    @Autowired
    EmailService emailService;

    @PostMapping("/envia-email")
    public ResponseEntity<EmailModel> enviaEmail(@RequestBody @Valid EmailModel emailModel) {
        return new ResponseEntity<>(emailService.enviaEmail(emailModel), HttpStatus.OK);
    }

}
