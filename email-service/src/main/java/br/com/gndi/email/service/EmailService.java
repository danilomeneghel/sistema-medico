package br.com.gndi.email.service;

import br.com.gndi.email.entity.EmailEntity;
import br.com.gndi.email.enums.EmailStatus;
import br.com.gndi.email.model.EmailModel;
import br.com.gndi.email.repository.EmailRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    private JavaMailSender emailSender;

    private ModelMapper mapper = new ModelMapper();

    public EmailModel enviaEmail(EmailModel emailModel) {
        EmailEntity emailEntity = mapper.map(emailModel, EmailEntity.class);
        SimpleMailMessage email = criaEmail(emailModel);
        try {
            emailSender.send(email);
            emailEntity.setStatus(EmailStatus.ENVIADO);
        } catch (MailException e) {
            emailEntity.setStatus(EmailStatus.ERRO);
        } finally {
            emailRepository.save(emailEntity);
            return emailModel;
        }
    }

    public SimpleMailMessage criaEmail(EmailModel emailModel) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(emailModel.getEmailDe());
        message.setTo(emailModel.getEmailPara());
        message.setSubject(emailModel.getAssunto());
        message.setText(emailModel.getTexto());
        return message;
    }

}
