package br.com.gndi.email.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailModel {

    @NotBlank
    @Email
    private String emailDe;

    @NotBlank
    @Email
    private String emailPara;

    @NotBlank
    private String assunto;

    @NotBlank
    private String texto;

    private String tipo = "";

}
