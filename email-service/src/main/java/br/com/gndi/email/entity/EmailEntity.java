package br.com.gndi.email.entity;

import br.com.gndi.email.enums.EmailStatus;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Table(name = "email")
public class EmailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String emailDe;

    private String emailPara;

    private String assunto;

    @Size(min = 1, max = 800)
    private String texto;

    private String tipo;

    private EmailStatus status;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

}
