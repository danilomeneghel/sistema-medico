# Email Service

Microsserviço de Envio de E-mail

## Requisitos

* Java OpenJDK 8 (+)
* Maven (Opcional)
* Postman (Opcional)
* Oracle Database 19c

## Tecnologias

* Java
* Maven
* Spring Web
* Spring Data JPA
* Spring Java Mail
* Validation
* Lombok
* Oracle

Para carregar o projeto, digite no terminal:

```
$ ./mvnw spring-boot:run
```

Aguarde carregar todo o serviço web. <br>
Após concluído, digite o endereço abaixo no Postman para testar o envio de e-mail. <br>

POST: http://localhost:8081/srv/email-service/envia-email

```
{
	"emailDe": "notremedical@intermedica.com.br",
	"emailPara": "contato@devires.com.br",
	"assunto": "Teste de Email 0001",
	"texto": "Mensagem Teste"
}
```