package br.com.gndi.vaga.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoVaga {

    FIXA("Fixa"),
    COBERTURA("Cobertura");

    private String value;

}
