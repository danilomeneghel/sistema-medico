package br.com.gndi.vaga.entity;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "candidatura_vaga_status")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Data
public class CandidaturaVagaStatusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codStatus;

    private String status;

}
