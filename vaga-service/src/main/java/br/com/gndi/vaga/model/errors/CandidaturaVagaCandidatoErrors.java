package br.com.gndi.vaga.model.errors;

import org.springframework.http.HttpStatus;

public class CandidaturaVagaCandidatoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Candidato não encontrado");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Candidato já cadastrado");

}
