package br.com.gndi.vaga.service;

import br.com.gndi.vaga.entity.CandidaturaVagaStatusEntity;
import br.com.gndi.vaga.exception.ModelException;
import br.com.gndi.vaga.mapper.PaginationMapper;
import br.com.gndi.vaga.model.CandidaturaVagaStatus;
import br.com.gndi.vaga.model.errors.CandidaturaVagaStatusErrors;
import br.com.gndi.vaga.repository.CandidaturaVagaStatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CandidaturaVagaStatusService {

    @Autowired
    private CandidaturaVagaStatusRepository candidaturaVagaStatusRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public CandidaturaVagaStatus localizaCandidaturaVagaStatus(Integer codStatus) {
        CandidaturaVagaStatusEntity candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(codStatus);
        if (candidaturaVagaStatusExistente == null) {
            throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
        }
        return mapper.map(candidaturaVagaStatusExistente, CandidaturaVagaStatus.class);
    }

    public List<CandidaturaVagaStatus> listaCandidaturaVagaStatus() {
        List<CandidaturaVagaStatusEntity> listaCandidaturaVagaStatus = candidaturaVagaStatusRepository.findAll();
        ArrayList<CandidaturaVagaStatus> candidaturaVagaStatus = new ArrayList<>();
        for(CandidaturaVagaStatusEntity candidaturaVagaStatusEntity : listaCandidaturaVagaStatus) {
            candidaturaVagaStatus.add(mapper.map(candidaturaVagaStatusEntity, CandidaturaVagaStatus.class));
        }
        return candidaturaVagaStatus;
    }

    public CandidaturaVagaStatus criaCandidaturaVagaStatus(CandidaturaVagaStatus candidaturaVagaStatus) {
        CandidaturaVagaStatusEntity candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatusOrStatus(candidaturaVagaStatus.getCodStatus(), candidaturaVagaStatus.getStatus());
        if (candidaturaVagaStatusExistente != null) {
            throw new ModelException(CandidaturaVagaStatusErrors.DUPLICATE_RECORD);
        }
        CandidaturaVagaStatusEntity candidaturaVagaStatusEntity = mapper.map(candidaturaVagaStatus, CandidaturaVagaStatusEntity.class);
        candidaturaVagaStatusEntity = candidaturaVagaStatusRepository.save(candidaturaVagaStatusEntity);
        candidaturaVagaStatusRepository.flush();
        return mapper.map(candidaturaVagaStatusEntity, CandidaturaVagaStatus.class);
    }

    public CandidaturaVagaStatus alteraCandidaturaVagaStatus(Integer codStatus, CandidaturaVagaStatus candidaturaVagaStatus) {
        CandidaturaVagaStatusEntity candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(codStatus);
        if (candidaturaVagaStatusExistente == null) {
            throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
        }
        CandidaturaVagaStatusEntity candidaturaVagaStatusEntity = candidaturaVagaStatusExistente;
        candidaturaVagaStatusEntity.setStatus(candidaturaVagaStatus.getStatus());
        candidaturaVagaStatusEntity = candidaturaVagaStatusRepository.save(candidaturaVagaStatusEntity);
        candidaturaVagaStatusRepository.flush();
        return mapper.map(candidaturaVagaStatusEntity, CandidaturaVagaStatus.class);
    }

    public String excluiCandidaturaVagaStatus(Integer codStatus) {
        CandidaturaVagaStatusEntity candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(codStatus);
        if (candidaturaVagaStatusExistente == null) {
            throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
        }
        try {
            candidaturaVagaStatusRepository.deleteById(candidaturaVagaStatusExistente.getId());
            return "Status excluído com sucesso";
        } catch (Exception ex) {
            return String.valueOf("Erro ao excluir: " + ex);
        }
    }

}
