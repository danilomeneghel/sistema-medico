package br.com.gndi.vaga.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

@ApiModel
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CandidaturaVaga {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Vaga", example = "x999")
    @NotBlank(message = "Campo Obrigatório")
    private String codVaga;

    private CandidaturaVagaCandidato candidato;

    private CandidaturaVagaStatus status;

    @ApiModelProperty(value = "Nome Vaga", example = "xxxxxxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nomeVaga;

    @ApiModelProperty(value = "Tipo de Vaga", example = "xxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipoVaga;

    @ApiModelProperty(value = "Centro Clínico da Vaga", example = "xxxxxxxx")
    private String centroClinicoVaga;

    @ApiModelProperty(value = "Localidade de Interesse da Vaga", example = "xxxxxxxxxxxxxxx")
    private String localidadeInteresse;

    @ApiModelProperty(value = "Data Início de Interesse", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    @NotNull(message = "Campo Obrigatório")
    private LocalDate dataInicioInteresse;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
