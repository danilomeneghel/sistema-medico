package br.com.gndi.vaga.client.salesforce.controller;

import br.com.gndi.vaga.client.salesforce.feign.StatusVagaClient;
import br.com.gndi.vaga.client.salesforce.model.StatusVagaCobertura;
import br.com.gndi.vaga.client.salesforce.model.StatusVagaFixa;
import br.com.gndi.vaga.client.salesforce.model.errors.VagaErrors;
import br.com.gndi.vaga.exception.ModelException;
import br.com.gndi.vaga.model.CandidaturaVagaCandidato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("status-vaga")
public class StatusVagaController {

    @Autowired
    TokenController tokenController;

    @Autowired
    StatusVagaClient statusVagaClient;

    @PutMapping("atualizar-status-vaga-fixa/{codVaga}")
    public StatusVagaFixa atualizarStatusVagaFixa(@PathVariable(value = "codVaga") String codVaga, Integer codStatus, CandidaturaVagaCandidato candidato) {
        String token = tokenController.gerarToken();
        StatusVagaFixa statusVagaFixa = new StatusVagaFixa();
        if (codStatus == 4) { //Aprovado
            statusVagaFixa.setObservacoes__c("Aprovado - " + candidato.getNome() + " - CPF: " + candidato.getCpf());
        } else {
            statusVagaFixa.setObservacoes__c("Cancelado/Rejeitado");
        }
        statusVagaFixa.setFim__c(null);
        try {
            return statusVagaClient.atualizarStatusVagaFixa(codVaga, statusVagaFixa, "Bearer " + token);
        } catch (Exception e) {
            throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
        }
    }

    @PutMapping("atualizar-status-vaga-cobertura/{codVaga}")
    public StatusVagaCobertura atualizarStatusVagaCobertura(@PathVariable(value = "codVaga") String codVaga, Integer codStatus, CandidaturaVagaCandidato candidato) {
        String token = tokenController.gerarToken();
        StatusVagaCobertura statusVagaCobertura = new StatusVagaCobertura();
        if (codStatus == 4) { //Aprovado
            statusVagaCobertura.setObservacoes__c("Aprovado - " + candidato.getNome() + " - CPF: " + candidato.getCpf());
        } else {
            statusVagaCobertura.setObservacoes__c("Cancelado/Rejeitado");
        }
        try {
            return statusVagaClient.atualizarStatusVagaCobertura(codVaga, statusVagaCobertura, "Bearer " + token);
        } catch (Exception e) {
            throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
        }
    }

}
