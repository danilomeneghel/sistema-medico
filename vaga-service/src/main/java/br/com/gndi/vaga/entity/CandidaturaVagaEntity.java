package br.com.gndi.vaga.entity;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "candidatura_vaga")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Data
public class CandidaturaVagaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String codVaga;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_candidatura_vaga_candidato", nullable = false)
    private CandidaturaVagaCandidatoEntity candidato;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_candidatura_vaga_status", nullable = false)
    private CandidaturaVagaStatusEntity status;

    private String nomeVaga;

    private String tipoVaga;

    private String centroClinicoVaga;

    private String localidadeInteresse;

    private LocalDate dataInicioInteresse;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
