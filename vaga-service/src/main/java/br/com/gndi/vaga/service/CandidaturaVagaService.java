package br.com.gndi.vaga.service;

import br.com.gndi.vaga.client.salesforce.controller.StatusVagaController;
import br.com.gndi.vaga.client.salesforce.model.errors.VagaErrors;
import br.com.gndi.vaga.entity.CandidaturaVagaCandidatoEntity;
import br.com.gndi.vaga.entity.CandidaturaVagaEntity;
import br.com.gndi.vaga.entity.CandidaturaVagaStatusEntity;
import br.com.gndi.vaga.enums.TipoVaga;
import br.com.gndi.vaga.exception.ModelException;
import br.com.gndi.vaga.mapper.PaginationMapper;
import br.com.gndi.vaga.model.CandidaturaVaga;
import br.com.gndi.vaga.model.CandidaturaVagaCandidato;
import br.com.gndi.vaga.model.errors.CandidaturaVagaCandidatoErrors;
import br.com.gndi.vaga.model.errors.CandidaturaVagaErrors;
import br.com.gndi.vaga.model.errors.CandidaturaVagaStatusErrors;
import br.com.gndi.vaga.model.filter.CandidaturaVagaFilter;
import br.com.gndi.vaga.model.pagination.PaginationResponse;
import br.com.gndi.vaga.repository.CandidaturaVagaCandidatoRepository;
import br.com.gndi.vaga.repository.CandidaturaVagaRepository;
import br.com.gndi.vaga.repository.CandidaturaVagaStatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CandidaturaVagaService {

    @Autowired
    private StatusVagaController statusVagaController;

    @Autowired
    private CandidaturaVagaRepository candidaturaVagaRepository;

    @Autowired
    private CandidaturaVagaCandidatoRepository candidaturaVagaCandidatoRepository;

    @Autowired
    private CandidaturaVagaCandidatoService candidaturaVagaCandidatoService;

    @Autowired
    private PaginationMapper paginationMapper;

    @Autowired
    private CandidaturaVagaStatusRepository candidaturaVagaStatusRepository;


    private ModelMapper mapper = new ModelMapper();

    public CandidaturaVaga localizaCandidaturaVaga(Long id) {
        Optional<CandidaturaVagaEntity> candidaturaVagaExistente = candidaturaVagaRepository.findById(id);
        if (!candidaturaVagaExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaErrors.NOT_FOUND);
        }
        return mapper.map(candidaturaVagaExistente.get(), CandidaturaVaga.class);
    }

    public PaginationResponse<CandidaturaVaga> pesquisaCandidaturaVaga(CandidaturaVagaFilter candidaturaVagaFilter) {
        Pageable pageable = paginationMapper.toPageable(candidaturaVagaFilter);
        Page<CandidaturaVagaEntity> pageResult = candidaturaVagaRepository.findAll(CandidaturaVagaRepository.filtraCandidaturaVaga(candidaturaVagaFilter), pageable);
        List<CandidaturaVaga> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, CandidaturaVaga.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public CandidaturaVaga criaCandidaturaVaga(CandidaturaVaga candidaturaVaga) {
        CandidaturaVagaEntity candidaturaVagaEntity = mapper.map(candidaturaVaga, CandidaturaVagaEntity.class);
        //Verifica se o candidato existe
        CandidaturaVagaCandidato candidato;
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findByCpf(candidaturaVaga.getCandidato().getCpf());
        if (!candidatoExistente.isPresent()) {
            //Caso não tenha ainda os dados do candidato, salva primeiro seus dados no banco
            candidato = candidaturaVagaCandidatoService.criaCandidato(candidaturaVaga.getCandidato());
            candidaturaVagaEntity.setCandidato(mapper.map(candidato, CandidaturaVagaCandidatoEntity.class));
        } else {
            candidaturaVagaEntity.setCandidato(candidatoExistente.get());
            candidato = mapper.map(candidatoExistente.get(), CandidaturaVagaCandidato.class);
        }
        //Verifica o tipo de vaga e cria o status inicial
        Integer statusInicial = 1;
        CandidaturaVagaStatusEntity candidaturaVagaStatusExistente;
        if (candidaturaVaga.getTipoVaga().equals(TipoVaga.FIXA.getValue())) {
            statusInicial = 1; //Em análise
            //Verifica o Status Inicial 'Aprovado' existe
            candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(statusInicial);
            if (candidaturaVagaStatusExistente == null) {
                throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
            }
            //Verifica se o candidato já realizou candidatura na mesma vaga
            List<CandidaturaVagaEntity> candidaturaVagaCandidatoExistente = candidaturaVagaRepository.findByCodVagaAndTipoVagaAndCandidato(candidaturaVaga.getCodVaga(), candidaturaVaga.getTipoVaga(), candidatoExistente);
            if (!candidaturaVagaCandidatoExistente.isEmpty()) {
                for (int i = 1; i <= candidaturaVagaCandidatoExistente.size(); i++) {
                    //Caso o status da candidatura seja 'Cancelado', permite re-candidatar
                    if (i == candidaturaVagaCandidatoExistente.size() && candidaturaVagaCandidatoExistente.get(i-1).getStatus().getCodStatus() == 3) {
                        //Atualiza o status da Vaga Fixa na API SalesForce
                        try {
                            statusVagaController.atualizarStatusVagaFixa(candidaturaVaga.getCodVaga(), statusInicial, candidato);
                        } catch (Exception e) {
                            throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                        }
                    } else if (i == candidaturaVagaCandidatoExistente.size() && candidaturaVagaCandidatoExistente.get(i-1).getStatus().getCodStatus() != 3) {
                        throw new ModelException(CandidaturaVagaErrors.DUPLICATE_RECORD);
                    }
                }
            } else {
                //Atualiza o status da Vaga Fixa na API SalesForce
                try {
                    statusVagaController.atualizarStatusVagaFixa(candidaturaVaga.getCodVaga(), statusInicial, candidato);
                } catch (Exception e) {
                    throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                }
            }
        } else if (candidaturaVaga.getTipoVaga().equals(TipoVaga.COBERTURA.getValue())) {
            statusInicial = 4; //Aprovado
            //Verifica o Status Inicial 'Aprovado' existe
            candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(statusInicial);
            if (candidaturaVagaStatusExistente == null) {
                throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
            }
            //Verifica se não existe candidatura de vaga de cobertura 'Aprovado'
            List<CandidaturaVagaEntity> candidaturaVagaExistente = candidaturaVagaRepository.findByCodVagaAndTipoVagaAndStatus(candidaturaVaga.getCodVaga(), candidaturaVaga.getTipoVaga(), candidaturaVagaStatusExistente);
            //Se for novo entra direto como 'Aprovado'
            if (candidaturaVagaExistente.isEmpty()) {
                //Atualiza o status da Vaga Cobertura na API SalesForce
                try {
                    statusVagaController.atualizarStatusVagaCobertura(candidaturaVaga.getCodVaga(), statusInicial, candidato);
                } catch (Exception e) {
                    throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                }
            } else {
                //Verifica se o candidato já realizou candidatura na mesma vaga
                List<CandidaturaVagaEntity> candidaturaVagaCandidatoExistente = candidaturaVagaRepository.findByCodVagaAndTipoVaga(candidaturaVaga.getCodVaga(), candidaturaVaga.getTipoVaga());
                if (!candidaturaVagaCandidatoExistente.isEmpty()) {
                    for (int i = 1; i <= candidaturaVagaCandidatoExistente.size(); i++) {
                        //Caso o status da candidatura seja 'Cancelado', permite re-candidatar
                        if (candidaturaVagaCandidatoExistente.get(i-1).getStatus().getCodStatus() == 4) {
                            throw new ModelException(CandidaturaVagaErrors.VACANCY_FILLED);
                        } else if (i == candidaturaVagaCandidatoExistente.size() && candidaturaVagaCandidatoExistente.get(i-1).getStatus().getCodStatus() == 3) {
                            //Atualiza o status da Vaga Cobertura na API SalesForce
                            try {
                                statusVagaController.atualizarStatusVagaCobertura(candidaturaVaga.getCodVaga(), statusInicial, candidato);
                            } catch (Exception e) {
                                throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                            }
                        }
                    }
                } else {
                    //Caso já tenha alguma candidatura nessa mesma vaga, dá o alerta de vaga preenchida
                    throw new ModelException(CandidaturaVagaErrors.VACANCY_FILLED);
                }
            }
        } else {
            throw new ModelException(CandidaturaVagaErrors.TYPE_NOT_FOUND);
        }
        candidaturaVagaEntity.setStatus(candidaturaVagaStatusExistente);
        candidaturaVagaEntity = candidaturaVagaRepository.save(candidaturaVagaEntity);
        candidaturaVagaRepository.flush();
        return mapper.map(candidaturaVagaEntity, CandidaturaVaga.class);
    }

    public CandidaturaVaga cancelaCandidaturaVaga(Long id) {
        //Verifica se a candidatura existe
        Optional<CandidaturaVagaEntity> candidaturaVagaExistente = candidaturaVagaRepository.findById(id);
        if (!candidaturaVagaExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaErrors.NOT_FOUND);
        }
        //Verifica se o candidato existe
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findByCpf(candidaturaVagaExistente.get().getCandidato().getCpf());
        if (candidatoExistente.isPresent()) {
            Integer codStatus = 3; //Cancelado
            //Localiza o status 'Cancelado'
            CandidaturaVagaStatusEntity candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(codStatus);
            if (candidaturaVagaStatusExistente != null) {
                //Verifica se a vaga é do tipo 'Fixa'
                if (candidaturaVagaExistente.get().getTipoVaga().equals(TipoVaga.FIXA.getValue())) {
                    //Atualiza o status da Vaga Fixa na API SalesForce
                    try {
                        statusVagaController.atualizarStatusVagaFixa(candidaturaVagaExistente.get().getCodVaga(), codStatus, mapper.map(candidatoExistente.get(), CandidaturaVagaCandidato.class));
                    } catch (Exception e) {
                        throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                    }
                } else if (candidaturaVagaExistente.get().getTipoVaga().equals(TipoVaga.COBERTURA.getValue())) {
                    //Atualiza o status da Vaga Cobertura na API SalesForce
                    try {
                        statusVagaController.atualizarStatusVagaCobertura(candidaturaVagaExistente.get().getCodVaga(), codStatus, mapper.map(candidatoExistente.get(), CandidaturaVagaCandidato.class));
                    } catch (Exception e) {
                        throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                    }
                }
                CandidaturaVagaEntity candidaturaVagaEntity = candidaturaVagaExistente.get();
                //Seta o novo status e realiza a alteração
                candidaturaVagaEntity.setStatus(candidaturaVagaStatusExistente);
                candidaturaVagaEntity = candidaturaVagaRepository.save(candidaturaVagaEntity);
                candidaturaVagaRepository.flush();
                return mapper.map(candidaturaVagaEntity, CandidaturaVaga.class);
            } else {
                throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
            }
        } else {
            throw new ModelException(CandidaturaVagaCandidatoErrors.NOT_FOUND);
        }
    }

    public CandidaturaVaga alteraStatusCandidaturaVaga(Long id, Integer codStatus) {
        //Verifica se a candidatura existe
        Optional<CandidaturaVagaEntity> candidaturaVagaExistente = candidaturaVagaRepository.findById(id);
        if (!candidaturaVagaExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaErrors.NOT_FOUND);
        }
        //Verifica se o candidato existe
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findByCpf(candidaturaVagaExistente.get().getCandidato().getCpf());
        if (candidatoExistente.isPresent()) {
            //Verifica se o status informado existe
            CandidaturaVagaStatusEntity candidaturaVagaStatusExistente = candidaturaVagaStatusRepository.findByCodStatus(codStatus);
            if (candidaturaVagaStatusExistente == null) {
                throw new ModelException(CandidaturaVagaStatusErrors.NOT_FOUND);
            }
            //Verifica se a vaga é do tipo Fixa
            if (candidaturaVagaExistente.get().getTipoVaga().equals(TipoVaga.FIXA.getValue())) {
                //Caso o status seja 'Aprovado', coloca as demais candidaturas como 'Reprovado'
                if (codStatus == 4) { //Aprovado
                    //Atualiza o status da Vaga Fixa na API SalesForce
                    try {
                        statusVagaController.atualizarStatusVagaFixa(candidaturaVagaExistente.get().getCodVaga(), codStatus, mapper.map(candidatoExistente.get(), CandidaturaVagaCandidato.class));
                    } catch (Exception e) {
                        throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                    }
                    //Atualiza o status das demais Candidatura da Vaga para Reprovado
                    CandidaturaVagaStatusEntity statusReprovado = candidaturaVagaStatusRepository.findByCodStatus(2);
                    candidaturaVagaRepository.updateCandidaturaVagaNaoAprovado(candidaturaVagaExistente.get().getCodVaga(), statusReprovado.getId());
                } else if (codStatus == 2) { //Reprovado
                    //Atualiza o status da Vaga Fixa na API SalesForce
                    try {
                        statusVagaController.atualizarStatusVagaFixa(candidaturaVagaExistente.get().getCodVaga(), codStatus, mapper.map(candidatoExistente.get(), CandidaturaVagaCandidato.class));
                    } catch (Exception e) {
                        throw new ModelException(VagaErrors.STATUS_UPDATE_ERROR);
                    }
                } else if (codStatus == 3) { //Cancelado
                    //Verifica se a alteração do status é 'Cancelado' (Gestor não pode cancelar)
                    throw new ModelException(CandidaturaVagaStatusErrors.NOT_CHANGE_STATUS);
                }
            }
            CandidaturaVagaEntity candidaturaVagaEntity = candidaturaVagaExistente.get();
            //Seta o novo status e realiza a alteração
            candidaturaVagaEntity.setStatus(candidaturaVagaStatusExistente);
            candidaturaVagaEntity = candidaturaVagaRepository.save(candidaturaVagaEntity);
            candidaturaVagaRepository.flush();
            return mapper.map(candidaturaVagaEntity, CandidaturaVaga.class);
        } else {
            throw new ModelException(CandidaturaVagaCandidatoErrors.NOT_FOUND);
        }
    }

    public String excluiCandidaturaVaga(Long id) {
        Optional<CandidaturaVagaEntity> candidaturaVagaExistente = candidaturaVagaRepository.findById(id);
        if (!candidaturaVagaExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaErrors.NOT_FOUND);
        }
        try {
            candidaturaVagaRepository.deleteById(id);
            return "Candidatura de vaga excluída com sucesso";
        } catch (Exception ex) {
            return "Erro ao excluir: " + ex;
        }
    }

}
