package br.com.gndi.vaga.model.filter;

import br.com.gndi.vaga.model.CandidaturaVagaStatus;
import br.com.gndi.vaga.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class CandidaturaVagaStatusFilter extends PaginationRequest<CandidaturaVagaStatus> {

    @ApiModelProperty(value = "Cod Status", example = "9")
    private Integer codStatus;

    @ApiModelProperty(value = "Status", example = "xxxxxxxxxxxxx")
    private String status;

}
