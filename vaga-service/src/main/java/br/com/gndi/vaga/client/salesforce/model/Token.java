package br.com.gndi.vaga.client.salesforce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Token {

    private String access_token;

    private String instance_url;

    private String instance_url_edt;

    private String id;

    private String token_type;

    private String issued_at;

    private String signature;

}
