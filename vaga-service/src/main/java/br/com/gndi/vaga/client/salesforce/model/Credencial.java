package br.com.gndi.vaga.client.salesforce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Credencial {

    @NotBlank
    private String client_secret;

    @NotBlank
    private String grant_type;

    @NotBlank
    private String client_id;

    @NotBlank
    private String redirect_uri;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

}
