package br.com.gndi.vaga.client.salesforce.feign;

import br.com.gndi.vaga.client.salesforce.model.StatusVagaCobertura;
import br.com.gndi.vaga.client.salesforce.model.StatusVagaFixa;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.validation.Valid;

@FeignClient(name = "status-vaga", url = "${feign.client.url.salesforce}")
public interface StatusVagaClient {
    @PutMapping("/atualizar/statusvaga/{codVaga}")
    StatusVagaFixa atualizarStatusVagaFixa(@PathVariable(value = "codVaga") String codVaga, @RequestBody @Valid StatusVagaFixa statusVagaFixa, @RequestHeader("Authorization") String token);
    @PutMapping("/atualizar/statusvaga/{codVaga}")
    StatusVagaCobertura atualizarStatusVagaCobertura(@PathVariable(value = "codVaga") String codVaga, @RequestBody @Valid StatusVagaCobertura statusVagaCobertura, @RequestHeader("Authorization") String token);
}
