package br.com.gndi.vaga.controller;

import br.com.gndi.vaga.model.CandidaturaVagaCandidato;
import br.com.gndi.vaga.model.filter.CandidaturaVagaCandidatoFilter;
import br.com.gndi.vaga.model.pagination.PaginationResponse;
import br.com.gndi.vaga.service.CandidaturaVagaCandidatoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/candidatura-vaga-candidato")
@Api(value = "Candidatos", tags = "Candidatos das Vagas")
public class CandidaturaVagaCandidatoController {

    @Autowired
    private CandidaturaVagaCandidatoService candidaturaVagaCandidatoService;

    @GetMapping("{id}")
    public ResponseEntity<CandidaturaVagaCandidato> localizaCandidato(@PathVariable Long id) {
        return new ResponseEntity<>(candidaturaVagaCandidatoService.localizaCandidato(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<CandidaturaVagaCandidato> pesquisaCandidato(@Valid @ModelAttribute CandidaturaVagaCandidatoFilter candidaturaVagaCandidatoFilter) {
        return candidaturaVagaCandidatoService.pesquisaCandidato(candidaturaVagaCandidatoFilter);
    }

    @PostMapping
    public ResponseEntity<CandidaturaVagaCandidato> criaCandidato(@Valid @RequestBody CandidaturaVagaCandidato candidaturaVagaCandidato) {
        return new ResponseEntity<>(candidaturaVagaCandidatoService.criaCandidato(candidaturaVagaCandidato), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<CandidaturaVagaCandidato> alteraCandidato(@PathVariable Long id, @Valid @RequestBody CandidaturaVagaCandidato candidaturaVagaCandidato) {
        return new ResponseEntity<>(candidaturaVagaCandidatoService.alteraCandidato(id, candidaturaVagaCandidato), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiCandidato(@PathVariable Long id) {
        return new ResponseEntity<>(candidaturaVagaCandidatoService.excluiCandidato(id), HttpStatus.OK);
    }

}
