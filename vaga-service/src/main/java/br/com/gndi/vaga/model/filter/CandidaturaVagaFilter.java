package br.com.gndi.vaga.model.filter;

import br.com.gndi.vaga.model.CandidaturaVaga;
import br.com.gndi.vaga.model.pagination.PaginationRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class CandidaturaVagaFilter extends PaginationRequest<CandidaturaVaga> {

    @ApiModelProperty(value = "Cod Vaga", example = "x999")
    private String codVaga;

    @ApiModelProperty(value = "Cod Status", example = "9")
    private Integer codStatus;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "Nome Vaga", example = "xxxxxxxxxxxxxxxxxxx")
    private String nomeVaga;

    @ApiModelProperty(value = "Tipo de Vaga", example = "xxxxxxx")
    private String tipoVaga;

    @ApiModelProperty(value = "Centro Clínico da Vaga", example = "xxxxxxxx")
    private String centroClinicoVaga;

    @ApiModelProperty(value = "Localidade de Interesse da Vaga", example = "xxxxxxxxxxxxxxx")
    private String localidadeInteresse;

    @ApiModelProperty(value = "Data Início de Interesse", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataInicioInteresse;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdTo;

}
