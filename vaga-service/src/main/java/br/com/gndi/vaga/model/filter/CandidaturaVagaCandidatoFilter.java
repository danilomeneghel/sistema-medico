package br.com.gndi.vaga.model.filter;

import br.com.gndi.vaga.model.CandidaturaVagaCandidato;
import br.com.gndi.vaga.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel
@Data
public class CandidaturaVagaCandidatoFilter extends PaginationRequest<CandidaturaVagaCandidato> {

    @ApiModelProperty(value = "Cod Especialidade", example = "x999")
    private String codEspecialidade;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "Nome Candidato", example = "xxxxxxxxxxxxxxxxxxxxx")
    private String nome;

    @ApiModelProperty(value = "Nome Especialidade", example = "xxxxxxxxxxxxxxxxxxxxx")
    private String nomeEspecialidade;

    @ApiModelProperty(value = "Ano Formação", example = "2001")
    private Integer anoFormacao;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdTo;

}
