package br.com.gndi.vaga.client.salesforce.controller;

import br.com.gndi.vaga.client.salesforce.feign.TokenClient;
import br.com.gndi.vaga.client.salesforce.model.errors.VagaErrors;
import br.com.gndi.vaga.exception.ModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("token-salesforce")
public class TokenController {

    @Autowired
    TokenClient tokenClient;

    @Value("${gndi.salesforce.client.id}")
    private String clientId;

    @Value("${gndi.salesforce.client.secret}")
    private String clientSecret;

    @Value("${gndi.salesforce.username}")
    private String username;

    @Value("${gndi.salesforce.password}")
    private String password;

    @Value("${gndi.salesforce.grant_type}")
    private String grantType;

    @PostMapping
    public String gerarToken() {
        try {
            return tokenClient.gerarToken(getFilledForm()).getAccess_token();
        } catch (Exception e) {
            throw new ModelException(VagaErrors.TOKEN_ERROR);
        }
    }

    private MultiValueMap<String, ?> getFilledForm() {
        LinkedMultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
        form.add("client_id", clientId);
        form.add("client_secret", clientSecret);
        form.add("username", username);
        form.add("password", password);
        form.add("grant_type", grantType);
        return form;
    }

}
