package br.com.gndi.vaga.service;

import br.com.gndi.vaga.entity.CandidaturaVagaCandidatoEntity;
import br.com.gndi.vaga.exception.ModelException;
import br.com.gndi.vaga.mapper.PaginationMapper;
import br.com.gndi.vaga.model.CandidaturaVagaCandidato;
import br.com.gndi.vaga.model.errors.CandidaturaVagaCandidatoErrors;
import br.com.gndi.vaga.model.filter.CandidaturaVagaCandidatoFilter;
import br.com.gndi.vaga.model.pagination.PaginationResponse;
import br.com.gndi.vaga.repository.CandidaturaVagaCandidatoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CandidaturaVagaCandidatoService {

    @Autowired
    private CandidaturaVagaCandidatoRepository candidaturaVagaCandidatoRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    ModelMapper mapper = new ModelMapper();

    public CandidaturaVagaCandidato localizaCandidato(Long id) {
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findById(id);
        if (!candidatoExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaCandidatoErrors.NOT_FOUND);
        }
        return mapper.map(candidatoExistente.get(), CandidaturaVagaCandidato.class);
    }

    public PaginationResponse<CandidaturaVagaCandidato> pesquisaCandidato(CandidaturaVagaCandidatoFilter candidaturaVagaCandidatoFilter) {
        Pageable pageable = paginationMapper.toPageable(candidaturaVagaCandidatoFilter);
        Page<CandidaturaVagaCandidatoEntity> pageResult = candidaturaVagaCandidatoRepository.findAll(CandidaturaVagaCandidatoRepository.filtraCandidato(candidaturaVagaCandidatoFilter), pageable);
        List<CandidaturaVagaCandidato> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, CandidaturaVagaCandidato.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public CandidaturaVagaCandidato criaCandidato(CandidaturaVagaCandidato candidaturaVagaCandidato) {
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findByCpf(candidaturaVagaCandidato.getCpf());
        if (candidatoExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaCandidatoErrors.DUPLICATE_RECORD);
        }
        CandidaturaVagaCandidatoEntity candidaturaVagaCandidatoEntity = mapper.map(candidaturaVagaCandidato, CandidaturaVagaCandidatoEntity.class);
        candidaturaVagaCandidatoEntity = candidaturaVagaCandidatoRepository.save(candidaturaVagaCandidatoEntity);
        candidaturaVagaCandidatoRepository.flush();
        return mapper.map(candidaturaVagaCandidatoEntity, CandidaturaVagaCandidato.class);
    }

    public CandidaturaVagaCandidato alteraCandidato(Long id, CandidaturaVagaCandidato candidaturaVagaCandidato) {
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findById(id);
        if (!candidatoExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaCandidatoErrors.NOT_FOUND);
        }
        CandidaturaVagaCandidatoEntity candidaturaVagaCandidatoEntity = candidatoExistente.get();
        candidaturaVagaCandidatoEntity.setCodEspecialidade(candidaturaVagaCandidato.getCodEspecialidade());
        candidaturaVagaCandidatoEntity.setCpf(candidaturaVagaCandidato.getCpf());
        candidaturaVagaCandidatoEntity.setNome(candidaturaVagaCandidato.getNome());
        candidaturaVagaCandidatoEntity.setNomeEspecialidade(candidaturaVagaCandidato.getNomeEspecialidade());
        candidaturaVagaCandidatoEntity.setAnoFormacao(candidaturaVagaCandidato.getAnoFormacao());
        candidaturaVagaCandidatoEntity = candidaturaVagaCandidatoRepository.save(candidaturaVagaCandidatoEntity);
        candidaturaVagaCandidatoRepository.flush();
        return mapper.map(candidaturaVagaCandidatoEntity, CandidaturaVagaCandidato.class);
    }

    public String excluiCandidato(Long id) {
        Optional<CandidaturaVagaCandidatoEntity> candidatoExistente = candidaturaVagaCandidatoRepository.findById(id);
        if (!candidatoExistente.isPresent()) {
            throw new ModelException(CandidaturaVagaCandidatoErrors.NOT_FOUND);
        }
        try {
            candidaturaVagaCandidatoRepository.deleteById(id);
            return "Candidato excluído com sucesso";
        } catch (Exception ex) {
            return String.valueOf("Erro ao excluir: " + ex);
        }
    }

}
