package br.com.gndi.vaga.model.errors;

import org.springframework.http.HttpStatus;

public class CandidaturaVagaStatusErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Status da candidatura da vaga não encontrado");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Status da candaditura da vaga já cadastrado");

    public static final ErrorModel NOT_CHANGE_STATUS = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Não é possível mudar o status da candidatura");

}
