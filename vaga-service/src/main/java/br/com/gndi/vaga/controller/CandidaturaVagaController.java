package br.com.gndi.vaga.controller;

import br.com.gndi.vaga.model.CandidaturaVaga;
import br.com.gndi.vaga.model.filter.CandidaturaVagaFilter;
import br.com.gndi.vaga.model.pagination.PaginationResponse;
import br.com.gndi.vaga.service.CandidaturaVagaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/candidatura-vaga")
@Api(value = "Candidaturas", tags = "Candidaturas das Vagas")
public class CandidaturaVagaController {

    @Autowired
    private CandidaturaVagaService candidaturaVagaService;

    @GetMapping("{id}")
    public ResponseEntity<CandidaturaVaga> localizaCandidaturaVaga(@PathVariable Long id) {
        return new ResponseEntity<>(candidaturaVagaService.localizaCandidaturaVaga(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<CandidaturaVaga> pesquisaCandidaturaVaga(@Valid @ModelAttribute CandidaturaVagaFilter candidaturaVagaFilter) {
        return candidaturaVagaService.pesquisaCandidaturaVaga(candidaturaVagaFilter);
    }

    @PostMapping
    public ResponseEntity<CandidaturaVaga> criaCandidaturaVaga(@Valid @RequestBody CandidaturaVaga candidaturaVaga) {
        return new ResponseEntity<>(candidaturaVagaService.criaCandidaturaVaga(candidaturaVaga), HttpStatus.CREATED);
    }

    @PutMapping("/cancela-candidatura-vaga/{id}")
    public ResponseEntity<CandidaturaVaga> cancelaCandidaturaVaga(@PathVariable Long id) {
        return new ResponseEntity<>(candidaturaVagaService.cancelaCandidaturaVaga(id), HttpStatus.OK);
    }

    @PutMapping("/altera-status-candidatura-vaga/{id}/{codStatus}")
    public ResponseEntity<CandidaturaVaga> alteraStatusCandidaturaVaga(@PathVariable Long id, @PathVariable Integer codStatus) {
        return new ResponseEntity<>(candidaturaVagaService.alteraStatusCandidaturaVaga(id, codStatus), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiCandidaturaVaga(@PathVariable Long id) {
        return new ResponseEntity<>(candidaturaVagaService.excluiCandidaturaVaga(id), HttpStatus.OK);
    }

}
