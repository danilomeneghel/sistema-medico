package br.com.gndi.vaga.repository;

import br.com.gndi.vaga.entity.CandidaturaVagaStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidaturaVagaStatusRepository extends JpaRepository<CandidaturaVagaStatusEntity, Long> {

    CandidaturaVagaStatusEntity findByCodStatus(Integer codStatus);

    CandidaturaVagaStatusEntity findByCodStatusOrStatus(Integer codStatus, String status);

}
