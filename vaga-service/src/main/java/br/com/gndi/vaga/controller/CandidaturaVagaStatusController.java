package br.com.gndi.vaga.controller;

import br.com.gndi.vaga.model.CandidaturaVagaStatus;
import br.com.gndi.vaga.service.CandidaturaVagaStatusService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/candidatura-vaga-status")
@Api(value = "Status", tags = "Status das Candidaturas das Vagas")
public class CandidaturaVagaStatusController {

    @Autowired
    private CandidaturaVagaStatusService candidaturaVagaStatusService;

    @GetMapping("{codStatus}")
    public ResponseEntity<CandidaturaVagaStatus> localizaCandidaturaVagaStatus(@PathVariable Integer codStatus) {
        return new ResponseEntity<>(candidaturaVagaStatusService.localizaCandidaturaVagaStatus(codStatus), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CandidaturaVagaStatus>> listaCandidaturaVagaStatus() {
        return new ResponseEntity<>(candidaturaVagaStatusService.listaCandidaturaVagaStatus(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CandidaturaVagaStatus> criaCandidaturaVagaStatus(@Valid @RequestBody CandidaturaVagaStatus candidaturaVagaStatus) {
        return new ResponseEntity<>(candidaturaVagaStatusService.criaCandidaturaVagaStatus(candidaturaVagaStatus), HttpStatus.CREATED);
    }

    @PutMapping("{codStatus}")
    public ResponseEntity<CandidaturaVagaStatus> alteraCandidaturaVagaStatus(@PathVariable Integer codStatus, @Valid @RequestBody CandidaturaVagaStatus candidaturaVagaStatus) {
        return new ResponseEntity<>(candidaturaVagaStatusService.alteraCandidaturaVagaStatus(codStatus, candidaturaVagaStatus), HttpStatus.OK);
    }

    @DeleteMapping("{codStatus}")
    public ResponseEntity<String> excluiCandidaturaVagaStatus(@PathVariable Integer codStatus) {
        return new ResponseEntity<>(candidaturaVagaStatusService.excluiCandidaturaVagaStatus(codStatus), HttpStatus.OK);
    }

}
