package br.com.gndi.vaga.client.salesforce.model.errors;

import br.com.gndi.vaga.model.errors.ErrorModel;
import org.springframework.http.HttpStatus;

public class VagaErrors {

    public static final ErrorModel TOKEN_ERROR = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Erro ao gerar o Token do SalesForce");
    public static final ErrorModel STATUS_UPDATE_ERROR = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Erro ao atualizar o status da vaga, por favor tente novamente");

}
