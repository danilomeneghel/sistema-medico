package br.com.gndi.vaga.repository;

import br.com.gndi.vaga.entity.CandidaturaVagaCandidatoEntity;
import br.com.gndi.vaga.entity.CandidaturaVagaEntity;
import br.com.gndi.vaga.entity.CandidaturaVagaStatusEntity;
import br.com.gndi.vaga.model.CandidaturaVaga;
import br.com.gndi.vaga.model.filter.CandidaturaVagaFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface CandidaturaVagaRepository extends JpaRepository<CandidaturaVagaEntity, Long> {

    Optional<CandidaturaVagaEntity> findById(Long id);

    List<CandidaturaVagaEntity> findByCodVagaAndTipoVagaAndStatus(String codVaga, String tipo, CandidaturaVagaStatusEntity status);

    List<CandidaturaVagaEntity> findByCodVagaAndTipoVagaAndCandidato(String codVaga, String tipo, Optional<CandidaturaVagaCandidatoEntity> candidato);
    List<CandidaturaVagaEntity> findByCodVagaAndTipoVaga(String codVaga, String tipo);

    Page<CandidaturaVagaEntity> findAll(Specification<CandidaturaVaga> specification, Pageable pageable);

    static Specification<CandidaturaVaga> filtraCandidaturaVaga(CandidaturaVagaFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodVaga() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codVaga"), filter.getCodVaga()));
            }

            if (filter.getCodStatus() != null) {
                predicates.add(criteriaBuilder.equal(root.get("status").get("codStatus"), filter.getCodStatus()));
            }

            if (filter.getNomeVaga() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeVaga"), "%" + filter.getNomeVaga() + "%"));
            }

            if (filter.getCpf() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("candidato").get("cpf"), filter.getCpf()));
            }

            if (filter.getTipoVaga() != null) {
                predicates.add(criteriaBuilder.equal(root.get("tipoVaga"), filter.getTipoVaga()));
            }

            if (filter.getCentroClinicoVaga() != null) {
                predicates.add(criteriaBuilder.equal(root.get("centroClinicoVaga"), filter.getCentroClinicoVaga()));
            }

            if (filter.getLocalidadeInteresse() != null) {
                predicates.add(criteriaBuilder.equal(root.get("localidadeInteresse"), filter.getLocalidadeInteresse()));
            }

            if (filter.getDataInicioInteresse() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataInicioInteresse"), filter.getDataInicioInteresse()));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    @Modifying
    @Query("UPDATE CandidaturaVagaEntity c SET c.status.id = :idVagaStatus WHERE c.codVaga = :codVaga")
    void updateCandidaturaVagaNaoAprovado(@Param(value = "codVaga") String codVaga, @Param(value = "idVagaStatus") Long idVagaStatus);

}
