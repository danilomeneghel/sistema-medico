package br.com.gndi.vaga.repository;

import br.com.gndi.vaga.entity.CandidaturaVagaCandidatoEntity;
import br.com.gndi.vaga.model.CandidaturaVagaCandidato;
import br.com.gndi.vaga.model.filter.CandidaturaVagaCandidatoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface CandidaturaVagaCandidatoRepository extends JpaRepository<CandidaturaVagaCandidatoEntity, Long> {

    Optional<CandidaturaVagaCandidatoEntity> findById(Long id);

    Optional<CandidaturaVagaCandidatoEntity> findByCpf(String cpf);

    Page<CandidaturaVagaCandidatoEntity> findAll(Specification<CandidaturaVagaCandidato> specification, Pageable pageable);

    static Specification<CandidaturaVagaCandidato> filtraCandidato(CandidaturaVagaCandidatoFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCodEspecialidade() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codEspecialidade"), filter.getCodEspecialidade()));
            }

            if (filter.getCpf() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpf"), filter.getCpf()));
            }

            if (filter.getNome() != null) {
                predicates.add(criteriaBuilder.like(root.get("nome"), "%" + filter.getNome() + "%"));
            }

            if (filter.getNomeEspecialidade() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeEspecialidade"), "%" + filter.getNomeEspecialidade() + "%"));
            }

            if (filter.getAnoFormacao() != null) {
                predicates.add(criteriaBuilder.equal(root.get("anoFormacao"), filter.getAnoFormacao()));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
