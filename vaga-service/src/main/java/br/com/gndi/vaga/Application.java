package br.com.gndi.vaga;

import br.com.gndi.security.jwt.filter.JWTAuthenticationFilter;
import br.com.gndi.security.jwt.service.AuthenticationClientService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("br.com.gndi")
@EntityScan("br.com.gndi")
@EnableJpaRepositories("br.com.gndi")
@EnableFeignClients
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public FilterRegistrationBean<JWTAuthenticationFilter> registerJWTAuthFilter() {
        return new FilterRegistrationBean<JWTAuthenticationFilter>(new JWTAuthenticationFilter());
    }

    @Bean
    public AuthenticationClientService newInstanceAuthService() {
        return new AuthenticationClientService();
    }

}
