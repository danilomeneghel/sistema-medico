package br.com.gndi.vaga.model.errors;

import org.springframework.http.HttpStatus;

public class CommonErrors {

    public static final ErrorModel INVALID_PARAMETERS = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400000", "Invalid parameters");

    public static final ErrorModel UNAUTHORIZED = new ErrorModel(HttpStatus.UNAUTHORIZED.value(), "401000", "Unauthorized");

    public static final ErrorModel UNEXPECTED_ERROR = new ErrorModel(HttpStatus.INTERNAL_SERVER_ERROR.value(), "500000", "Unexpected error");

}
