package br.com.gndi.vaga.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@ApiModel
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CandidaturaVagaCandidato {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Especialidade", example = "x999")
    private String codEspecialidade;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "Nome Candidato", example = "xxxxxxxxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nome;

    @ApiModelProperty(value = "Nome Especialidade", example = "xxxxxxxxxxxxxxxxxxxxx")
    private String nomeEspecialidade;

    @ApiModelProperty(value = "Ano Formação", example = "2001")
    private Integer anoFormacao;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
