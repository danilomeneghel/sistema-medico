package br.com.gndi.vaga.client.salesforce.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatusVagaCobertura {

    private String Observacoes__c;

}
