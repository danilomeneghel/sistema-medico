package br.com.gndi.vaga.model.errors;

import org.springframework.http.HttpStatus;

public class CandidaturaVagaErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Candidatura de vaga não encontrada");

    public static final ErrorModel TYPE_NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Tipo de candidatura de vaga inexistente");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Você já se candidatou a essa vaga");

    public static final ErrorModel VACANCY_FILLED = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Esse plantão já possui um candidato aprovado");

}
