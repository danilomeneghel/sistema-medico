package br.com.gndi.social.mapper;

import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.model.errors.CommonErrors;
import br.com.gndi.social.model.pagination.PaginationInfo;
import br.com.gndi.social.model.pagination.PaginationRequest;
import br.com.gndi.social.model.sort.SortingInfo;
import br.com.gndi.social.utils.OptionalUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@ConditionalOnClass(Pageable.class)
public class PaginationMapper {

    public PaginationInfo toPaginationInfo(Page<?> page) {
        PaginationInfo pageInfo = new PaginationInfo();
        pageInfo.setPageNumber(page.getNumber());
        pageInfo.setPageSize(page.getSize());
        pageInfo.setContentSize(page.getNumberOfElements());
        pageInfo.setTotalElements(page.getTotalElements());
        pageInfo.setTotalPages(page.getTotalPages());
        pageInfo.setHasContent(page.hasContent());
        pageInfo.setHasPreviousPage(page.hasPrevious());
        pageInfo.setHasNextPage(page.hasNext());
        List<SortingInfo> sorting = page.getSort().stream()
                .map(order -> new SortingInfo(order.getProperty(), order.getDirection().name().toLowerCase()))
                .collect(Collectors.toList());
        if (!sorting.isEmpty()) {
            pageInfo.setSorting(sorting);
        }
        return pageInfo;
    }

    public Pageable toPageable(PaginationRequest<?> paginationRequest) {
        Sort sort;
        if (OptionalUtils.isPresentAndNotNull(paginationRequest.getSortBy())) {
            String[] sortByFields = parseCsvString(paginationRequest.getSortBy());
            String[] sortDirections = parseCsvString(paginationRequest.getSortDirection());
            validateFieldsPresence(paginationRequest, sortByFields);
            List<Sort.Order> orders = new ArrayList<>();
            for (int i = 0; i < sortByFields.length; i++) {
                String field = sortByFields[i];
                Sort.Direction direction = sortDirections.length > i ? Sort.Direction.fromString(sortDirections[i]) : Sort.DEFAULT_DIRECTION;
                orders.add(new Sort.Order(direction, field));
            }
            sort = Sort.by(orders);
        } else {
            sort = Sort.unsorted();
        }
        return PageRequest.of(paginationRequest.getPageNumber(), paginationRequest.getPageSize(), sort);
    }

    private String[] parseCsvString(Optional<String> str) {
        if (OptionalUtils.isPresentAndNotNull(str)) {
            return str.get().split("\\s*,\\s*");
        }
        return new String[]{};
    }

    private void validateFieldsPresence(PaginationRequest<?> paginationRequest, String[] sortByFields) {
        Type genericType = paginationRequest.getClass().getGenericSuperclass();
        if (genericType instanceof ParameterizedType) {
            Class<?> typeClass = (Class<?>) ((ParameterizedType) paginationRequest.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            if (!typeClass.isInstance(Object.class)) {
                for (String field : sortByFields) {
                    if (ReflectionUtils.findField(typeClass, field) == null) {
                        throw new ModelException(CommonErrors.INVALID_PARAMETERS);
                    }
                }
            }
        }
    }

}
