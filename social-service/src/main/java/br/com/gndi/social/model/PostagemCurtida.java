package br.com.gndi.social.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostagemCurtida {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "ID Postagem", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idPostagem;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "Curtida", example = "true")
    @NotNull(message = "Campo Obrigatório")
    private Boolean curtida;

}
