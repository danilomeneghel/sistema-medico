package br.com.gndi.social.repository;

import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.entity.PostagemVideoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostagemVideoRepository extends JpaRepository<PostagemVideoEntity, Long> {

    Optional<PostagemVideoEntity> findById(Long id);

    void deleteByNome(String nome);

    void deleteAllByPostagem(PostagemEntity Postagem);

}
