package br.com.gndi.social.service;

import br.com.gndi.social.entity.ComentarioDenunciaEntity;
import br.com.gndi.social.entity.DenunciaStatusEntity;
import br.com.gndi.social.entity.PostagemComentarioEntity;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.mapper.PaginationMapper;
import br.com.gndi.social.model.ComentarioDenuncia;
import br.com.gndi.social.model.DenunciaStatus;
import br.com.gndi.social.model.errors.DenunciaErrors;
import br.com.gndi.social.model.errors.PostagemComentarioErrors;
import br.com.gndi.social.model.errors.PostagemErrors;
import br.com.gndi.social.model.filter.ComentarioDenunciaFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.repository.ComentarioDenunciaRepository;
import br.com.gndi.social.repository.PostagemComentarioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ComentarioDenunciaService {

    @Autowired
    private PostagemComentarioRepository postagemComentarioRepository;

    @Autowired
    private ComentarioDenunciaRepository comentarioDenunciaRepository;

    @Autowired
    private DenunciaTipoService denunciaTipoService;

    @Autowired
    private DenunciaStatusService denunciaStatusService;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private PaginationMapper paginationMapper;

    public ComentarioDenuncia localizaComentarioDenuncia(Long id) {
        Optional<ComentarioDenunciaEntity> denunciaExistente = comentarioDenunciaRepository.findById(id);
        if (!denunciaExistente.isPresent()) {
            throw new ModelException(DenunciaErrors.NOT_FOUND);
        }
        return mapper.map(denunciaExistente.get(), ComentarioDenuncia.class);
    }

    public PaginationResponse<ComentarioDenuncia> pesquisaComentarioDenuncia(ComentarioDenunciaFilter comentarioDenunciaFilter) {
        Pageable pageable = paginationMapper.toPageable(comentarioDenunciaFilter);
        Page<ComentarioDenunciaEntity> pageResult = comentarioDenunciaRepository.findAll(ComentarioDenunciaRepository.filtraComentarioDenuncia(comentarioDenunciaFilter), pageable);
        List<ComentarioDenuncia> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, ComentarioDenuncia.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public ComentarioDenuncia criaComentarioDenuncia(ComentarioDenuncia comentarioDenuncia) {
        // Verifica se o comentário existe
        Optional<PostagemComentarioEntity> postagemComentarioExistente = postagemComentarioRepository.findById(comentarioDenuncia.getIdPostagemComentario());
        if (!postagemComentarioExistente.isPresent()) {
            throw new ModelException(PostagemComentarioErrors.NOT_FOUND);
        }
        // Verifica se o tipo da denúncia existe
        denunciaTipoService.denunciaTipoExistente(comentarioDenuncia.getIdDenunciaTipo());
        // Verifica se o status da denúncia existe
        denunciaStatusService.denunciaStatusExistente(comentarioDenuncia.getIdDenunciaStatus());
        // Verifica se a denúncia da mesma postagem e cpf já foi cadastrada
        Optional<ComentarioDenunciaEntity> comentarioDenunciaExistente = comentarioDenunciaRepository.findByPostagemComentarioAndCpf(postagemComentarioExistente.get(), comentarioDenuncia.getCpf());
        if (comentarioDenunciaExistente.isPresent()) {
            throw new ModelException(DenunciaErrors.DUPLICATE_RECORD);
        }
        ComentarioDenunciaEntity comentarioDenunciaEntity = mapper.map(comentarioDenuncia, ComentarioDenunciaEntity.class);
        comentarioDenunciaEntity = comentarioDenunciaRepository.save(comentarioDenunciaEntity);
        return mapper.map(comentarioDenunciaEntity, ComentarioDenuncia.class);
    }

    public ComentarioDenuncia alteraStatusComentarioDenuncia(Long id, DenunciaStatus denunciaStatus) {
        // Verifica se a denúncia do comentário existe
        Optional<ComentarioDenunciaEntity> comentarioDenunciaExistente = comentarioDenunciaRepository.findById(id);
        if (!comentarioDenunciaExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        // Verifica se o status da denúncia existe
        Optional<DenunciaStatusEntity> denunciaStatusExistente = denunciaStatusService.denunciaStatusExistente(denunciaStatus.getId());
        ComentarioDenunciaEntity comentarioDenunciaEntity = comentarioDenunciaExistente.get();
        comentarioDenunciaEntity.setDenunciaStatus(denunciaStatusExistente.get());
        comentarioDenunciaEntity = comentarioDenunciaRepository.save(comentarioDenunciaEntity);
        return mapper.map(comentarioDenunciaEntity, ComentarioDenuncia.class);
    }

}
