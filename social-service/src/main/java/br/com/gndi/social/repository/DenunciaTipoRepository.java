package br.com.gndi.social.repository;

import br.com.gndi.social.entity.DenunciaTipoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DenunciaTipoRepository extends JpaRepository<DenunciaTipoEntity, Long> {

    Optional<DenunciaTipoEntity> findById(Long id);

}
