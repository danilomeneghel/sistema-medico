package br.com.gndi.social.model.errors;

import org.springframework.http.HttpStatus;

public class DenunciaStatusErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Status da Denúncia não encontrada");
    
}
