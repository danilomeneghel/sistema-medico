package br.com.gndi.social.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostagemDenuncia {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "ID Postagem", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idPostagem;

    @ApiModelProperty(value = "ID Tipo", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idDenunciaTipo;

    @ApiModelProperty(value = "ID Status", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idDenunciaStatus;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "Descrição", example = "xxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String descricao;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
