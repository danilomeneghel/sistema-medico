package br.com.gndi.social.model;

import lombok.Data;

@Data
public class PostagemImagem {

    private String nome;

    private String tipo;

    private long tamanho;

    private String url;
}
