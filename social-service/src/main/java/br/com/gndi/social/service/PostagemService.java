package br.com.gndi.social.service;

import br.com.gndi.social.entity.PostagemCurtidaEntity;
import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.entity.PostagemImagemEntity;
import br.com.gndi.social.entity.PostagemVideoEntity;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.mapper.ArquivoMapper;
import br.com.gndi.social.mapper.PaginationMapper;
import br.com.gndi.social.model.*;
import br.com.gndi.social.model.errors.ArquivoErrors;
import br.com.gndi.social.model.errors.PostagemErrors;
import br.com.gndi.social.model.filter.PostagemFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostagemService implements ArquivoMapper {

    @Value("${azure.storage.blob.url}")
    public String URL;

    @Autowired
    private PostagemRepository postagemRepository;

    @Autowired
    private PostagemImagemRepository postagemImagemRepository;

    @Autowired
    private PostagemVideoRepository postagemVideoRepository;

    @Autowired
    private PostagemCurtidaRepository postagemCurtidaRepository;

    @Autowired
    private PostagemComentarioRepository postagemComentarioRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    @Autowired
    private ArquivoMapper arquivoMapper;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private ArmazenaArquivoService armazenaArquivoService;

    public PostagemLista localizaPostagem(Long id) {
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(id);
        if (!postagemExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        return mapper.map(postagemExistente.get(), PostagemLista.class);
    }

    public PaginationResponse<PostagemLista> pesquisaPostagem(PostagemFilter postagemFilter) {
        Pageable pageable = paginationMapper.toPageable(postagemFilter);
        Page<PostagemEntity> pageResult = postagemRepository.findAll(PostagemRepository.filtraPostagem(postagemFilter), pageable);
        List<PostagemLista> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, PostagemLista.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public PostagemLista criaPostagem(Postagem postagem, MultipartFile[] imagens, MultipartFile[] videos) {
        // Verifica se a mesma postagem já foi cadastrada
        /*Optional<PostagemEntity> postagemExistente = postagemRepository.findByPublicacaoContainingIgnoreCase(postagem.getPublicacao());
        if (postagemExistente.isPresent()) {
            throw new ModelException(PostagemErrors.DUPLICATE_RECORD);
        }*/
        PostagemEntity postagemEntity = mapper.map(postagem, PostagemEntity.class);
        postagemEntity = postagemRepository.save(postagemEntity);
        return setArquivos(postagemEntity, imagens, videos);
    }

    public PostagemLista alteraPostagem(Long id, Postagem postagem, MultipartFile[] imagens, MultipartFile[] videos) {
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(id);
        if (!postagemExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        // Verifica se existem arquivos antigos para excluir antes de salvar os novos
        if (imagens != null && imagens.length > 0) {
            postagemImagemRepository.deleteAllByPostagem(postagemExistente.get());
        }
        if (videos != null && videos.length > 0) {
            postagemVideoRepository.deleteAllByPostagem(postagemExistente.get());
        }
        PostagemEntity postagemEntity = postagemExistente.get();
        postagemEntity.setPublicacao(postagem.getPublicacao());
        postagemEntity = postagemRepository.save(postagemEntity);
        return setArquivos(postagemEntity, imagens, videos);
    }

    public Boolean curtiPostagem(PostagemCurtida postagemCurtida) {
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(postagemCurtida.getIdPostagem());
        if (!postagemExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        PostagemCurtidaEntity postagemCurtidaEntity;
        Optional<PostagemCurtidaEntity> postagemCurtidaExistente = postagemCurtidaRepository.findByPostagemAndCpf(postagemExistente.get(), postagemCurtida.getCpf());
        try {
            if (!postagemCurtidaExistente.isPresent()) {
                postagemCurtidaEntity = new PostagemCurtidaEntity();
            } else {
                postagemCurtidaEntity = postagemCurtidaExistente.get();
            }
            postagemCurtidaEntity.setPostagem(postagemExistente.get());
            postagemCurtidaEntity.setCpf(postagemCurtida.getCpf());
            postagemCurtidaEntity.setCurtida(postagemCurtida.getCurtida());
            postagemCurtidaEntity = postagemCurtidaRepository.save(postagemCurtidaEntity);
            return postagemCurtidaEntity.getCurtida();
        } catch (Exception e) {
            return null;
        }
    }

    public PostagemLista setArquivos(PostagemEntity postagemEntity, MultipartFile[] imagens, MultipartFile[] videos) {
        PostagemLista postagemLista = mapper.map(postagemEntity, PostagemLista.class);
        // Verifica se foi enviado alguma imagem
        if (imagens != null && imagens.length > 0) {
            // Verifica se a quantidade de imagens está acima do máximo permitido
            if (imagens.length > 4) {
                throw new ModelException(ArquivoErrors.MAX_LIMIT);
            }
            postagemLista.setImagens(listaImagens(postagemEntity, imagens));
        }
        // Verifica se foi enviado algum video
        if (videos != null && videos.length > 0) {
            // Verifica se a quantidade de videos está acima do máximo permitido
            if (videos.length > 4) {
                throw new ModelException(ArquivoErrors.MAX_LIMIT);
            }
            postagemLista.setVideos(listaVideos(postagemEntity, videos));
        }
        return postagemLista;
    }

    public List<PostagemImagem> listaImagens(PostagemEntity postagemEntity, MultipartFile[] arquivos) {
        List<PostagemImagem> listaArquivos = new ArrayList<>();
        for (MultipartFile arquivo : arquivos) {
            // Envia o arquivo para o diretório
            String nomeArquivo = armazenaArquivoService.arquivoArmazenamento(arquivo);
            // Gera a url do arquivo
            String urlArquivo = URL + "/" + nomeArquivo;
            // Seta os dados para salvar no banco
            PostagemImagemEntity postagemImagemEntity = arquivoMapper.setImagem(postagemEntity, nomeArquivo, arquivo, urlArquivo);
            postagemImagemRepository.save(postagemImagemEntity);
            PostagemImagem postagemImagem = mapper.map(postagemImagemEntity, PostagemImagem.class);
            listaArquivos.add(postagemImagem);
        }
        return listaArquivos;
    }

    public List<PostagemVideo> listaVideos(PostagemEntity postagemEntity, MultipartFile[] arquivos) {
        List<PostagemVideo> listaArquivos = new ArrayList<>();
        for (MultipartFile arquivo : arquivos) {
            // Envia o arquivo para o diretório
            String nomeArquivo = armazenaArquivoService.arquivoArmazenamento(arquivo);
            // Gera a url do arquivo
            String urlArquivo = URL + "/" + nomeArquivo;
            // Seta os dados para salvar no banco
            PostagemVideoEntity postagemVideoEntity = arquivoMapper.setVideo(postagemEntity, nomeArquivo, arquivo, urlArquivo);
            postagemVideoRepository.save(postagemVideoEntity);
            PostagemVideo postagemVideo = mapper.map(postagemVideoEntity, PostagemVideo.class);
            listaArquivos.add(postagemVideo);
        }
        return listaArquivos;
    }

    public String excluiPostagem(Long id) {
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(id);
        if (!postagemExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        try {
            postagemRepository.deleteById(id);
        } catch (Exception e) {
            return "Erro ao excluir a postagem. %s\n" + e;
        }
        return "Postagem excluída com sucesso";
    }

}
