package br.com.gndi.social.model.errors;

import org.springframework.http.HttpStatus;

public class DenunciaTipoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Tipo da Denúncia não encontrada");

}
