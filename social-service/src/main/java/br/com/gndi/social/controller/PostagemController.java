package br.com.gndi.social.controller;

import br.com.gndi.social.model.Postagem;
import br.com.gndi.social.model.PostagemCurtida;
import br.com.gndi.social.model.PostagemLista;
import br.com.gndi.social.model.filter.PostagemFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.service.PostagemService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping("/postagem")
@Api(value = "Postagens", tags = "Publicações das Postagens")
public class PostagemController {

    @Autowired
    private PostagemService postagemService;

    @GetMapping("{id}")
    public ResponseEntity<PostagemLista> localizaPostagem(@PathVariable Long id) {
        return new ResponseEntity<>(postagemService.localizaPostagem(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<PostagemLista> pesquisaPostagem(@Valid @ModelAttribute PostagemFilter postagemFilter) {
        return postagemService.pesquisaPostagem(postagemFilter);
    }

    @PostMapping(consumes = "multipart/form-data")
    public ResponseEntity<PostagemLista> criaPostagem(@Valid @ModelAttribute Postagem postagem,
                                                      @RequestParam(name = "imagem", required = false) MultipartFile[] imagem,
                                                      @RequestParam(name = "video", required = false) MultipartFile[] video) {
        return new ResponseEntity<>(postagemService.criaPostagem(postagem, imagem, video), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}", consumes = "multipart/form-data")
    public ResponseEntity<PostagemLista> alteraPostagem(@PathVariable Long id,
                                                        @Valid @ModelAttribute Postagem postagem,
                                                        @RequestParam(name = "imagem", required = false) MultipartFile[] imagem,
                                                        @RequestParam(name = "video", required = false) MultipartFile[] video) {
        return new ResponseEntity<>(postagemService.alteraPostagem(id, postagem, imagem, video), HttpStatus.OK);
    }

    @PostMapping(value = "/curtida")
    public ResponseEntity<Boolean> curtiPostagem(@Valid @RequestBody PostagemCurtida postagemCurtida) {
        return new ResponseEntity<>(postagemService.curtiPostagem(postagemCurtida), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiPostagem(@PathVariable Long id) {
        return new ResponseEntity<>(postagemService.excluiPostagem(id), HttpStatus.OK);
    }

}
