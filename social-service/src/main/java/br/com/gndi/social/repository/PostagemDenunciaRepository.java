package br.com.gndi.social.repository;

import br.com.gndi.social.entity.PostagemDenunciaEntity;
import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.model.PostagemDenuncia;
import br.com.gndi.social.model.filter.PostagemDenunciaFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface PostagemDenunciaRepository extends JpaRepository<PostagemDenunciaEntity, Long> {

    Optional<PostagemDenunciaEntity> findById(Long id);

    Optional<PostagemDenunciaEntity> findByPostagemAndCpf(PostagemEntity postagem, String cpf);

    Page<PostagemDenunciaEntity> findAll(Specification<PostagemDenuncia> specification, Pageable pageable);

    static Specification<PostagemDenuncia> filtraPostagemDenuncia(PostagemDenunciaFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdPostagem() != null) {
                predicates.add(criteriaBuilder.equal(root.get("postagem").get("id"), filter.getIdPostagem()));
            }

            if (filter.getIdDenunciaTipo() != null) {
                predicates.add(criteriaBuilder.equal(root.get("denunciaTipo").get("id"), filter.getIdDenunciaTipo()));
            }

            if (filter.getIdDenunciaStatus() != null) {
                predicates.add(criteriaBuilder.equal(root.get("denunciaStatus").get("id"), filter.getIdDenunciaStatus()));
            }

            if (filter.getCpf() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpf"), filter.getCpf()));
            }

            if (filter.getDescricao() != null) {
                predicates.add(criteriaBuilder.like(root.get("descricao"), "%" + filter.getDescricao() + "%"));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
