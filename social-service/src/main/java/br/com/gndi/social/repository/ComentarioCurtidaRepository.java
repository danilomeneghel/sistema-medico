package br.com.gndi.social.repository;

import br.com.gndi.social.entity.ComentarioCurtidaEntity;
import br.com.gndi.social.entity.PostagemComentarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ComentarioCurtidaRepository extends JpaRepository<ComentarioCurtidaEntity, Long> {

    Optional<ComentarioCurtidaEntity> findById(Long id);

    Optional<ComentarioCurtidaEntity> findByPostagemComentarioAndCpf(PostagemComentarioEntity postagemComentario, String cpf);

}
