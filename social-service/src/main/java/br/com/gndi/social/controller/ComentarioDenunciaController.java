package br.com.gndi.social.controller;

import br.com.gndi.social.model.ComentarioDenuncia;
import br.com.gndi.social.model.DenunciaStatus;
import br.com.gndi.social.model.filter.ComentarioDenunciaFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.service.ComentarioDenunciaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/comentario-denuncia")
@Api(value = "Denúncias dos Comentários", tags = "Denúncias dos Comentários")
public class ComentarioDenunciaController extends DenunciaController {

    @Autowired
    private ComentarioDenunciaService comentarioDenunciaService;

    @GetMapping("{id}")
    public ResponseEntity<ComentarioDenuncia> localizaComentarioDenuncia(@PathVariable Long id) {
        return new ResponseEntity<>(comentarioDenunciaService.localizaComentarioDenuncia(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<ComentarioDenuncia> pesquisaComentarioDenuncia(@Valid @ModelAttribute ComentarioDenunciaFilter comentarioDenunciaFilter) {
        return comentarioDenunciaService.pesquisaComentarioDenuncia(comentarioDenunciaFilter);
    }

    @PostMapping
    public ResponseEntity<ComentarioDenuncia> criaComentarioDenuncia(@Valid @RequestBody ComentarioDenuncia comentarioDenuncia) {
        return new ResponseEntity<>(comentarioDenunciaService.criaComentarioDenuncia(comentarioDenuncia), HttpStatus.CREATED);
    }

    @PutMapping(value = "/status/{id}")
    public ResponseEntity<ComentarioDenuncia> alteraStatusComentarioDenuncia(@PathVariable Long id, @Valid @RequestBody DenunciaStatus denunciaStatus) {
        return new ResponseEntity<>(comentarioDenunciaService.alteraStatusComentarioDenuncia(id, denunciaStatus), HttpStatus.OK);
    }

}
