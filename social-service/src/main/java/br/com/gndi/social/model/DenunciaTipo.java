package br.com.gndi.social.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DenunciaTipo {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Tipo", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codTipo;

    @ApiModelProperty(value = "Tipo", example = "xxxxxxxxx")
    private String tipo;

}
