package br.com.gndi.social.service;

import br.com.gndi.social.entity.DenunciaStatusEntity;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.model.DenunciaStatus;
import br.com.gndi.social.model.errors.DenunciaStatusErrors;
import br.com.gndi.social.repository.DenunciaStatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DenunciaStatusService {

    @Autowired
    private DenunciaStatusRepository denunciaStatusRepository;

    private ModelMapper mapper = new ModelMapper();

    public List<DenunciaStatus> listaDenunciaStatus() {
        List<DenunciaStatusEntity> listaDenunciaStatusEntity = denunciaStatusRepository.findAll();
        List<DenunciaStatus> listaDenunciaStatus = listaDenunciaStatusEntity.stream().map(entity -> mapper.map(entity, DenunciaStatus.class)).collect(Collectors.toList());
        return listaDenunciaStatus;
    }

    public Optional<DenunciaStatusEntity> denunciaStatusExistente(Long idDenunciaStatus) {
        Optional<DenunciaStatusEntity> denunciaStatusExistente = denunciaStatusRepository.findById(idDenunciaStatus);
        if (!denunciaStatusExistente.isPresent()) {
            throw new ModelException(DenunciaStatusErrors.NOT_FOUND);
        }
        return denunciaStatusExistente;
    }

}
