package br.com.gndi.social.repository;

import br.com.gndi.social.entity.PostagemComentarioEntity;
import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.model.PostagemComentario;
import br.com.gndi.social.model.filter.PostagemComentarioFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface PostagemComentarioRepository extends JpaRepository<PostagemComentarioEntity, Long> {

    Optional<PostagemComentarioEntity> findById(Long id);

    Optional<PostagemComentarioEntity> findByPostagemAndComentarioContainingIgnoreCase(PostagemEntity postagem, String comentario);

    void deleteByPostagem(PostagemEntity postagem);

    Page<PostagemComentarioEntity> findAll(Specification<PostagemComentario> specification, Pageable pageable);

    static Specification<PostagemComentario> filtraPostagemComentario(PostagemComentarioFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdPostagem() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idPostagem"), filter.getIdPostagem()));
            }

            if (filter.getCpf() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpf"), filter.getCpf()));
            }

            if (filter.getNome() != null) {
                predicates.add(criteriaBuilder.like(root.get("nome"), "%" + filter.getNome() + "%"));
            }

            if (filter.getEspecialidade() != null) {
                predicates.add(criteriaBuilder.like(root.get("especialidade"), "%" + filter.getEspecialidade() + "%"));
            }

            if (filter.getComentario() != null) {
                predicates.add(criteriaBuilder.like(root.get("comentario"), "%" + filter.getComentario() + "%"));
            }

            if (filter.getAtivo() != null) {
                predicates.add(criteriaBuilder.equal(root.get("ativo"), filter.getAtivo()));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
