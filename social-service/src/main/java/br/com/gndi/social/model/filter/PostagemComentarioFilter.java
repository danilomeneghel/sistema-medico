package br.com.gndi.social.model.filter;

import br.com.gndi.social.model.PostagemComentario;
import br.com.gndi.social.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class PostagemComentarioFilter extends PaginationRequest<PostagemComentario> {

    @ApiModelProperty(value = "ID Postagem", example = "9")
    private Long idPostagem;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "Nome", example = "xxxxxxxxxxxxx")
    private String nome;

    @ApiModelProperty(value = "Especialidade", example = "xxxxxxxxxxxxx")
    private String especialidade;

    @ApiModelProperty(value = "Comentário", example = "xxxxxxxxxxxxxxx")
    private String comentario;

    @ApiModelProperty(value = "Ativo", example = "True")
    private Boolean ativo;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdTo;

}
