package br.com.gndi.social.utils;


import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.model.errors.ArquivoErrors;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.blob.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Component
public class AzureUtils {

    @Value("${azure.storage.blob.connect}")
    public String CONNECT_STR;

    @Value("${azure.storage.blob.container.name}")
    public String CONTAINER_NAME;

    public void uploadFile(String localPath, String fileName, MultipartFile file) {
        try {
            File sourceFile = new File(localPath + "/" + fileName);
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(CONNECT_STR);
            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
            CloudBlobContainer container = blobClient.getContainerReference(CONTAINER_NAME);
            container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(), new OperationContext());
            CloudBlockBlob blob = container.getBlockBlobReference(sourceFile.getName());
            BlobProperties props = blob.getProperties();
            props.setContentType(file.getContentType());
            blob.uploadFromFile(sourceFile.getAbsolutePath());
        } catch (Exception e) {
            throw new ModelException(ArquivoErrors.NOT_FOUND);
        }
    }

}
