package br.com.gndi.social.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "postagem_comentario")
@Data
public class PostagemComentarioEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_postagem", nullable = false)
    private PostagemEntity postagem;

    @OneToMany(mappedBy = "postagemComentario", cascade = CascadeType.REMOVE)
    private List<ComentarioCurtidaEntity> curtidas;

    @OneToMany(mappedBy = "postagemComentario")
    private List<ComentarioDenunciaEntity> denuncias;

    private String cpf;

    private String nome;

    private String especialidade;

    @Size(max = 600)
    private String comentario;

    private Boolean ativo;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
