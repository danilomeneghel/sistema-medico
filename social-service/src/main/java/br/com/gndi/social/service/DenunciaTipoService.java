package br.com.gndi.social.service;

import br.com.gndi.social.entity.DenunciaTipoEntity;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.model.DenunciaTipo;
import br.com.gndi.social.model.errors.DenunciaTipoErrors;
import br.com.gndi.social.repository.DenunciaTipoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DenunciaTipoService {

    @Autowired
    private DenunciaTipoRepository denunciaTipoRepository;

    private ModelMapper mapper = new ModelMapper();

    public List<DenunciaTipo> listaDenunciaTipo() {
        List<DenunciaTipoEntity> listaDenunciaTipoEntity = denunciaTipoRepository.findAll();
        List<DenunciaTipo> listaDenunciaTipo = listaDenunciaTipoEntity.stream().map(entity -> mapper.map(entity, DenunciaTipo.class)).collect(Collectors.toList());
        return listaDenunciaTipo;
    }

    public Optional<DenunciaTipoEntity> denunciaTipoExistente(Long idDenunciaTipo) {
        Optional<DenunciaTipoEntity> denunciaTipoExistente = denunciaTipoRepository.findById(idDenunciaTipo);
        if (!denunciaTipoExistente.isPresent()) {
            throw new ModelException(DenunciaTipoErrors.NOT_FOUND);
        }
        return denunciaTipoExistente;
    }

}
