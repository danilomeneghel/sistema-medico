package br.com.gndi.social.model.errors;

import org.springframework.http.HttpStatus;

public class PostagemComentarioErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Comentário não encontrado");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Comentário já cadastrado");

}
