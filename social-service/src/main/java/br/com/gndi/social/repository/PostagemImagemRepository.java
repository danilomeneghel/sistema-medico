package br.com.gndi.social.repository;

import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.entity.PostagemImagemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostagemImagemRepository extends JpaRepository<PostagemImagemEntity, Long> {

    Optional<PostagemImagemEntity> findById(Long id);

    void deleteByNome(String nome);

    void deleteAllByPostagem(PostagemEntity Postagem);

}
