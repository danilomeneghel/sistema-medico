package br.com.gndi.social.service;

import br.com.gndi.social.entity.DenunciaStatusEntity;
import br.com.gndi.social.entity.PostagemDenunciaEntity;
import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.mapper.PaginationMapper;
import br.com.gndi.social.model.DenunciaStatus;
import br.com.gndi.social.model.PostagemDenuncia;
import br.com.gndi.social.model.errors.DenunciaErrors;
import br.com.gndi.social.model.errors.PostagemErrors;
import br.com.gndi.social.model.filter.PostagemDenunciaFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.repository.PostagemDenunciaRepository;
import br.com.gndi.social.repository.PostagemRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostagemDenunciaService {

    @Autowired
    private PostagemRepository postagemRepository;

    @Autowired
    private PostagemDenunciaRepository postagemDenunciaRepository;

    @Autowired
    private DenunciaTipoService denunciaTipoService;

    @Autowired
    private DenunciaStatusService denunciaStatusService;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public PostagemDenuncia localizaPostagemDenuncia(Long id) {
        Optional<PostagemDenunciaEntity> denunciaExistente = postagemDenunciaRepository.findById(id);
        if (!denunciaExistente.isPresent()) {
            throw new ModelException(DenunciaErrors.NOT_FOUND);
        }
        return mapper.map(denunciaExistente.get(), PostagemDenuncia.class);
    }

    public PaginationResponse<PostagemDenuncia> pesquisaPostagemDenuncia(PostagemDenunciaFilter postagemDenunciaFilter) {
        Pageable pageable = paginationMapper.toPageable(postagemDenunciaFilter);
        Page<PostagemDenunciaEntity> pageResult = postagemDenunciaRepository.findAll(PostagemDenunciaRepository.filtraPostagemDenuncia(postagemDenunciaFilter), pageable);
        List<PostagemDenuncia> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, PostagemDenuncia.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public PostagemDenuncia criaPostagemDenuncia(PostagemDenuncia postagemDenuncia) {
        // Verifica se a postagem existe
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(postagemDenuncia.getIdPostagem());
        if (!postagemExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        // Verifica se o tipo da denúncia existe
        denunciaTipoService.denunciaTipoExistente(postagemDenuncia.getIdDenunciaTipo());
        // Verifica se o status da denúncia existe
        denunciaStatusService.denunciaStatusExistente(postagemDenuncia.getIdDenunciaStatus());
        // Verifica se a denúncia da mesma postagem e cpf já foi cadastrada
        Optional<PostagemDenunciaEntity> postagemDenunciaExistente = postagemDenunciaRepository.findByPostagemAndCpf(postagemExistente.get(), postagemDenuncia.getCpf());
        if (postagemDenunciaExistente.isPresent()) {
            throw new ModelException(DenunciaErrors.DUPLICATE_RECORD);
        }
        PostagemDenunciaEntity postagemDenunciaEntity = mapper.map(postagemDenuncia, PostagemDenunciaEntity.class);
        postagemDenunciaEntity = postagemDenunciaRepository.save(postagemDenunciaEntity);
        return mapper.map(postagemDenunciaEntity, PostagemDenuncia.class);
    }

    public PostagemDenuncia alteraStatusPostagemDenuncia(Long id, DenunciaStatus denunciaStatus) {
        // Verifica se a denúncia da postagem existe
        Optional<PostagemDenunciaEntity> postagemDenunciaExistente = postagemDenunciaRepository.findById(id);
        if (!postagemDenunciaExistente.isPresent()) {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
        // Verifica se o status da denúncia existe
        Optional<DenunciaStatusEntity> denunciaStatusExistente = denunciaStatusService.denunciaStatusExistente(denunciaStatus.getId());
        PostagemDenunciaEntity postagemDenunciaEntity = postagemDenunciaExistente.get();
        postagemDenunciaEntity.setDenunciaStatus(denunciaStatusExistente.get());
        postagemDenunciaEntity = postagemDenunciaRepository.save(postagemDenunciaEntity);
        return mapper.map(postagemDenunciaEntity, PostagemDenuncia.class);
    }

}
