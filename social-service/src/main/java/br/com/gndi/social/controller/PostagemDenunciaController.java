package br.com.gndi.social.controller;

import br.com.gndi.social.model.DenunciaStatus;
import br.com.gndi.social.model.PostagemDenuncia;
import br.com.gndi.social.model.filter.PostagemDenunciaFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.service.PostagemDenunciaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/postagem-denuncia")
@Api(value = "Denúncias das Postagens", tags = "Denúncias das Postagens")
public class PostagemDenunciaController extends DenunciaController {

    @Autowired
    private PostagemDenunciaService postagemDenunciaService;

    @GetMapping("{id}")
    public ResponseEntity<PostagemDenuncia> localizaPostagemDenuncia(@PathVariable Long id) {
        return new ResponseEntity<>(postagemDenunciaService.localizaPostagemDenuncia(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<PostagemDenuncia> pesquisaPostagemDenuncia(@Valid @ModelAttribute PostagemDenunciaFilter postagemDenunciaFilter) {
        return postagemDenunciaService.pesquisaPostagemDenuncia(postagemDenunciaFilter);
    }

    @PostMapping
    public ResponseEntity<PostagemDenuncia> criaPostagemDenuncia(@Valid @RequestBody PostagemDenuncia postagemDenuncia) {
        return new ResponseEntity<>(postagemDenunciaService.criaPostagemDenuncia(postagemDenuncia), HttpStatus.CREATED);
    }

    @PutMapping(value = "/status/{id}")
    public ResponseEntity<PostagemDenuncia> alteraStatusPostagemDenuncia(@PathVariable Long id, @Valid @RequestBody DenunciaStatus denunciaStatus) {
        return new ResponseEntity<>(postagemDenunciaService.alteraStatusPostagemDenuncia(id, denunciaStatus), HttpStatus.OK);
    }

}
