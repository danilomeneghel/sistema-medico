package br.com.gndi.social.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comentario_denuncia")
@Data
public class ComentarioDenunciaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_postagem_comentario", nullable = false)
    private PostagemComentarioEntity postagemComentario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_denuncia_tipo", nullable = false)
    private DenunciaTipoEntity denunciaTipo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_denuncia_status", nullable = false)
    private DenunciaStatusEntity denunciaStatus;

    private String cpf;

    private String descricao;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
