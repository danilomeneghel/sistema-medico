package br.com.gndi.social.utils;

import java.util.Optional;

public class OptionalUtils {

    /*
        Uso do Optional<?> no JSON de REQUEST:
        getField() == null -> Campo nao informado (isNotPresent)
        getField().isEmpty() -> Campo informado como null (isPresentButNull)
        getField().isPresent() -> Campo informado com valor (isPresentAndNotNull)
     */

    public static boolean isNotPresent(Optional<?> optionalParam) {
        return optionalParam == null;
    }

    public static boolean isPresent(Optional<?> optionalParam) {
        return optionalParam != null;
    }

    /*public static boolean isPresentButNull(Optional<?> optionalParam) {
        return isPresent(optionalParam) && optionalParam.isEmpty();
    }*/

    public static boolean isPresentAndNotNull(Optional<?> optionalParam) {
        return isPresent(optionalParam) && optionalParam.isPresent();
    }

}
