package br.com.gndi.social.controller;

import br.com.gndi.social.model.DenunciaStatus;
import br.com.gndi.social.model.DenunciaTipo;
import br.com.gndi.social.service.DenunciaStatusService;
import br.com.gndi.social.service.DenunciaTipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public class DenunciaController {

    @Autowired
    private DenunciaTipoService denunciaTipoService;

    @Autowired
    private DenunciaStatusService denunciaStatusService;

    @GetMapping("/tipo")
    public ResponseEntity<List<DenunciaTipo>> listaDenunciaTipo() {
        return ResponseEntity.ok(denunciaTipoService.listaDenunciaTipo());
    }

    @GetMapping("/status")
    public ResponseEntity<List<DenunciaStatus>> listaDenunciaStatus() {
        return ResponseEntity.ok(denunciaStatusService.listaDenunciaStatus());
    }

}
