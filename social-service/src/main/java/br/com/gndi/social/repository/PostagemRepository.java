package br.com.gndi.social.repository;

import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.model.Postagem;
import br.com.gndi.social.model.filter.PostagemFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface PostagemRepository extends JpaRepository<PostagemEntity, Long> {

    Optional<PostagemEntity> findById(Long id);

    Optional<PostagemEntity> findByPublicacaoContainingIgnoreCase(String publicacao);

    Page<PostagemEntity> findAll(Specification<Postagem> specification, Pageable pageable);

    static Specification<Postagem> filtraPostagem(PostagemFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getCpf() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpf"), filter.getCpf()));
            }

            if (filter.getNome() != null) {
                predicates.add(criteriaBuilder.like(root.get("nome"), "%" + filter.getNome() + "%"));
            }

            if (filter.getEspecialidade() != null) {
                predicates.add(criteriaBuilder.like(root.get("especialidade"), "%" + filter.getEspecialidade() + "%"));
            }

            if (filter.getPublicacao() != null) {
                predicates.add(criteriaBuilder.like(root.get("publicacao"), "%" + filter.getPublicacao() + "%"));
            }

            if (filter.getAtivo() != null) {
                predicates.add(criteriaBuilder.equal(root.get("ativo"), filter.getAtivo()));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
