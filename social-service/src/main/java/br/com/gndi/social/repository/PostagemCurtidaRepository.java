package br.com.gndi.social.repository;

import br.com.gndi.social.entity.PostagemCurtidaEntity;
import br.com.gndi.social.entity.PostagemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostagemCurtidaRepository extends JpaRepository<PostagemCurtidaEntity, Long> {

    Optional<PostagemCurtidaEntity> findById(Long id);

    Optional<PostagemCurtidaEntity> findByPostagemAndCpf(PostagemEntity postagem, String cpf);

}
