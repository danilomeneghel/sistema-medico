package br.com.gndi.social.model.errors;

import org.springframework.http.HttpStatus;

public class PostagemErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Postagem não encontrada");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Postagem já cadastrada");

}
