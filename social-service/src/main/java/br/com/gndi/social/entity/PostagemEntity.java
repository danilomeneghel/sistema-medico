package br.com.gndi.social.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "postagem")
@Data
public class PostagemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String cpf;

    private String nome;

    private String especialidade;

    @Size(max = 600)
    private String publicacao;

    private Boolean ativo;

    @OneToMany(mappedBy = "postagem", cascade = CascadeType.REMOVE)
    private List<PostagemImagemEntity> imagens;

    @OneToMany(mappedBy = "postagem", cascade = CascadeType.REMOVE)
    private List<PostagemVideoEntity> videos;

    @OneToMany(mappedBy = "postagem", cascade = CascadeType.REMOVE)
    private List<PostagemComentarioEntity> comentarios;

    @OneToMany(mappedBy = "postagem", cascade = CascadeType.REMOVE)
    private List<PostagemCurtidaEntity> curtidas;

    @OneToMany(mappedBy = "postagem")
    private List<PostagemDenunciaEntity> denuncias;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

}
