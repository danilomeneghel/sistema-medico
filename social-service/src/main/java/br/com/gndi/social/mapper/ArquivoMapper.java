package br.com.gndi.social.mapper;

import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.entity.PostagemImagemEntity;
import br.com.gndi.social.entity.PostagemVideoEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface ArquivoMapper {

    default PostagemImagemEntity setImagem(PostagemEntity postagemEntity, String nomeArquivo, MultipartFile arquivo, String urlArquivo) {
        PostagemImagemEntity postagemImagemEntity = new PostagemImagemEntity();
        postagemImagemEntity.setPostagem(postagemEntity);
        postagemImagemEntity.setNome(nomeArquivo);
        postagemImagemEntity.setTipo(arquivo.getContentType());
        postagemImagemEntity.setTamanho(arquivo.getSize());
        postagemImagemEntity.setUrl(urlArquivo);
        return postagemImagemEntity;
    }

    default PostagemVideoEntity setVideo(PostagemEntity postagemEntity, String nomeArquivo, MultipartFile arquivo, String urlArquivo) {
        PostagemVideoEntity postagemVideoEntity = new PostagemVideoEntity();
        postagemVideoEntity.setPostagem(postagemEntity);
        postagemVideoEntity.setNome(nomeArquivo);
        postagemVideoEntity.setTipo(arquivo.getContentType());
        postagemVideoEntity.setTamanho(arquivo.getSize());
        postagemVideoEntity.setUrl(urlArquivo);
        return postagemVideoEntity;
    }

}
