package br.com.gndi.social.controller;

import br.com.gndi.social.model.ComentarioCurtida;
import br.com.gndi.social.model.PostagemComentario;
import br.com.gndi.social.model.filter.PostagemComentarioFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.service.PostagemComentarioService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/postagem-comentario")
@Api(value = "Comentários", tags = "Comentários das Postagens")
public class PostagemComentarioController {

    @Autowired
    private PostagemComentarioService postagemComentarioService;

    @GetMapping("{id}")
    public ResponseEntity<PostagemComentario> localizaComentario(@PathVariable Long id) {
        return new ResponseEntity<>(postagemComentarioService.localizaComentario(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<PostagemComentario> pesquisaComentario(@Valid @ModelAttribute PostagemComentarioFilter postagemComentarioFilter) {
        return postagemComentarioService.pesquisaComentario(postagemComentarioFilter);
    }

    @PostMapping
    public ResponseEntity<PostagemComentario> criaComentario(@Valid @RequestBody PostagemComentario postagemComentario) {
        return new ResponseEntity<>(postagemComentarioService.criaComentario(postagemComentario), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<PostagemComentario> alteraComentario(@PathVariable Long id, @Valid @RequestBody PostagemComentario postagemComentario) {
        return new ResponseEntity<>(postagemComentarioService.alteraComentario(id, postagemComentario), HttpStatus.OK);
    }

    @PostMapping(value = "/curtida")
    public ResponseEntity<Boolean> curtiComentario(@Valid @RequestBody ComentarioCurtida comentarioCurtida) {
        return new ResponseEntity<>(postagemComentarioService.curtiComentario(comentarioCurtida), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiComentario(@PathVariable Long id) {
        return new ResponseEntity<>(postagemComentarioService.excluiComentario(id), HttpStatus.OK);
    }

}
