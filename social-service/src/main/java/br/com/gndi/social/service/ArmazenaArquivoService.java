package br.com.gndi.social.service;

import br.com.gndi.social.exception.FileNotFoundException;
import br.com.gndi.social.exception.FileStorageException;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.model.errors.ArquivoErrors;
import br.com.gndi.social.utils.AzureUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.*;
import java.util.UUID;

@Service
public class ArmazenaArquivoService {

    @Value("${file.dir-image}")
    public String DIR_IMAGE;

    @Value("${file.dir-video}")
    public String DIR_VIDEO;

    @Value("${file.extensions-image}")
    private String EXTENSIONS_IMAGE;

    @Value("${file.extensions-video}")
    private String EXTENSIONS_VIDEO;

    @Autowired
    private AzureUtils azureUtils;

    public String arquivoArmazenamento(MultipartFile arquivo) {
        // Cria o diretório para enviar o arquivo, caso ainda não esteja criado
        try {
            Files.createDirectories(Paths.get(DIR_IMAGE));
            Files.createDirectories(Paths.get(DIR_VIDEO));
        } catch (Exception ex) {
            throw new FileStorageException("Não foi possível criar o diretório em que os arquivos enviados serão armazenados.", ex);
        }

        // Gera um codigo aleatório para ser o nome do arquivo sem repetir
        String nomeArquivo = UUID.randomUUID().toString().replace("-", "");

        String extensaoArquivo = StringUtils.getFilenameExtension(arquivo.getOriginalFilename());
        Path novoNomeArquivo = Paths.get(nomeArquivo + "." + extensaoArquivo);

        // Verifica se o nome do arquivo contém caracteres inválidos
        if (nomeArquivo.contains("..")) {
            throw new FileStorageException("Desculpe! Nome do arquivo contém sequência de caminho inválido " + nomeArquivo);
        }

        // Verifica se a extensão do arquivo é válida
        if (EXTENSIONS_IMAGE.contains(extensaoArquivo)) {
            //Envia a imagem para o diretório local
            this.copyFile(DIR_IMAGE, novoNomeArquivo, arquivo);
            //Envia a imagem para Azure Blob Storage
            azureUtils.uploadFile(DIR_IMAGE, novoNomeArquivo.toString(), arquivo);
            //Exclui o arquivo do diretório local
            excluiArquivo(novoNomeArquivo.toString(), extensaoArquivo);
        } else if (EXTENSIONS_VIDEO.contains(extensaoArquivo)) {
            //Envia o vídeo para o diretório local
            this.copyFile(DIR_VIDEO, novoNomeArquivo, arquivo);
            //Envia o vídeo para Azure Blob Storage
            azureUtils.uploadFile(DIR_VIDEO, novoNomeArquivo.toString(), arquivo);
            //Exclui o arquivo do diretório local
            excluiArquivo(novoNomeArquivo.toString(), extensaoArquivo);
        } else {
            throw new ModelException(ArquivoErrors.INVALID_EXT);
        }
        return novoNomeArquivo.getFileName().toString();
    }

    public String excluiArquivo(String novoNomeArquivo, String extensaoArquivo) {
        Path path = null;
        if (EXTENSIONS_IMAGE.contains(extensaoArquivo)) {
            path = Paths.get(DIR_IMAGE + "/" + novoNomeArquivo);
        } else if (EXTENSIONS_VIDEO.contains(extensaoArquivo)) {
            path = Paths.get(DIR_VIDEO + "/" + novoNomeArquivo);
        }
        try {
            // Exclui arquivo do diretório
            Files.delete(path);
        } catch (NoSuchFileException ex) {
            return "Arquivo não encontrado: %s\n" + path;
        } catch (IOException ex) {
            return String.valueOf(ex);
        }
        return null;
    }

    public Resource carregaArquivo(String nomeArquivo) {
        String extensaoArquivo = StringUtils.getFilenameExtension(nomeArquivo);
        // Verifica qual é a extensão do arquivo
        if (EXTENSIONS_IMAGE.contains(extensaoArquivo)) {
            return this.getFile(DIR_IMAGE, nomeArquivo);
        } else if (EXTENSIONS_VIDEO.contains(extensaoArquivo)) {
            return this.getFile(DIR_VIDEO, nomeArquivo);
        } else {
            throw new ModelException(ArquivoErrors.INVALID_EXT);
        }
    }

    public void copyFile(String dir, Path novoNomeArquivo, MultipartFile arquivo) {
        // Copia o arquivo para o local de destino
        try {
            Path targetLocation = Paths.get(dir).resolve(novoNomeArquivo);
            Files.copy(arquivo.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileStorageException("Não foi possivel armazenar o arquivo " + arquivo.getOriginalFilename() + ". Por favor tente novamente", ex);
        }
    }

    public Resource getFile(String dir, String nomeArquivo) {
        // Pega o arquivo do local de destino
        try {
            Path caminhoArquivo = Paths.get(dir).resolve(nomeArquivo).normalize();
            Resource resource = new UrlResource(caminhoArquivo.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("Arquivo não encontrado " + nomeArquivo);
            }
        } catch (IOException ex) {
            throw new FileStorageException("Não foi possivel localizar o arquivo " + nomeArquivo + ". Por favor tente novamente", ex);
        }
    }

}