package br.com.gndi.social.controller;

import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.model.errors.ArquivoErrors;
import br.com.gndi.social.service.ArmazenaArquivoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@Api(value = "Carrega Arquivo", tags = "Carregamento de arquivo enviado")
public class CarregaArquivoController {

    @Autowired
    private ArmazenaArquivoService armazenaArquivoService;

    @GetMapping(path = "/arquivo/{nomeArquivo:.+}")
    public ResponseEntity<Resource> carregaArquivo(@PathVariable String nomeArquivo, HttpServletRequest request) {
        // Carrega o arquivo como recurso
        Resource resource = armazenaArquivoService.carregaArquivo(nomeArquivo);
        // Tenta determinar o tipo de conteúdo do arquivo
        String contentType = null;
        try {
            Path path = new File(resource.getFile().getAbsolutePath()).toPath();
            contentType = Files.probeContentType(path);
        } catch (IOException ex) {
            throw new ModelException(ArquivoErrors.NOT_FOUND);
        }
        // Atribui o tipo de conteúdo padrão, se o tipo não puder ser determinado
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}