package br.com.gndi.social.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "denuncia_tipo")
@Data
public class DenunciaTipoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codTipo;

    private String tipo;

}
