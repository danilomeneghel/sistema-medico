package br.com.gndi.social.repository;

import br.com.gndi.social.entity.DenunciaStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DenunciaStatusRepository extends JpaRepository<DenunciaStatusEntity, Long> {

    Optional<DenunciaStatusEntity> findById(Long id);

}
