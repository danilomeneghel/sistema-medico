package br.com.gndi.social.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DenunciaStatus {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Status", example = "xxxxxxxxx")
    @NotNull(message = "Campo Obrigatório")
    private Integer codStatus;

    @ApiModelProperty(value = "Status", example = "xxxxxxxxx")
    private String status;

}
