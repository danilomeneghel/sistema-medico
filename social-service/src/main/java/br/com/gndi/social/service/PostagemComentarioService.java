package br.com.gndi.social.service;

import br.com.gndi.social.entity.ComentarioCurtidaEntity;
import br.com.gndi.social.entity.PostagemComentarioEntity;
import br.com.gndi.social.entity.PostagemEntity;
import br.com.gndi.social.exception.ModelException;
import br.com.gndi.social.mapper.PaginationMapper;
import br.com.gndi.social.model.ComentarioCurtida;
import br.com.gndi.social.model.PostagemComentario;
import br.com.gndi.social.model.errors.PostagemComentarioErrors;
import br.com.gndi.social.model.errors.PostagemErrors;
import br.com.gndi.social.model.filter.PostagemComentarioFilter;
import br.com.gndi.social.model.pagination.PaginationResponse;
import br.com.gndi.social.repository.ComentarioCurtidaRepository;
import br.com.gndi.social.repository.PostagemComentarioRepository;
import br.com.gndi.social.repository.PostagemRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostagemComentarioService {

    @Autowired
    private PostagemComentarioRepository postagemComentarioRepository;

    @Autowired
    private ComentarioCurtidaRepository comentarioCurtidaRepository;

    @Autowired
    private PostagemRepository postagemRepository;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private ArmazenaArquivoService armazenaArquivoService;

    public PostagemComentario localizaComentario(Long id) {
        Optional<PostagemComentarioEntity> postagemComentarioExistente = postagemComentarioRepository.findById(id);
        if (!postagemComentarioExistente.isPresent()) {
            throw new ModelException(PostagemComentarioErrors.NOT_FOUND);
        }
        return mapper.map(postagemComentarioExistente.get(), PostagemComentario.class);
    }

    public PaginationResponse<PostagemComentario> pesquisaComentario(PostagemComentarioFilter postagemComentarioFilter) {
        Pageable pageable = paginationMapper.toPageable(postagemComentarioFilter);
        Page<PostagemComentarioEntity> pageResult = postagemComentarioRepository.findAll(PostagemComentarioRepository.filtraPostagemComentario(postagemComentarioFilter), pageable);
        List<PostagemComentario> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, PostagemComentario.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public PostagemComentario criaComentario(PostagemComentario postagemComentario) {
        // Verifica se o id da postagem existe
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(postagemComentario.getIdPostagem());
        if (postagemExistente.isPresent()) {
            // Verifica se o comentário para a mesma postagem já foi cadastrada
            /*Optional<PostagemComentarioEntity> postagemComentarioExistente = postagemComentarioRepository.findByPostagemAndComentarioContainingIgnoreCase(postagemExistente.get(), postagemComentario.getComentario());
            if (postagemComentarioExistente.isPresent()) {
                throw new ModelException(PostagemComentarioErrors.DUPLICATE_RECORD);
            }*/
            PostagemComentarioEntity postagemComentarioEntity = mapper.map(postagemComentario, PostagemComentarioEntity.class);
            postagemComentarioEntity = postagemComentarioRepository.save(postagemComentarioEntity);
            return mapper.map(postagemComentarioEntity, PostagemComentario.class);
        } else {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
    }

    public PostagemComentario alteraComentario(Long id, PostagemComentario postagemComentario) {
        // Verifica se o id da postagem existe
        Optional<PostagemEntity> postagemExistente = postagemRepository.findById(postagemComentario.getIdPostagem());
        if (postagemExistente.isPresent()) {
            // Verifica se o id do comentário existe
            Optional<PostagemComentarioEntity> postagemComentarioExistente = postagemComentarioRepository.findById(id);
            if (!postagemComentarioExistente.isPresent()) {
                throw new ModelException(PostagemComentarioErrors.NOT_FOUND);
            }
            // Verifica se o comentário para a mesma postagem já foi cadastrada
            /*Optional<PostagemComentarioEntity> postagemComentarioDuplicado = postagemComentarioRepository.findByPostagemAndComentarioContainingIgnoreCase(postagemExistente.get(), postagemComentario.getComentario());
            if (postagemComentarioDuplicado.isPresent()) {
                throw new ModelException(PostagemComentarioErrors.DUPLICATE_RECORD);
            }*/
            // Localiza o comentário para realizar a alteração
            Optional<PostagemComentarioEntity> comentarioExistente = postagemComentarioRepository.findById(id);
            PostagemComentarioEntity postagemComentarioEntity = comentarioExistente.get();
            postagemComentarioEntity.setComentario(postagemComentario.getComentario());
            postagemComentarioEntity.setAtivo(postagemComentario.getAtivo());
            postagemComentarioEntity = postagemComentarioRepository.save(postagemComentarioEntity);
            return mapper.map(postagemComentarioEntity, PostagemComentario.class);
        } else {
            throw new ModelException(PostagemErrors.NOT_FOUND);
        }
    }

    public Boolean curtiComentario(ComentarioCurtida comentarioCurtida) {
        Optional<PostagemComentarioEntity> postagemComentarioExistente = postagemComentarioRepository.findById(comentarioCurtida.getIdComentario());
        if (!postagemComentarioExistente.isPresent()) {
            throw new ModelException(PostagemComentarioErrors.NOT_FOUND);
        }
        ComentarioCurtidaEntity comentarioCurtidaEntity;
        Optional<ComentarioCurtidaEntity> comentarioCurtidaExistente = comentarioCurtidaRepository.findByPostagemComentarioAndCpf(postagemComentarioExistente.get(), comentarioCurtida.getCpf());
        try {
            if (!comentarioCurtidaExistente.isPresent()) {
                comentarioCurtidaEntity = new ComentarioCurtidaEntity();
            } else {
                comentarioCurtidaEntity = comentarioCurtidaExistente.get();
            }
            comentarioCurtidaEntity.setPostagemComentario(postagemComentarioExistente.get());
            comentarioCurtidaEntity.setCpf(comentarioCurtida.getCpf());
            comentarioCurtidaEntity.setCurtida(comentarioCurtida.getCurtida());
            comentarioCurtidaEntity = comentarioCurtidaRepository.save(comentarioCurtidaEntity);
            return comentarioCurtidaEntity.getCurtida();
        } catch (Exception e) {
            return null;
        }
    }

    public String excluiComentario(Long id) {
        Optional<PostagemComentarioEntity> postagemComentarioExistente = postagemComentarioRepository.findById(id);
        if (!postagemComentarioExistente.isPresent()) {
            throw new ModelException(PostagemComentarioErrors.NOT_FOUND);
        }
        try {
            postagemComentarioRepository.deleteById(id);
        } catch (Exception e) {
            return "Erro ao excluir o comentário. %s\n" + e;
        }
        return "Comentário excluído com sucesso";
    }

}
