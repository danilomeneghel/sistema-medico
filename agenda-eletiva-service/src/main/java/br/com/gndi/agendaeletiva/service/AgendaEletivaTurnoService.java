package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaTurnoEntity;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.model.AgendaEletivaTurno;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaTurnoErrors;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaTurnoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaTurnoService {

    @Autowired
    private AgendaEletivaTurnoRepository agendaEletivaTurnoRepository;

    private ModelMapper mapper = new ModelMapper();

    public List<AgendaEletivaTurno> listaTurno() {
        List<AgendaEletivaTurnoEntity> listaAgendaEletivaTurnoEntity = agendaEletivaTurnoRepository.findAll();
        List<AgendaEletivaTurno> listaAgendaEletivaTurnos = listaAgendaEletivaTurnoEntity.stream().map(entity -> mapper.map(entity, AgendaEletivaTurno.class)).collect(Collectors.toList());
        return listaAgendaEletivaTurnos;
    }

    public Optional<AgendaEletivaTurnoEntity> turnoExistente(Integer codTurno) {
        Optional<AgendaEletivaTurnoEntity> turnoExistente = agendaEletivaTurnoRepository.findByCodTurno(codTurno);
        if (!turnoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaTurnoErrors.NOT_FOUND);
        }
        return turnoExistente;
    }

}
