package br.com.gndi.agendaeletiva.client.email.controller;

import br.com.gndi.agendaeletiva.client.email.feign.EmailClient;
import br.com.gndi.agendaeletiva.client.email.model.Email;
import br.com.gndi.agendaeletiva.client.email.model.errors.EmailErrors;
import br.com.gndi.agendaeletiva.client.email.util.BearerTokenUtil;
import br.com.gndi.agendaeletiva.exception.ModelException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("envia-email")
public class EmailController {

    @Autowired
    EmailClient emailClient;

    @PostMapping
    public Email enviaEmail(@RequestBody @Valid Email email){
        try {
            return emailClient.enviaEmail(email, BearerTokenUtil.getBearerTokenHeader());
        } catch (Exception e) {
            throw new ModelException(EmailErrors.EMAIL_SEND_ERROR);
        }
    }

}
