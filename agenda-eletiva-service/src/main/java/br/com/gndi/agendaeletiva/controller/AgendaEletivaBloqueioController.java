package br.com.gndi.agendaeletiva.controller;

import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueio;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaBloqueioFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.service.AgendaEletivaBloqueioService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-eletiva-bloqueio")
@Api(value = "Agenda Eletiva Bloqueio", tags = "Bloqueio das Agendas")
public class AgendaEletivaBloqueioController {

    @Autowired
    private AgendaEletivaBloqueioService agendaEletivaBloqueioService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaEletivaBloqueio> localizaAgendaEletivaBloqueio(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaBloqueioService.localizaAgendaEletivaBloqueio(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaEletivaBloqueio> pesquisaAgendaEletivaBloqueio(@Valid @ModelAttribute
                                                                                   AgendaEletivaBloqueioFilter agendaEletivaBloqueioFilter) {
        return agendaEletivaBloqueioService.pesquisaAgendaEletivaBloqueio(agendaEletivaBloqueioFilter);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiAgendaEletivaBloqueio(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaBloqueioService.excluiAgendaEletivaBloqueio(id), HttpStatus.OK);
    }

}
