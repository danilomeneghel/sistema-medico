package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioDataEntity;
import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioEntity;
import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioTipoEntity;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.mapper.PaginationMapper;
import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueio;
import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioData;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaBloqueioErrors;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaBloqueioFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaBloqueioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaBloqueioService {

    @Autowired
    private AgendaEletivaBloqueioRepository agendaEletivaBloqueioRepository;

    @Autowired
    private AgendaEletivaSolicitacaoService agendaEletivaSolicitacaoService;

    @Autowired
    private AgendaEletivaBloqueioTipoService agendaEletivaBloqueioTipoService;

    @Autowired
    private AgendaEletivaStatusService agendaEletivaStatusService;

    @Autowired
    private AgendaEletivaTurnoService agendaEletivaTurnoService;

    @Autowired
    private AgendaEletivaBloqueioDataService agendaEletivaBloqueioDataService;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private PaginationMapper paginationMapper;

    public AgendaEletivaBloqueio localizaAgendaEletivaBloqueio(Long id) {
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(id);
        if (!agendaEletivaBloqueioExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioErrors.NOT_FOUND);
        }
        return mapper.map(agendaEletivaBloqueioExistente.get(), AgendaEletivaBloqueio.class);
    }

    public PaginationResponse<AgendaEletivaBloqueio> pesquisaAgendaEletivaBloqueio(AgendaEletivaBloqueioFilter agendaEletivaBloqueioFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaEletivaBloqueioFilter);
        Page<AgendaEletivaBloqueioEntity> pageResult = agendaEletivaBloqueioRepository.findAll(AgendaEletivaBloqueioRepository.filtraBloqueio(agendaEletivaBloqueioFilter), pageable);
        List<AgendaEletivaBloqueio> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaBloqueio.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaEletivaBloqueio criaAgendaEletivaBloqueio(AgendaEletivaBloqueio agendaEletivaBloqueio) {
        AgendaEletivaBloqueioEntity agendaEletivaBloqueioEntity = mapper.map(agendaEletivaBloqueio, AgendaEletivaBloqueioEntity.class);
        // Verifica se o id do tipo de bloqueio existe
        Optional<AgendaEletivaBloqueioTipoEntity> agendaEletivaBloqueioTipoExistente = agendaEletivaBloqueioTipoService.bloqueioTipoExistente(agendaEletivaBloqueio.getAgendaEletivaBloqueioTipo().getId());
        agendaEletivaBloqueioEntity.setAgendaEletivaBloqueioTipo(agendaEletivaBloqueioTipoExistente.get());
        // Verifica se o tipo do bloqueio é 'Outro' para adicionar o motivo
        if (agendaEletivaBloqueioTipoExistente.get().getCodBloqueioTipo() == 6 && !agendaEletivaBloqueio.getBloqueioTipoMotivo().isEmpty()) {
            agendaEletivaBloqueioEntity.setBloqueioTipoMotivo(agendaEletivaBloqueio.getBloqueioTipoMotivo());
        } else {
            agendaEletivaBloqueioEntity.setBloqueioTipoMotivo(null);
        }
        agendaEletivaBloqueioEntity = agendaEletivaBloqueioRepository.save(agendaEletivaBloqueioEntity);
        // Verifica se existe data para bloqueio
        if (agendaEletivaBloqueio.getDatas() == null || agendaEletivaBloqueio.getDatas().isEmpty()) {
            throw new ModelException(AgendaEletivaBloqueioErrors.DATE_NOT_SELECTED);
        } else {
            List<AgendaEletivaBloqueioData> listaAgendaEletivaBloqueioData = agendaEletivaBloqueioDataService.inseriDatas(agendaEletivaBloqueioEntity.getId(), agendaEletivaBloqueio.getDatas());
            List<AgendaEletivaBloqueioDataEntity> listaAgendaEletivaBloqueioDataEntity = new ArrayList<>();
            for (AgendaEletivaBloqueioData agendaEletivaBloqueioData : listaAgendaEletivaBloqueioData) {
                Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(agendaEletivaBloqueioData.getIdAgendaEletivaBloqueio());
                AgendaEletivaBloqueioDataEntity agendaEletivaBloqueioDataEntity = mapper.map(agendaEletivaBloqueioData, AgendaEletivaBloqueioDataEntity.class);
                agendaEletivaBloqueioDataEntity.setAgendaEletivaBloqueio(agendaEletivaBloqueioExistente.get());
                agendaEletivaBloqueioDataEntity.setCodAgenda(agendaEletivaBloqueioData.getCodAgenda());
                agendaEletivaBloqueioDataEntity.setDataBloqueio(agendaEletivaBloqueioData.getDataBloqueio());
                listaAgendaEletivaBloqueioDataEntity.add(agendaEletivaBloqueioDataEntity);
            }
            agendaEletivaBloqueioEntity.setDatas(listaAgendaEletivaBloqueioDataEntity);
        }
        return mapper.map(agendaEletivaBloqueioEntity, AgendaEletivaBloqueio.class);
    }

    public AgendaEletivaBloqueio alteraAgendaEletivaBloqueio(Long id, AgendaEletivaBloqueio agendaEletivaBloqueio) {
        // Verifica se o id da agenda eletiva bloqueio existe
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(id);
        if (!agendaEletivaBloqueioExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioErrors.NOT_FOUND);
        }
        // Localiza o bloqueio para realizar a alteração
        AgendaEletivaBloqueioEntity agendaEletivaBloqueioEntity = agendaEletivaBloqueioExistente.get();
        // Verifica se o id do tipo de bloqueio existe
        Optional<AgendaEletivaBloqueioTipoEntity> agendaEletivaBloqueioTipoExistente = agendaEletivaBloqueioTipoService.bloqueioTipoExistente(agendaEletivaBloqueio.getAgendaEletivaBloqueioTipo().getId());
        agendaEletivaBloqueioEntity.setAgendaEletivaBloqueioTipo(agendaEletivaBloqueioTipoExistente.get());
        // Verifica se o tipo do bloqueio é 'Outro' para adicionar o motivo
        if (agendaEletivaBloqueioTipoExistente.get().getCodBloqueioTipo() == 6 && !agendaEletivaBloqueio.getBloqueioTipoMotivo().isEmpty()) {
            agendaEletivaBloqueioEntity.setBloqueioTipoMotivo(agendaEletivaBloqueio.getBloqueioTipoMotivo());
        } else {
            agendaEletivaBloqueioEntity.setBloqueioTipoMotivo(null);
        }
        agendaEletivaBloqueioEntity.setAgendaExtra(agendaEletivaBloqueio.getAgendaExtra());
        agendaEletivaBloqueioEntity.setDataAgendaExtra(agendaEletivaBloqueio.getDataAgendaExtra());
        agendaEletivaBloqueioEntity.setHoraInicioAgendaExtra(agendaEletivaBloqueio.getHoraInicioAgendaExtra());
        agendaEletivaBloqueioEntity.setHoraFimAgendaExtra(agendaEletivaBloqueio.getHoraFimAgendaExtra());
        agendaEletivaBloqueioEntity.setQtdeConsulta(agendaEletivaBloqueio.getQtdeConsulta());
        // Verifica se existe data para bloqueio
        if (agendaEletivaBloqueio.getDatas() == null || agendaEletivaBloqueio.getDatas().isEmpty()) {
            throw new ModelException(AgendaEletivaBloqueioErrors.DATE_NOT_SELECTED);
        } else {
            // Exclui todas as datas antigas para salvar as novas
            agendaEletivaBloqueioDataService.excluiDatas(id);
            List<AgendaEletivaBloqueioData> listaAgendaEletivaBloqueioData = agendaEletivaBloqueioDataService.inseriDatas(agendaEletivaBloqueioEntity.getId(), agendaEletivaBloqueio.getDatas());
            List<AgendaEletivaBloqueioDataEntity> listaAgendaEletivaBloqueioDataEntity = new ArrayList<>();
            for (AgendaEletivaBloqueioData agendaEletivaBloqueioData : listaAgendaEletivaBloqueioData) {
                AgendaEletivaBloqueioDataEntity agendaEletivaBloqueioDataEntity = mapper.map(agendaEletivaBloqueioData, AgendaEletivaBloqueioDataEntity.class);
                agendaEletivaBloqueioDataEntity.setAgendaEletivaBloqueio(agendaEletivaBloqueioExistente.get());
                agendaEletivaBloqueioDataEntity.setCodAgenda(agendaEletivaBloqueioData.getCodAgenda());
                agendaEletivaBloqueioDataEntity.setDataBloqueio(agendaEletivaBloqueioData.getDataBloqueio());
                listaAgendaEletivaBloqueioDataEntity.add(agendaEletivaBloqueioDataEntity);
            }
            agendaEletivaBloqueioEntity.setDatas(listaAgendaEletivaBloqueioDataEntity);
        }
        agendaEletivaBloqueioEntity = agendaEletivaBloqueioRepository.save(agendaEletivaBloqueioEntity);
        return mapper.map(agendaEletivaBloqueioEntity, AgendaEletivaBloqueio.class);
    }

    public String excluiAgendaEletivaBloqueio(Long id) {
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(id);
        if (!agendaEletivaBloqueioExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioErrors.NOT_FOUND);
        }
        try {
            agendaEletivaBloqueioRepository.deleteById(id);
        } catch (Exception e) {
            return "Erro ao excluir o bloqueio. %s\n" + e;
        }
        return "Bloqueio excluído com sucesso";
    }

}
