package br.com.gndi.agendaeletiva.controller;

import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioTipo;
import br.com.gndi.agendaeletiva.model.AgendaEletivaStatus;
import br.com.gndi.agendaeletiva.model.AgendaEletivaTurno;
import br.com.gndi.agendaeletiva.service.AgendaEletivaBloqueioTipoService;
import br.com.gndi.agendaeletiva.service.AgendaEletivaStatusService;
import br.com.gndi.agendaeletiva.service.AgendaEletivaTurnoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/agenda-eletiva")
@Api(value = "Agenda Eletiva", tags = "Lista Tipos de Bloqueios, Status e Turnos")
public class AgendaEletivaController {

    @Autowired
    private AgendaEletivaBloqueioTipoService bloqueioTipoService;

    @Autowired
    private AgendaEletivaStatusService statusService;

    @Autowired
    private AgendaEletivaTurnoService turnoService;

    @GetMapping("/bloqueio-tipo")
    public ResponseEntity<List<AgendaEletivaBloqueioTipo>> listaBloqueioTipo() {
        return ResponseEntity.ok(bloqueioTipoService.listaBloqueioTipo());
    }

    @GetMapping("/status")
    public ResponseEntity<List<AgendaEletivaStatus>> listaStatus() {
        return ResponseEntity.ok(statusService.listaStatus());
    }

    @GetMapping("/turno")
    public ResponseEntity<List<AgendaEletivaTurno>> listaTurno() {
        return ResponseEntity.ok(turnoService.listaTurno());
    }

}
