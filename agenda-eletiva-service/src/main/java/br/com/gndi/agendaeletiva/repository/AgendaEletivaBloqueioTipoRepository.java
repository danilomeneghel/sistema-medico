package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioTipoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgendaEletivaBloqueioTipoRepository extends JpaRepository<AgendaEletivaBloqueioTipoEntity, Long> {

    Optional<AgendaEletivaBloqueioTipoEntity> findById(Long idAgendaEletivaBloqueioTipo);

}
