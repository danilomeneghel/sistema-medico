package br.com.gndi.agendaeletiva.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaEletivaCancelamentoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Cancelamento não encontrado");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Cancelamento já cadastrado");

}
