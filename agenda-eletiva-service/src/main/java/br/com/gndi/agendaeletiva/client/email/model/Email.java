package br.com.gndi.agendaeletiva.client.email.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    @NotBlank
    @javax.validation.constraints.Email
    private String emailDe;

    @NotBlank
    @javax.validation.constraints.Email
    private String emailPara;

    @NotBlank
    private String assunto;

    @NotBlank
    private String texto;

    private String tipo = "";

}
