package br.com.gndi.agendaeletiva.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "agenda_eletiva_bloqueio")
@Data
public class AgendaEletivaBloqueioEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_agenda_eletiva_bloqueio_tipo", nullable = false)
    private AgendaEletivaBloqueioTipoEntity agendaEletivaBloqueioTipo;

    private String bloqueioTipoMotivo;

    private Boolean agendaExtra;

    private LocalDate dataAgendaExtra;

    private String horaInicioAgendaExtra;

    private String horaFimAgendaExtra;

    private Integer qtdeConsulta;

    @OneToMany(mappedBy = "agendaEletivaBloqueio", cascade = CascadeType.REMOVE)
    private List<AgendaEletivaBloqueioDataEntity> datas;

    public AgendaEletivaBloqueioEntity() {
    }

}
