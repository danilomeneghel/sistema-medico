package br.com.gndi.agendaeletiva.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Date;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaSolicitacao {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    private AgendaEletivaBloqueio agendaEletivaBloqueio;

    private AgendaEletivaCancelamento agendaEletivaCancelamento;

    private AgendaEletivaTurno turno;

    private AgendaEletivaStatus status;

    private String codSolicitacao;

    @ApiModelProperty(value = "Cod Estabelecimento", example = "x999")
    private String codEstabelecimento;

    @ApiModelProperty(value = "Nome Estabelecimento", example = "xxxxxxxxxxxx")
    private String nomeEstabelecimento;

    @ApiModelProperty(value = "Cod Especialidade", example = "x999")
    @NotBlank(message = "Campo Obrigatório")
    private String codEspecialidade;

    @ApiModelProperty(value = "Nome Especialidade", example = "xxxxxxxxxxxx")
    private String nomeEspecialidade;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipoSolicitacao;

    @ApiModelProperty(value = "CPF do Médico", example = "999.999.999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CPF
    private String cpfMedico;

    @ApiModelProperty(value = "Nome do Médico", example = "xxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nomeMedico;

    @ApiModelProperty(value = "Nome do Profissional Cobertura", example = "xxxxxxxxxxxx")
    private String nomeProCobertura;

    @ApiModelProperty(value = "Observação", example = "xxxxxxxxxxxxxxxxxxx")
    private String observacao;

    @ApiModelProperty(value = "Data de Execução do Gestor", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataExecucaoGestor;

    @ApiModelProperty(value = "Data de Execução do Administrador", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataExecucaoAdministrador;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

    private String prazo;

}
