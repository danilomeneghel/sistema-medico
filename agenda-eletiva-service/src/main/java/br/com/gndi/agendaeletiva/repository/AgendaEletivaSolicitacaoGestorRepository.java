package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaSolicitacaoEntity;
import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacaoGestor;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoGestorFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaEletivaSolicitacaoGestorRepository extends JpaRepository<AgendaEletivaSolicitacaoEntity, Long> {

    Optional<AgendaEletivaSolicitacaoEntity> findById(Long id);

    Page<AgendaEletivaSolicitacaoEntity> findAll(Specification<AgendaEletivaSolicitacaoGestor> specification, Pageable pageable);

    static Specification<AgendaEletivaSolicitacaoGestor> filtraSolicitacaoGestor(AgendaEletivaSolicitacaoGestorFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdAgendaEletivaBloqueio() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaEletivaBloqueio"), filter.getIdAgendaEletivaBloqueio()));
            }

            if (filter.getIdAgendaEletivaCancelamento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaEletivaCancelamento"), filter.getIdAgendaEletivaCancelamento()));
            }

            if (filter.getCodStatus() != null) {
                predicates.add(criteriaBuilder.equal(root.get("status").get("codStatus"), filter.getCodStatus()));
            }

            if (filter.getCpfMedico() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpfMedico"), filter.getCpfMedico()));
            }

            if (filter.getNomeMedico() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeMedico"), "%" + filter.getNomeMedico() + "%"));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
