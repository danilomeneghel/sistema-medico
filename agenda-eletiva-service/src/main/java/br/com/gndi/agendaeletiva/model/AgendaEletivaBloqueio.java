package br.com.gndi.agendaeletiva.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaBloqueio {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    private AgendaEletivaBloqueioTipo agendaEletivaBloqueioTipo;

    @ApiModelProperty(value = "Motivo do Tipo de Bloqueio", example = "xxxxxxxxxx")
    private String bloqueioTipoMotivo;

    @ApiModelProperty(value = "Agenda Extra", example = "true")
    @NotNull(message = "Campo Obrigatório")
    private Boolean agendaExtra;

    @ApiModelProperty(value = "Data Agenda Extra", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataAgendaExtra;

    @ApiModelProperty(value = "Hora Início Agenda Extra", example = "13:00")
    @JsonFormat(pattern="HH:mm")
    private String horaInicioAgendaExtra;

    @ApiModelProperty(value = "Hora Fim Agenda Extra", example = "14:00")
    @JsonFormat(pattern="HH:mm")
    private String horaFimAgendaExtra;

    @ApiModelProperty(value = "Quantidade de Consulta", example = "9")
    private Integer qtdeConsulta;

    @ApiModelProperty(value = "Datas")
    private List<AgendaEletivaBloqueioData> datas;

}
