package br.com.gndi.agendaeletiva.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Date;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaSolicitacaoAdministrador {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    private AgendaEletivaStatus status;

    @ApiModelProperty(value = "Nome Estabelecimento", example = "xxxxxxxxxxxx")
    private String nomeEstabelecimento;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipoSolicitacao;

    @ApiModelProperty(value = "Nome do Médico", example = "xxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nomeMedico;

    @ApiModelProperty(value = "Data de Execução do Gestor", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataExecucaoGestor;

    @ApiModelProperty(value = "Data de Execução do Administrador", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataExecucaoAdministrador;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

}
