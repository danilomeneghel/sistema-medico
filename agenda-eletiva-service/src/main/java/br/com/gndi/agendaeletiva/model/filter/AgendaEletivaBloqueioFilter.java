package br.com.gndi.agendaeletiva.model.filter;

import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueio;
import br.com.gndi.agendaeletiva.model.pagination.PaginationRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEletivaBloqueioFilter extends PaginationRequest<AgendaEletivaBloqueio> {

    @ApiModelProperty(value = "Id Bloqueio Tipo", example = "9")
    private Long idAgendaEletivaBloqueioTipo;

    @ApiModelProperty(value = "Motivo do Tipo de Bloqueio", example = "xxxxxxxxxx")
    private String bloqueioTipoMotivo;

    @ApiModelProperty(value = "Agenda Extra", example = "true")
    private Boolean agendaExtra;

    @ApiModelProperty(value = "Data Agenda Extra", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataAgendaExtra;

    @ApiModelProperty(value = "Hora Início Agenda Extra", example = "13:00")
    @JsonFormat(pattern="HH:mm")
    private String horaInicioAgendaExtra;

    @ApiModelProperty(value = "Hora Fim Agenda Extra", example = "14:00")
    @JsonFormat(pattern="HH:mm")
    private String horaFimAgendaExtra;

    @ApiModelProperty(value = "Quantidade de Consulta", example = "9")
    private Integer qtdeConsulta;

}
