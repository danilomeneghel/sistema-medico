package br.com.gndi.agendaeletiva.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaEletivaBloqueioDataErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Data de bloqueio não encontrada");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Data de bloqueio já cadastrada");

}
