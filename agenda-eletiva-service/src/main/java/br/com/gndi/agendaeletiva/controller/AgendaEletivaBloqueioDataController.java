package br.com.gndi.agendaeletiva.controller;

import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioData;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaBloqueioDataFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.service.AgendaEletivaBloqueioDataService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/agenda-eletiva-bloqueio-data")
@Api(value = "Agenda Eletiva Bloqueio Data", tags = "Datas de Bloqueio da Agenda Eletiva")
public class AgendaEletivaBloqueioDataController {

    @Autowired
    private AgendaEletivaBloqueioDataService agendaEletivaBloqueioDataService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaEletivaBloqueioData> localizaData(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaBloqueioDataService.localizaData(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaEletivaBloqueioData> pesquisaData(@Valid @ModelAttribute
                                                                      AgendaEletivaBloqueioDataFilter agendaEletivaBloqueioDataFilter) {
        return agendaEletivaBloqueioDataService.pesquisaData(agendaEletivaBloqueioDataFilter);
    }

    @PostMapping
    public ResponseEntity<AgendaEletivaBloqueioData> inseriData(@RequestParam Long idAgendaEletivaBloqueio,
                                                                @Valid @RequestBody AgendaEletivaBloqueioData agendaEletivaBloqueioData) {
        return new ResponseEntity<>(agendaEletivaBloqueioDataService.inseriData(idAgendaEletivaBloqueio, agendaEletivaBloqueioData), HttpStatus.CREATED);
    }

    @PostMapping("/inseri-datas")
    public ResponseEntity<List<AgendaEletivaBloqueioData>> inseriDatas(@RequestParam Long idAgendaEletivaBloqueio,
                                                                       @Valid @RequestBody List<AgendaEletivaBloqueioData> listaAgendaEletivaBloqueioData) {
        return new ResponseEntity<>(agendaEletivaBloqueioDataService.inseriDatas(idAgendaEletivaBloqueio, listaAgendaEletivaBloqueioData), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<AgendaEletivaBloqueioData> alteraData(@PathVariable Long id,
                                                                @Valid @RequestBody AgendaEletivaBloqueioData agendaEletivaBloqueioData) {
        return new ResponseEntity<>(agendaEletivaBloqueioDataService.alteraData(id, agendaEletivaBloqueioData), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiData(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaBloqueioDataService.excluiData(id), HttpStatus.OK);
    }

}
