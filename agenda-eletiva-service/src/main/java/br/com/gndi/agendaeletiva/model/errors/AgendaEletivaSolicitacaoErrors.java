package br.com.gndi.agendaeletiva.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaEletivaSolicitacaoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Solicitação não encontrada");

    public static final ErrorModel NON_EXISTENT_TYPE = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Tipo de solicitação não existente");

    public static final ErrorModel EMAIL_BODY_ERROR = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Erro ao montar o e-mail para envio");

}
