package br.com.gndi.agendaeletiva.controller;

import br.com.gndi.agendaeletiva.model.AgendaEletivaCancelamento;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaCancelamentoFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.service.AgendaEletivaCancelamentoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-eletiva-cancelamento")
@Api(value = "Agenda Eletiva Cancelamento", tags = "Cancelamento das Agendas")
public class AgendaEletivaCancelamentoController {

    @Autowired
    private AgendaEletivaCancelamentoService agendaEletivaCancelamentoService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaEletivaCancelamento> localizaAgendaEletivaCancelamento(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaCancelamentoService.localizaAgendaEletivaCancelamento(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaEletivaCancelamento> pesquisaAgendaEletivaCancelamento(@Valid @ModelAttribute
                                                                                           AgendaEletivaCancelamentoFilter agendaEletivaCancelamentoFilter) {
        return agendaEletivaCancelamentoService.pesquisaAgendaEletivaCancelamento(agendaEletivaCancelamentoFilter);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiAgendaEletivaCancelamento(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaCancelamentoService.excluiAgendaEletivaCancelamento(id), HttpStatus.OK);
    }

}
