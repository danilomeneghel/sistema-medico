package br.com.gndi.agendaeletiva.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaSolicitacaoGestor {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    private AgendaEletivaBloqueio agendaEletivaBloqueio;

    private AgendaEletivaCancelamento agendaEletivaCancelamento;

    private AgendaEletivaStatus status;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipoSolicitacao;

    @ApiModelProperty(value = "Nome do Médico", example = "xxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nomeMedico;

    private String prazo;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

}
