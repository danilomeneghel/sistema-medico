package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaSolicitacaoEntity;
import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacao;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaEletivaSolicitacaoRepository extends JpaRepository<AgendaEletivaSolicitacaoEntity, Long> {

    Optional<AgendaEletivaSolicitacaoEntity> findById(Long id);

    Page<AgendaEletivaSolicitacaoEntity> findAll(Specification<AgendaEletivaSolicitacao> specification, Pageable pageable);

    static Specification<AgendaEletivaSolicitacao> filtraSolicitacao(AgendaEletivaSolicitacaoFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdAgendaEletivaBloqueio() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaEletivaBloqueio"), filter.getIdAgendaEletivaBloqueio()));
            }

            if (filter.getIdAgendaEletivaCancelamento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaEletivaCancelamento"), filter.getIdAgendaEletivaCancelamento()));
            }

            if (filter.getCodTurno() != null) {
                predicates.add(criteriaBuilder.equal(root.get("agendaEletivaTurno").get("codTurno"), filter.getCodTurno()));
            }

            if (filter.getCodStatus() != null) {
                predicates.add(criteriaBuilder.equal(root.get("status").get("codStatus"), filter.getCodStatus()));
            }

            if (filter.getCodSolicitacao() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codSolicitacao"), filter.getCodSolicitacao()));
            }

            if (filter.getCodEstabelecimento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codEstabelecimento"), filter.getCodEstabelecimento()));
            }

            if (filter.getNomeEstabelecimento() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeEstabelecimento"), "%" + filter.getNomeEstabelecimento() + "%"));
            }

            if (filter.getCodEspecialidade() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codEspecialidade"), filter.getCodEspecialidade()));
            }

            if (filter.getNomeEspecialidade() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeEspecialidade"), "%" + filter.getNomeEspecialidade() + "%"));
            }

            if (filter.getCpfMedico() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpfMedico"), filter.getCpfMedico()));
            }

            if (filter.getNomeMedico() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeMedico"), "%" + filter.getNomeMedico() + "%"));
            }

            if (filter.getNomeProCobertura() != null) {
                predicates.add(criteriaBuilder.like(root.get("nomeProCobertura"), "%" + filter.getNomeProCobertura() + "%"));
            }

            if (filter.getDataExecucaoGestor() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataExecucaoGestor"), filter.getDataExecucaoGestor()));
            }

            if (filter.getDataExecucaoAdministrador() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataExecucaoAdministrador"), filter.getDataExecucaoAdministrador()));
            }

            if (filter.getCreatedFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createdAt"), filter.getCreatedFrom()));
            }

            if (filter.getCreatedTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("createdAt"), filter.getCreatedTo()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
