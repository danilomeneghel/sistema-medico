package br.com.gndi.agendaeletiva.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "agenda_eletiva_bloqueio_tipo")
@Data
public class AgendaEletivaBloqueioTipoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codBloqueioTipo;

    private String descricao;
    
}
