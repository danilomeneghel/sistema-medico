package br.com.gndi.agendaeletiva.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaEletivaStatusErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Status não encontrado");
    
}
