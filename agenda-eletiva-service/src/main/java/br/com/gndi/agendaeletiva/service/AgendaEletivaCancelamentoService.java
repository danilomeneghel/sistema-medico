package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaCancelamentoEntity;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.mapper.PaginationMapper;
import br.com.gndi.agendaeletiva.model.AgendaEletivaCancelamento;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaCancelamentoErrors;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaCancelamentoFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaCancelamentoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaCancelamentoService {

    @Autowired
    private AgendaEletivaCancelamentoRepository agendaEletivaCancelamentoRepository;

    @Autowired
    private AgendaEletivaSolicitacaoService agendaEletivaSolicitacaoService;

    @Autowired
    private AgendaEletivaStatusService agendaEletivaStatusService;

    @Autowired
    private AgendaEletivaTurnoService agendaEletivaTurnoService;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private PaginationMapper paginationMapper;

    public AgendaEletivaCancelamento localizaAgendaEletivaCancelamento(Long id) {
        Optional<AgendaEletivaCancelamentoEntity> agendaEletivaCancelamentoExistente = agendaEletivaCancelamentoRepository.findById(id);
        if (!agendaEletivaCancelamentoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaCancelamentoErrors.NOT_FOUND);
        }
        return mapper.map(agendaEletivaCancelamentoExistente.get(), AgendaEletivaCancelamento.class);
    }

    public PaginationResponse<AgendaEletivaCancelamento> pesquisaAgendaEletivaCancelamento(AgendaEletivaCancelamentoFilter agendaEletivaCancelamentoFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaEletivaCancelamentoFilter);
        Page<AgendaEletivaCancelamentoEntity> pageResult = agendaEletivaCancelamentoRepository.findAll(AgendaEletivaCancelamentoRepository.filtraCancelamento(agendaEletivaCancelamentoFilter), pageable);
        List<AgendaEletivaCancelamento> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaCancelamento.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaEletivaCancelamento criaAgendaEletivaCancelamento(AgendaEletivaCancelamento agendaEletivaCancelamento) {
        AgendaEletivaCancelamentoEntity agendaEletivaCancelamentoEntity = mapper.map(agendaEletivaCancelamento, AgendaEletivaCancelamentoEntity.class);
        agendaEletivaCancelamentoEntity = agendaEletivaCancelamentoRepository.save(agendaEletivaCancelamentoEntity);
        return mapper.map(agendaEletivaCancelamentoEntity, AgendaEletivaCancelamento.class);
    }

    public AgendaEletivaCancelamento alteraAgendaEletivaCancelamento(Long id, AgendaEletivaCancelamento agendaEletivaCancelamento) {
        // Verifica se o id da agenda eletiva Cancelamento existe
        Optional<AgendaEletivaCancelamentoEntity> agendaEletivaCancelamentoExistente = agendaEletivaCancelamentoRepository.findById(id);
        if (!agendaEletivaCancelamentoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaCancelamentoErrors.NOT_FOUND);
        }
        // Localiza o cancelamento para realizar a alteração
        AgendaEletivaCancelamentoEntity agendaEletivaCancelamentoEntity = agendaEletivaCancelamentoExistente.get();
        agendaEletivaCancelamentoEntity.setMotivo(agendaEletivaCancelamento.getMotivo());
        agendaEletivaCancelamentoEntity.setCancelarAgenda(agendaEletivaCancelamento.getCancelarAgenda());
        agendaEletivaCancelamentoEntity.setDataInicioCancelamento(agendaEletivaCancelamento.getDataInicioCancelamento());
        agendaEletivaCancelamentoEntity.setDiaCancelamento(agendaEletivaCancelamento.getDiaCancelamento());
        agendaEletivaCancelamentoEntity.setQtdeConsulta(agendaEletivaCancelamento.getQtdeConsulta());
        agendaEletivaCancelamentoEntity = agendaEletivaCancelamentoRepository.save(agendaEletivaCancelamentoEntity);
        return mapper.map(agendaEletivaCancelamentoEntity, AgendaEletivaCancelamento.class);
    }

    public String excluiAgendaEletivaCancelamento(Long id) {
        Optional<AgendaEletivaCancelamentoEntity> agendaEletivaCancelamentoExistente = agendaEletivaCancelamentoRepository.findById(id);
        if (!agendaEletivaCancelamentoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaCancelamentoErrors.NOT_FOUND);
        }
        try {
            agendaEletivaCancelamentoRepository.deleteById(id);
        } catch (Exception e) {
            return "Erro ao excluir o cancelamento. %s\n" + e;
        }
        return "Cancelamento excluído com sucesso";
    }

}
