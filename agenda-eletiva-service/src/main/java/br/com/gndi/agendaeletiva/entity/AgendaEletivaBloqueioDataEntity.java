package br.com.gndi.agendaeletiva.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "agenda_eletiva_bloqueio_data")
@Data
public class AgendaEletivaBloqueioDataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_agenda_eletiva_bloqueio", nullable = false)
    private AgendaEletivaBloqueioEntity agendaEletivaBloqueio;

    private String codAgenda;

    private LocalDate dataBloqueio;

    public AgendaEletivaBloqueioDataEntity() {
    }

}
