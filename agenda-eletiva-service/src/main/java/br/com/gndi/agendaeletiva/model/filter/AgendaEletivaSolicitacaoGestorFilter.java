package br.com.gndi.agendaeletiva.model.filter;

import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacaoGestor;
import br.com.gndi.agendaeletiva.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEletivaSolicitacaoGestorFilter extends PaginationRequest<AgendaEletivaSolicitacaoGestor> {

    @ApiModelProperty(value = "ID Bloqueio", example = "9")
    private Long idAgendaEletivaBloqueio;

    @ApiModelProperty(value = "ID Cancelamento", example = "9")
    private Long idAgendaEletivaCancelamento;

    @ApiModelProperty(value = "Cod Status", example = "x999")
    private Integer codStatus;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    private String tipoSolicitacao;

    @ApiModelProperty(value = "CPF do Médico", example = "999.999.999-99")
    @CPF
    private String cpfMedico;

    @ApiModelProperty(value = "Nome do Médico", example = "xxxxxxxxxxxx")
    private String nomeMedico;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdTo;

}
