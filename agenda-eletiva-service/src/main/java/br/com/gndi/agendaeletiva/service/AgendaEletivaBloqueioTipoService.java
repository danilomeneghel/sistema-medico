package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioTipoEntity;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioTipo;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaBloqueioTipoErrors;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaBloqueioTipoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaBloqueioTipoService {

    @Autowired
    private AgendaEletivaBloqueioTipoRepository agendaEletivaBloqueioTipoRepository;

    private ModelMapper mapper = new ModelMapper();

    public List<AgendaEletivaBloqueioTipo> listaBloqueioTipo() {
        List<AgendaEletivaBloqueioTipoEntity> listaAgendaEletivaBloqueioTipoEntity = agendaEletivaBloqueioTipoRepository.findAll();
        List<AgendaEletivaBloqueioTipo> listaAgendaEletivaBloqueioTipo = listaAgendaEletivaBloqueioTipoEntity.stream().map(entity -> mapper.map(entity, AgendaEletivaBloqueioTipo.class)).collect(Collectors.toList());
        return listaAgendaEletivaBloqueioTipo;
    }

    public Optional<AgendaEletivaBloqueioTipoEntity> bloqueioTipoExistente(Long idAgendaEletivaBloqueioTipo) {
        Optional<AgendaEletivaBloqueioTipoEntity> bloqueioTipoExistente = agendaEletivaBloqueioTipoRepository.findById(idAgendaEletivaBloqueioTipo);
        if (!bloqueioTipoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioTipoErrors.NOT_FOUND);
        }
        return bloqueioTipoExistente;
    }

}
