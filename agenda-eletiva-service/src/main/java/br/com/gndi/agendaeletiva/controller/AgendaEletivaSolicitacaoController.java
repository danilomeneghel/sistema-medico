package br.com.gndi.agendaeletiva.controller;

import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacao;
import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacaoAdministrador;
import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacaoAgendamento;
import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacaoGestor;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoAdministradorFilter;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoAgendamentoFilter;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoFilter;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoGestorFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.service.AgendaEletivaSolicitacaoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/agenda-eletiva-solicitacao")
@Api(value = "Agenda Eletiva Solicitação", tags = "Solicitações das Agendas")
public class AgendaEletivaSolicitacaoController {

    @Autowired
    private AgendaEletivaSolicitacaoService agendaEletivaSolicitacaoService;

    @GetMapping("{id}")
    public ResponseEntity<AgendaEletivaSolicitacao> localizaAgendaEletivaSolicitacao(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaSolicitacaoService.localizaAgendaEletivaSolicitacao(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<AgendaEletivaSolicitacao> pesquisaAgendaEletivaSolicitacao(@Valid @ModelAttribute
                                                                                         AgendaEletivaSolicitacaoFilter agendaEletivaSolicitacaoFilter) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        return agendaEletivaSolicitacaoService.pesquisaAgendaEletivaSolicitacao(agendaEletivaSolicitacaoFilter);
    }

    @GetMapping("/pesquisa-solicitacao-agendamento")
    public PaginationResponse<AgendaEletivaSolicitacaoAgendamento> pesquisaAgendaEletivaSolicitacaoAgendamento(@Valid @ModelAttribute
                                                                                                               AgendaEletivaSolicitacaoAgendamentoFilter pesquisaAgendaEletivaAgendamento) {
        return agendaEletivaSolicitacaoService.pesquisaAgendaEletivaSolicitacaoAgendamento(pesquisaAgendaEletivaAgendamento);
    }

    @GetMapping("/pesquisa-solicitacao-gestor")
    public PaginationResponse<AgendaEletivaSolicitacaoGestor> pesquisaAgendaEletivaSolicitacaoGestor(@Valid @ModelAttribute
                                                                                                     AgendaEletivaSolicitacaoGestorFilter pesquisaAgendaEletivaGestor) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        return agendaEletivaSolicitacaoService.pesquisaAgendaEletivaSolicitacaoGestor(pesquisaAgendaEletivaGestor);
    }

    @GetMapping("/pesquisa-solicitacao-administrador")
    public PaginationResponse<AgendaEletivaSolicitacaoAdministrador> pesquisaAgendaEletivaSolicitacaoAdministrador(@Valid @ModelAttribute
                                                                                                                   AgendaEletivaSolicitacaoAdministradorFilter agendaEletivaSolicitacaoAdministradorFilter) {
        return agendaEletivaSolicitacaoService.pesquisaAgendaEletivaSolicitacaoAdministrador(agendaEletivaSolicitacaoAdministradorFilter);
    }

    @PostMapping
    public ResponseEntity<AgendaEletivaSolicitacao> criaAgendaEletivaSolicitacao(@Valid @RequestBody AgendaEletivaSolicitacao agendaEletivaSolicitacao) {
        return new ResponseEntity<>(agendaEletivaSolicitacaoService.criaAgendaEletivaSolicitacao(agendaEletivaSolicitacao), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<AgendaEletivaSolicitacao> alteraAgendaEletivaSolicitacao(@PathVariable Long id, @Valid @RequestBody AgendaEletivaSolicitacao agendaEletivaSolicitacao) {
        return new ResponseEntity<>(agendaEletivaSolicitacaoService.alteraAgendaEletivaSolicitacao(id, agendaEletivaSolicitacao), HttpStatus.OK);
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<AgendaEletivaSolicitacao> alteraStatusAgendaEletivaSolicitacao(@PathVariable Long id, @RequestParam Integer codStatus, @RequestParam String observacao) {
        return new ResponseEntity<>(agendaEletivaSolicitacaoService.alteraStatusAgendaEletivaSolicitacao(id, codStatus, observacao), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> excluiAgendaEletivaSolicitacao(@PathVariable Long id) {
        return new ResponseEntity<>(agendaEletivaSolicitacaoService.excluiAgendaEletivaSolicitacao(id), HttpStatus.OK);
    }

}
