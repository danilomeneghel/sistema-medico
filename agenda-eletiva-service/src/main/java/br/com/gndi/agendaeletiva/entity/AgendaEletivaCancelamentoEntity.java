package br.com.gndi.agendaeletiva.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "agenda_eletiva_cancelamento")
@Data
public class AgendaEletivaCancelamentoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 1, max = 500)
    private String motivo;

    private LocalDate dataInicioCancelamento;

    private Boolean cancelarAgenda;

    private String diaCancelamento;

    private Integer qtdeConsulta;

    public AgendaEletivaCancelamentoEntity() {
    }

}
