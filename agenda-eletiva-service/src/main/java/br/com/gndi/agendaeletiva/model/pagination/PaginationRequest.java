package br.com.gndi.agendaeletiva.model.pagination;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@ApiModel
@Data
public class PaginationRequest<T> {

    @ApiModelProperty(value = "Page number, starting at 0", example = "0")
    @NotNull
    private Integer pageNumber = 0;

    @ApiModelProperty(value = "Page size", example = "20")
    @NotNull
    private Integer pageSize = 20;

    @ApiModelProperty(value = "Sort by field", example = "id")
    private Optional<@NotBlank String> sortBy = Optional.of("id");

    @ApiModelProperty(value = "Sort direction, one of: asc, desc", example = "asc")
    private Optional<@NotBlank String> sortDirection;

}
