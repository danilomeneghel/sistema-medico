package br.com.gndi.agendaeletiva.model.errors;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@ApiModel
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class ErrorModel {

    @NonNull
    @ApiModelProperty(value = "Http status code", example = "400", required = true)
    private Integer status;

    @NonNull
    @ApiModelProperty(value = "Error code", example = "400001", required = true)
    private String code;

    @NonNull
    @ApiModelProperty(value = "Error message", example = "Error message", required = true)
    private String message;

    @ApiModelProperty(value = "Error details", example = "Error details")
    private String details;

}
