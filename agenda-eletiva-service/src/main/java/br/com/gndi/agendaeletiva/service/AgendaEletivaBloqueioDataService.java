package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioDataEntity;
import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioEntity;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.mapper.PaginationMapper;
import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioData;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaBloqueioDataErrors;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaBloqueioErrors;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaBloqueioDataFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaBloqueioDataRepository;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaBloqueioRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaBloqueioDataService {

    @Autowired
    private AgendaEletivaBloqueioDataRepository agendaEletivaBloqueioDataRepository;

    @Autowired
    private AgendaEletivaBloqueioRepository agendaEletivaBloqueioRepository;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private PaginationMapper paginationMapper;

    public AgendaEletivaBloqueioData localizaData(Long id) {
        Optional<AgendaEletivaBloqueioDataEntity> agendaEletivaBloqueioDataExistente = agendaEletivaBloqueioDataRepository.findById(id);
        if (!agendaEletivaBloqueioDataExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioDataErrors.NOT_FOUND);
        }
        return mapper.map(agendaEletivaBloqueioDataExistente.get(), AgendaEletivaBloqueioData.class);
    }

    public PaginationResponse<AgendaEletivaBloqueioData> pesquisaData(AgendaEletivaBloqueioDataFilter agendaEletivaBloqueioDataFilter) {
        Pageable pageable = paginationMapper.toPageable(agendaEletivaBloqueioDataFilter);
        Page<AgendaEletivaBloqueioDataEntity> pageResult = agendaEletivaBloqueioDataRepository.findAll(AgendaEletivaBloqueioDataRepository.filtraBloqueioData(agendaEletivaBloqueioDataFilter), pageable);
        List<AgendaEletivaBloqueioData> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaBloqueioData.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public AgendaEletivaBloqueioData inseriData(Long idAgendaEletivaBloqueio, AgendaEletivaBloqueioData agendaEletivaBloqueioData) {
        // Verifica se o id da Agenda Eletiva Bloqueio existe
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(idAgendaEletivaBloqueio);
        if (agendaEletivaBloqueioExistente.isPresent()) {
            // Verifica se a CodAgenda, CodEstabelecimento e DataAgenda para a mesma Agenda Eletiva Bloqueio Data já foi cadastrada
            Optional<AgendaEletivaBloqueioDataEntity> agendaEletivaBloqueioDataExistente = agendaEletivaBloqueioDataRepository.findByAgendaEletivaBloqueioAndDataBloqueio(agendaEletivaBloqueioExistente.get(), agendaEletivaBloqueioData.getDataBloqueio());
            if (agendaEletivaBloqueioDataExistente.isPresent()) {
                throw new ModelException(AgendaEletivaBloqueioDataErrors.DUPLICATE_RECORD);
            }
            AgendaEletivaBloqueioDataEntity agendaEletivaBloqueioDataEntity = mapper.map(agendaEletivaBloqueioData, AgendaEletivaBloqueioDataEntity.class);
            agendaEletivaBloqueioDataEntity.setAgendaEletivaBloqueio(agendaEletivaBloqueioExistente.get());
            agendaEletivaBloqueioDataEntity = agendaEletivaBloqueioDataRepository.save(agendaEletivaBloqueioDataEntity);
            return mapper.map(agendaEletivaBloqueioDataEntity, AgendaEletivaBloqueioData.class);
        } else {
            throw new ModelException(AgendaEletivaBloqueioErrors.NOT_FOUND);
        }
    }

    public List<AgendaEletivaBloqueioData> inseriDatas(Long idAgendaEletivaBloqueio, List<AgendaEletivaBloqueioData> listaAgendaEletivaBloqueioData) {
        // Verifica se o id da Agenda Eletiva Bloqueio existe
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(idAgendaEletivaBloqueio);
        if (!agendaEletivaBloqueioExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioErrors.NOT_FOUND);
        }
        List<AgendaEletivaBloqueioData> agendaEletivaBloqueioDatas = new ArrayList<>();
        for(AgendaEletivaBloqueioData agendaEletivaBloqueioData : listaAgendaEletivaBloqueioData) {
            agendaEletivaBloqueioDatas.add(inseriData(idAgendaEletivaBloqueio, agendaEletivaBloqueioData));
        }
        return agendaEletivaBloqueioDatas;
    }

    public AgendaEletivaBloqueioData alteraData(Long id, AgendaEletivaBloqueioData agendaEletivaBloqueioData) {
        // Verifica se o id da Agenda Eletiva Bloqueio existe
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(agendaEletivaBloqueioData.getIdAgendaEletivaBloqueio());
        if (agendaEletivaBloqueioExistente.isPresent()) {
            // Verifica se o id da Agenda Eletiva Bloqueio Data existe
            Optional<AgendaEletivaBloqueioDataEntity> agendaEletivaBloqueioDataExistente = agendaEletivaBloqueioDataRepository.findById(id);
            if (!agendaEletivaBloqueioDataExistente.isPresent()) {
                throw new ModelException(AgendaEletivaBloqueioDataErrors.NOT_FOUND);
            }
            // Localiza a data para realizar a alteração
            AgendaEletivaBloqueioDataEntity agendaEletivaBloqueioDataEntity = agendaEletivaBloqueioDataExistente.get();
            agendaEletivaBloqueioDataEntity.setAgendaEletivaBloqueio(agendaEletivaBloqueioExistente.get());
            agendaEletivaBloqueioDataEntity.setCodAgenda(agendaEletivaBloqueioData.getCodAgenda());
            agendaEletivaBloqueioDataEntity.setDataBloqueio(agendaEletivaBloqueioData.getDataBloqueio());
            agendaEletivaBloqueioDataEntity = agendaEletivaBloqueioDataRepository.save(agendaEletivaBloqueioDataEntity);
            return mapper.map(agendaEletivaBloqueioDataEntity, AgendaEletivaBloqueioData.class);
        } else {
            throw new ModelException(AgendaEletivaBloqueioErrors.NOT_FOUND);
        }
    }

    public String excluiData(Long id) {
        Optional<AgendaEletivaBloqueioDataEntity> agendaEletivaBloqueioDataExistente = agendaEletivaBloqueioDataRepository.findById(id);
        if (!agendaEletivaBloqueioDataExistente.isPresent()) {
            throw new ModelException(AgendaEletivaBloqueioDataErrors.NOT_FOUND);
        }
        try {
            agendaEletivaBloqueioDataRepository.deleteById(id);
        } catch (Exception e) {
            return "Erro ao excluir a data do bloqueio. %s\n" + e;
        }
        return "Data do bloqueio excluída com sucesso";
    }

    public String excluiDatas(Long idAgendaEletivaBloqueio) {
        Optional<AgendaEletivaBloqueioEntity> agendaEletivaBloqueioExistente = agendaEletivaBloqueioRepository.findById(idAgendaEletivaBloqueio);
        List<AgendaEletivaBloqueioDataEntity> agendaEletivaBloqueioDatas = agendaEletivaBloqueioDataRepository.findAllByAgendaEletivaBloqueio(agendaEletivaBloqueioExistente.get());
        if (agendaEletivaBloqueioDatas.isEmpty()) {
            throw new ModelException(AgendaEletivaBloqueioDataErrors.NOT_FOUND);
        }
        try {
            agendaEletivaBloqueioDataRepository.deleteAllByAgendaEletivaBloqueio(agendaEletivaBloqueioExistente.get());
        } catch (Exception e) {
            return "Erro ao excluir a(s) data(s) do bloqueio. %s\n" + e;
        }
        return "Data(s) do bloqueio excluída(s) com sucesso";
    }

}
