package br.com.gndi.agendaeletiva.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaBloqueioData {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "ID Agenda Eletiva Bloqueio", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Long idAgendaEletivaBloqueio;

    @ApiModelProperty(value = "Cod Agenda", example = "x999")
    @NotBlank(message = "Campo Obrigatório")
    private String codAgenda;

    @ApiModelProperty(value = "Data Bloqueio", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    @NotNull(message = "Campo Obrigatório")
    private LocalDate dataBloqueio;

}
