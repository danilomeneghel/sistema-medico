package br.com.gndi.agendaeletiva.model.filter;

import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioData;
import br.com.gndi.agendaeletiva.model.pagination.PaginationRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEletivaBloqueioDataFilter extends PaginationRequest<AgendaEletivaBloqueioData> {

    @ApiModelProperty(value = "ID Bloqueio", example = "9")
    private Long idAgendaEletivaBloqueio;

    @ApiModelProperty(value = "Cod Agenda", example = "x999")
    private String codAgenda;

    @ApiModelProperty(value = "Data Bloqueio", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataBloqueio;

}
