package br.com.gndi.agendaeletiva.model.filter;

import br.com.gndi.agendaeletiva.model.AgendaEletivaCancelamento;
import br.com.gndi.agendaeletiva.model.pagination.PaginationRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEletivaCancelamentoFilter extends PaginationRequest<AgendaEletivaCancelamento> {

    @ApiModelProperty(value = "Motivo", example = "xxxxxxxxxxxxxxx")
    private String motivo;

    @ApiModelProperty(value = "Data Inicio Cancelamento", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataInicioCancelamento;

    @ApiModelProperty(value = "Cancelar Agenda", example = "true")
    private Boolean cancelarAgenda;

    @ApiModelProperty(value = "Dia do Cancelamento (Semanal)", example = "xxxxxxxxxxxx")
    private String diaCancelamento;

    @ApiModelProperty(value = "Quantidade de Consulta", example = "9")
    private Integer qtdeConsulta;

}
