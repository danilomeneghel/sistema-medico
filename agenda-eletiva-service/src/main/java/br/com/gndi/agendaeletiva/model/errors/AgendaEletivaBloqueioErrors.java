package br.com.gndi.agendaeletiva.model.errors;

import org.springframework.http.HttpStatus;

public class AgendaEletivaBloqueioErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Bloqueio não encontrado");

    public static final ErrorModel DATE_NOT_SELECTED = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "404001", "Nenhuma data de bloqueio selecionada");

    public static final ErrorModel DUPLICATE_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Bloqueio já cadastrado");

}
