package br.com.gndi.agendaeletiva.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoSolicitacao {

    BLOQUEIO("Bloqueio"),
    CANCELAMENTO("Cancelamento");

    private String value;

}
