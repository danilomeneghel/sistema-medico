package br.com.gndi.agendaeletiva.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaSolicitacaoAgendamento {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    private AgendaEletivaStatus status;

    @ApiModelProperty(value = "Nome Estabelecimento", example = "xxxxxxxxxxxx")
    private String nomeEstabelecimento;

    @ApiModelProperty(value = "Nome Especialidade", example = "xxxxxxxxxxxx")
    private String nomeEspecialidade;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipoSolicitacao;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

}
