package br.com.gndi.agendaeletiva.model.filter;

import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacaoAgendamento;
import br.com.gndi.agendaeletiva.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEletivaSolicitacaoAgendamentoFilter extends PaginationRequest<AgendaEletivaSolicitacaoAgendamento> {

    @ApiModelProperty(value = "Cod Status", example = "9")
    private Integer codStatus;

    @ApiModelProperty(value = "Nome Estabelecimento", example = "xxxxxxxxxxxx")
    private String nomeEstabelecimento;

    @ApiModelProperty(value = "Nome Especialidade", example = "xxxxxxxxxxxx")
    private String nomeEspecialidade;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    private String tipoSolicitacao;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdTo;

}
