package br.com.gndi.agendaeletiva.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaStatus {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Status", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codStatus;

    @ApiModelProperty(value = "Status", example = "xxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String status;

}
