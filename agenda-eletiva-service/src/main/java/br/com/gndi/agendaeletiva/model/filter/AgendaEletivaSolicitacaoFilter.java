package br.com.gndi.agendaeletiva.model.filter;

import br.com.gndi.agendaeletiva.model.AgendaEletivaSolicitacao;
import br.com.gndi.agendaeletiva.model.pagination.PaginationRequest;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class AgendaEletivaSolicitacaoFilter extends PaginationRequest<AgendaEletivaSolicitacao> {

    @ApiModelProperty(value = "ID Bloqueio", example = "9")
    private Long idAgendaEletivaBloqueio;

    @ApiModelProperty(value = "ID Cancelamento", example = "9")
    private Long idAgendaEletivaCancelamento;

    @ApiModelProperty(value = "Cod Turno", example = "9")
    private Integer codTurno;

    @ApiModelProperty(value = "Cod Status", example = "9")
    private Integer codStatus;

    @ApiModelProperty(value = "Cod Solicitacao", example = "x999")
    private String codSolicitacao;

    @ApiModelProperty(value = "Cod Estabelecimento", example = "x999")
    private String codEstabelecimento;

    @ApiModelProperty(value = "Nome Estabelecimento", example = "xxxxxxxxxxxx")
    private String nomeEstabelecimento;

    @ApiModelProperty(value = "Cod Especialidade", example = "x999")
    private String codEspecialidade;

    @ApiModelProperty(value = "Nome Especialidade", example = "xxxxxxxxxxxx")
    private String nomeEspecialidade;

    @ApiModelProperty(value = "Tipo de Solicitação", example = "xxxxxxxx")
    private String tipoSolicitacao;

    @ApiModelProperty(value = "CPF do Médico", example = "999.999.999-99")
    @CPF
    private String cpfMedico;

    @ApiModelProperty(value = "Nome do Médico", example = "xxxxxxxxxxxx")
    private String nomeMedico;

    @ApiModelProperty(value = "Nome do Profissional Cobertura", example = "xxxxxxxxxxxx")
    private String nomeProCobertura;

    @ApiModelProperty(value = "Data de Execução do Gestor", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataExecucaoGestor;

    @ApiModelProperty(value = "Data de Execução do Administrador", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataExecucaoAdministrador;

    @ApiModelProperty(value = "Data de Criação De", example = "2021-12-01")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdFrom;

    @ApiModelProperty(value = "Data de Criação Para", example = "2021-12-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdTo;

}
