package br.com.gndi.agendaeletiva.client.email.model.errors;

import br.com.gndi.agendaeletiva.model.errors.ErrorModel;
import org.springframework.http.HttpStatus;

public class EmailErrors {

    public static final ErrorModel EMAIL_SEND_ERROR = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Erro ao enviar e-mail de confirmação");

}
