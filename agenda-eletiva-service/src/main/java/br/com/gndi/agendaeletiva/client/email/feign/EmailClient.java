package br.com.gndi.agendaeletiva.client.email.feign;

import br.com.gndi.agendaeletiva.client.email.model.Email;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.validation.Valid;

@FeignClient(name = "envia-email", url = "${feign.client.url.email-service}")
public interface EmailClient {
    @PostMapping("/envia-email")
    Email enviaEmail(@RequestBody @Valid Email email, @RequestHeader("Authorization") String token);
}
