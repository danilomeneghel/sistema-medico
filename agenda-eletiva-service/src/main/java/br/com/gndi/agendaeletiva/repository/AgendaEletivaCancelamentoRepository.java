package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaCancelamentoEntity;
import br.com.gndi.agendaeletiva.model.AgendaEletivaCancelamento;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaCancelamentoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaEletivaCancelamentoRepository extends JpaRepository<AgendaEletivaCancelamentoEntity, Long> {

    Optional<AgendaEletivaCancelamentoEntity> findById(Long id);

    Page<AgendaEletivaCancelamentoEntity> findAll(Specification<AgendaEletivaCancelamento> specification, Pageable pageable);

    static Specification<AgendaEletivaCancelamento> filtraCancelamento(AgendaEletivaCancelamentoFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getMotivo() != null) {
                predicates.add(criteriaBuilder.like(root.get("motivo"), "%" + filter.getMotivo() + "%"));
            }

            if (filter.getDataInicioCancelamento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataInicioCancelamento"), filter.getDataInicioCancelamento()));
            }

            if (filter.getCancelarAgenda() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cancelarAgenda"), filter.getCancelarAgenda()));
            }

            if (filter.getDiaCancelamento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("diaCancelamento"), filter.getDiaCancelamento()));
            }

            if (filter.getQtdeConsulta() != null) {
                predicates.add(criteriaBuilder.equal(root.get("qtdeConsulta"), filter.getQtdeConsulta()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
