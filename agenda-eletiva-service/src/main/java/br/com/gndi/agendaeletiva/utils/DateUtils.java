package br.com.gndi.agendaeletiva.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {

    public static String DateToString(Date inputDate, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        return simpleDateFormat.format(inputDate);
    }

    public static String LocalDateToString(LocalDate inputDate, String dateFormat) {
        return inputDate.format(DateTimeFormatter.ofPattern(dateFormat));
    }

}
