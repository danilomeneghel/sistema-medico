package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaStatusEntity;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.model.AgendaEletivaStatus;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaStatusErrors;
import br.com.gndi.agendaeletiva.repository.AgendaEletivaStatusRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaStatusService {

    @Autowired
    private AgendaEletivaStatusRepository agendaEletivaStatusRepository;

    private ModelMapper mapper = new ModelMapper();

    public List<AgendaEletivaStatus> listaStatus() {
        List<AgendaEletivaStatusEntity> listaAgendaEletivaStatusEntity = agendaEletivaStatusRepository.findAll();
        List<AgendaEletivaStatus> listaAgendaEletivaStatus = listaAgendaEletivaStatusEntity.stream().map(entity -> mapper.map(entity, AgendaEletivaStatus.class)).collect(Collectors.toList());
        return listaAgendaEletivaStatus;
    }

    public Optional<AgendaEletivaStatusEntity> statusExistente(Integer codStatus) {
        Optional<AgendaEletivaStatusEntity> statusExistente = agendaEletivaStatusRepository.findByCodStatus(codStatus);
        if (!statusExistente.isPresent()) {
            throw new ModelException(AgendaEletivaStatusErrors.NOT_FOUND);
        }
        return statusExistente;
    }

}
