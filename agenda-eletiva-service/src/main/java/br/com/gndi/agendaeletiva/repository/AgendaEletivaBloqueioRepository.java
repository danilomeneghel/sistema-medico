package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioEntity;
import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueio;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaBloqueioFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaEletivaBloqueioRepository extends JpaRepository<AgendaEletivaBloqueioEntity, Long> {

    Optional<AgendaEletivaBloqueioEntity> findById(Long id);

    Page<AgendaEletivaBloqueioEntity> findAll(Specification<AgendaEletivaBloqueio> specification, Pageable pageable);

    static Specification<AgendaEletivaBloqueio> filtraBloqueio(AgendaEletivaBloqueioFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdAgendaEletivaBloqueioTipo() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaEletivaBloqueioTipo"), filter.getIdAgendaEletivaBloqueioTipo()));
            }

            if (filter.getBloqueioTipoMotivo() != null) {
                predicates.add(criteriaBuilder.like(root.get("bloqueioTipoMotivo"), "%" + filter.getBloqueioTipoMotivo() + "%"));
            }

            if (filter.getAgendaExtra() != null) {
                predicates.add(criteriaBuilder.equal(root.get("agendaExtra"), filter.getAgendaExtra()));
            }

            if (filter.getDataAgendaExtra() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataAgenda"), filter.getDataAgendaExtra()));
            }

            if (filter.getHoraInicioAgendaExtra() != null) {
                predicates.add(criteriaBuilder.equal(root.get("horaInicioAgenda"), filter.getHoraInicioAgendaExtra()));
            }

            if (filter.getHoraFimAgendaExtra() != null) {
                predicates.add(criteriaBuilder.equal(root.get("horaFimAgenda"), filter.getHoraFimAgendaExtra()));
            }

            if (filter.getQtdeConsulta() != null) {
                predicates.add(criteriaBuilder.equal(root.get("qtdeConsulta"), filter.getQtdeConsulta()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
