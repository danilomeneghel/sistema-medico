package br.com.gndi.agendaeletiva.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "agenda_eletiva_solicitacao")
@Data
public class AgendaEletivaSolicitacaoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_agenda_eletiva_bloqueio", nullable = true)
    private AgendaEletivaBloqueioEntity agendaEletivaBloqueio;

    @OneToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_agenda_eletiva_cancelamento", nullable = true)
    private AgendaEletivaCancelamentoEntity agendaEletivaCancelamento;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_agenda_eletiva_turno", nullable = true)
    private AgendaEletivaTurnoEntity turno;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_agenda_eletiva_status", nullable = false)
    private AgendaEletivaStatusEntity status;

    private String codSolicitacao;

    private String codEstabelecimento;

    private String nomeEstabelecimento;

    private String codEspecialidade;

    private String nomeEspecialidade;

    private String tipoSolicitacao;

    private String cpfMedico;

    private String nomeMedico;

    private String nomeProCobertura;

    @Size(min = 0, max = 500)
    private String observacao;

    private LocalDate dataExecucaoGestor;

    private LocalDate dataExecucaoAdministrador;

    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedAt;

    public AgendaEletivaSolicitacaoEntity() {
    }

    public AgendaEletivaSolicitacaoEntity(Long id, AgendaEletivaStatusEntity status, String nomeEstabelecimento, String nomeEspecialidade, String tipoSolicitacao, Date createdAt) {
        this.id = id;
        this.status = status;
        this.nomeEstabelecimento = nomeEstabelecimento;
        this.nomeEspecialidade = nomeEspecialidade;
        this.tipoSolicitacao = tipoSolicitacao;
        this.createdAt = createdAt;
    }

    public AgendaEletivaSolicitacaoEntity(Long id, AgendaEletivaBloqueioEntity agendaEletivaBloqueio, AgendaEletivaCancelamentoEntity agendaEletivaCancelamento, AgendaEletivaStatusEntity status, String tipoSolicitacao, String nomeMedico, Date createdAt) {
        this.id = id;
        this.agendaEletivaBloqueio = agendaEletivaBloqueio;
        this.agendaEletivaCancelamento = agendaEletivaCancelamento;
        this.status = status;
        this.tipoSolicitacao = tipoSolicitacao;
        this.nomeMedico = nomeMedico;
        this.createdAt = createdAt;
    }

    public AgendaEletivaSolicitacaoEntity(Long id, AgendaEletivaStatusEntity status, String nomeEstabelecimento, String tipoSolicitacao, String nomeMedico, LocalDate dataExecucaoGestor, LocalDate dataExecucaoAdministrador, Date createdAt) {
        this.id = id;
        this.status = status;
        this.nomeEstabelecimento = nomeEstabelecimento;
        this.tipoSolicitacao = tipoSolicitacao;
        this.nomeMedico = nomeMedico;
        this.dataExecucaoGestor = dataExecucaoGestor;
        this.dataExecucaoAdministrador = dataExecucaoAdministrador;
        this.createdAt = createdAt;
    }

}
