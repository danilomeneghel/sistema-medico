package br.com.gndi.agendaeletiva.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaEletivaCancelamento {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Motivo", example = "xxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String motivo;

    @ApiModelProperty(value = "Data Inicio Cancelamento", example = "01/01/2001")
    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate dataInicioCancelamento;

    @ApiModelProperty(value = "Cancelar Agenda", example = "true")
    @NotNull(message = "Campo Obrigatório")
    private Boolean cancelarAgenda;

    @ApiModelProperty(value = "Dia do Cancelamento (Semanal)", example = "xxxxxxxxxxxx")
    private String diaCancelamento;

    @ApiModelProperty(value = "Quantidade de Consulta", example = "9")
    private Integer qtdeConsulta;

}
