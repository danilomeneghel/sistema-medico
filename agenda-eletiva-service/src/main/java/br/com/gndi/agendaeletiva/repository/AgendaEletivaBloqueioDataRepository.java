package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioDataEntity;
import br.com.gndi.agendaeletiva.entity.AgendaEletivaBloqueioEntity;
import br.com.gndi.agendaeletiva.model.AgendaEletivaBloqueioData;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaBloqueioDataFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgendaEletivaBloqueioDataRepository extends JpaRepository<AgendaEletivaBloqueioDataEntity, Long> {

    Optional<AgendaEletivaBloqueioDataEntity> findById(Long id);

    List<AgendaEletivaBloqueioDataEntity> findAllByAgendaEletivaBloqueio(AgendaEletivaBloqueioEntity agendaEletivaBloqueio);

    Optional<AgendaEletivaBloqueioDataEntity> findByAgendaEletivaBloqueioAndDataBloqueio(AgendaEletivaBloqueioEntity agendaEletivaBloqueio, LocalDate DataBloqueio);

    void deleteAllByAgendaEletivaBloqueio(AgendaEletivaBloqueioEntity agendaEletivaBloqueio);

    Page<AgendaEletivaBloqueioDataEntity> findAll(Specification<AgendaEletivaBloqueioData> specification, Pageable pageable);

    static Specification<AgendaEletivaBloqueioData> filtraBloqueioData(AgendaEletivaBloqueioDataFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getIdAgendaEletivaBloqueio() != null) {
                predicates.add(criteriaBuilder.equal(root.get("idAgendaEletivaBloqueio"), filter.getIdAgendaEletivaBloqueio()));
            }

            if (filter.getCodAgenda() != null) {
                predicates.add(criteriaBuilder.equal(root.get("codAgenda"), filter.getCodAgenda()));
            }

            if (filter.getDataBloqueio() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataBloqueio"), filter.getDataBloqueio()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
