package br.com.gndi.agendaeletiva.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "agenda_eletiva_status")
@Data
public class AgendaEletivaStatusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codStatus;

    private String status;

    public AgendaEletivaStatusEntity() {
    }

}
