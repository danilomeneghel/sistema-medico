package br.com.gndi.agendaeletiva.service;

import br.com.gndi.agendaeletiva.client.email.controller.EmailController;
import br.com.gndi.agendaeletiva.client.email.model.Email;
import br.com.gndi.agendaeletiva.entity.*;
import br.com.gndi.agendaeletiva.enums.TipoSolicitacao;
import br.com.gndi.agendaeletiva.exception.ModelException;
import br.com.gndi.agendaeletiva.mapper.PaginationMapper;
import br.com.gndi.agendaeletiva.model.*;
import br.com.gndi.agendaeletiva.model.errors.AgendaEletivaSolicitacaoErrors;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoAdministradorFilter;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoAgendamentoFilter;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoFilter;
import br.com.gndi.agendaeletiva.model.filter.AgendaEletivaSolicitacaoGestorFilter;
import br.com.gndi.agendaeletiva.model.pagination.PaginationResponse;
import br.com.gndi.agendaeletiva.repository.*;
import br.com.gndi.agendaeletiva.utils.DateUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.transaction.Transactional;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class AgendaEletivaSolicitacaoService {

    private static final String TEMPLATE_EMAIL_BLOQUEIO = "templates/email-agenda-eletiva-bloqueio.html";
    private static final String TEMPLATE_EMAIL_CANCELAMENTO = "templates/email-agenda-eletiva-cancelamento.html";
    private static final String tituloAgendaEletivaBloqueio = "Solicitação de Bloqueio - Agenda Eletiva";
    private static final String tituloAgendaEletivaCancelamento = "Solicitação de Cancelamento - Agenda Eletiva";
    @Value("${mail.from}")
    private String emailDe;

    @Value("${mail.to}")
    private String emailPara;

    @Autowired
    private EmailController emailController;

    @Autowired
    private AgendaEletivaSolicitacaoRepository agendaEletivaSolicitacaoRepository;

    @Autowired
    private AgendaEletivaSolicitacaoAgendamentoRepository agendaEletivaSolicitacaoAgendamentoRepository;

    @Autowired
    private AgendaEletivaSolicitacaoGestorRepository agendaEletivaSolicitacaoGestorRepository;

    @Autowired
    private AgendaEletivaSolicitacaoAdministradorRepository agendaEletivaSolicitacaoAdministradorRepository;

    @Autowired
    private AgendaEletivaBloqueioRepository agendaEletivaBloqueioRepository;

    @Autowired
    private AgendaEletivaCancelamentoRepository agendaEletivaCancelamentoRepository;

    @Autowired
    private AgendaEletivaStatusService agendaEletivaStatusService;

    @Autowired
    private AgendaEletivaTurnoService agendaEletivaTurnoService;

    @Autowired
    private AgendaEletivaBloqueioService agendaEletivaBloqueioService;

    @Autowired
    private AgendaEletivaCancelamentoService agendaEletivaCancelamentoService;

    @Autowired
    private PaginationMapper paginationMapper;

    private ModelMapper mapper = new ModelMapper();

    public AgendaEletivaSolicitacao localizaAgendaEletivaSolicitacao(Long id) {
        Optional<AgendaEletivaSolicitacaoEntity> agendaEletivaSolicitacaoExistente = agendaEletivaSolicitacaoRepository.findById(id);
        if (!agendaEletivaSolicitacaoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaSolicitacaoErrors.NOT_FOUND);
        }
        return mapper.map(agendaEletivaSolicitacaoExistente.get(), AgendaEletivaSolicitacao.class);
    }

    public PaginationResponse<AgendaEletivaSolicitacao> pesquisaAgendaEletivaSolicitacao(AgendaEletivaSolicitacaoFilter agendaEletivaSolicitacaoFilter) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Pageable pageable = paginationMapper.toPageable(agendaEletivaSolicitacaoFilter);
        Page<AgendaEletivaSolicitacaoEntity> pageResult = agendaEletivaSolicitacaoRepository.findAll(AgendaEletivaSolicitacaoRepository.filtraSolicitacao(agendaEletivaSolicitacaoFilter), pageable);
        List<AgendaEletivaSolicitacao> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaSolicitacao.class)).collect(Collectors.toList());
        for (AgendaEletivaSolicitacao registro : registros) {
            if (registro.getAgendaEletivaBloqueio() != null) {
                if (registro.getAgendaEletivaBloqueio().getDatas() != null && !registro.getAgendaEletivaBloqueio().getDatas().isEmpty()) {
                    LocalDate dataBloqueio = registro.getAgendaEletivaBloqueio().getDatas().get(0).getDataBloqueio();
                    registro.setPrazo(validaPrazo(registro.getCreatedAt(), dataBloqueio));
                }
            } else if (registro.getAgendaEletivaCancelamento() != null) {
                LocalDate dataCancelamento = registro.getAgendaEletivaCancelamento().getDataInicioCancelamento();
                registro.setPrazo(validaPrazo(registro.getCreatedAt(), dataCancelamento));
            }
        }
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public PaginationResponse<AgendaEletivaSolicitacaoAgendamento> pesquisaAgendaEletivaSolicitacaoAgendamento(AgendaEletivaSolicitacaoAgendamentoFilter pesquisaAgendaEletivaSolicitacao) {
        Pageable pageable = paginationMapper.toPageable(pesquisaAgendaEletivaSolicitacao);
        Page<AgendaEletivaSolicitacaoEntity> pageResult = agendaEletivaSolicitacaoAgendamentoRepository.findAll(AgendaEletivaSolicitacaoAgendamentoRepository.filtraSolicitacaoAgendamento(pesquisaAgendaEletivaSolicitacao), pageable);
        List<AgendaEletivaSolicitacaoAgendamento> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaSolicitacaoAgendamento.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public PaginationResponse<AgendaEletivaSolicitacaoGestor> pesquisaAgendaEletivaSolicitacaoGestor(AgendaEletivaSolicitacaoGestorFilter pesquisaAgendaEletivaSolicitacao) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Pageable pageable = paginationMapper.toPageable(pesquisaAgendaEletivaSolicitacao);
        Page<AgendaEletivaSolicitacaoEntity> pageResult = agendaEletivaSolicitacaoGestorRepository.findAll(AgendaEletivaSolicitacaoGestorRepository.filtraSolicitacaoGestor(pesquisaAgendaEletivaSolicitacao), pageable);
        List<AgendaEletivaSolicitacaoGestor> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaSolicitacaoGestor.class)).collect(Collectors.toList());
        for (AgendaEletivaSolicitacaoGestor registro : registros) {
            if (registro.getAgendaEletivaBloqueio() != null) {
                if (registro.getAgendaEletivaBloqueio().getDatas() != null && !registro.getAgendaEletivaBloqueio().getDatas().isEmpty()) {
                    LocalDate dataBloqueio = registro.getAgendaEletivaBloqueio().getDatas().get(0).getDataBloqueio();
                    registro.setPrazo(validaPrazo(registro.getCreatedAt(), dataBloqueio));
                }
            } else if (registro.getAgendaEletivaCancelamento() != null) {
                LocalDate dataCancelamento = registro.getAgendaEletivaCancelamento().getDataInicioCancelamento();
                registro.setPrazo(validaPrazo(registro.getCreatedAt(), dataCancelamento));
            }
        }
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public PaginationResponse<AgendaEletivaSolicitacaoAdministrador> pesquisaAgendaEletivaSolicitacaoAdministrador(AgendaEletivaSolicitacaoAdministradorFilter pesquisaAgendaEletivaSolicitacao) {
        Pageable pageable = paginationMapper.toPageable(pesquisaAgendaEletivaSolicitacao);
        Page<AgendaEletivaSolicitacaoEntity> pageResult = agendaEletivaSolicitacaoAdministradorRepository.findAll(AgendaEletivaSolicitacaoAdministradorRepository.filtraSolicitacaoAdministrador(pesquisaAgendaEletivaSolicitacao), pageable);
        List<AgendaEletivaSolicitacaoAdministrador> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, AgendaEletivaSolicitacaoAdministrador.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public String validaPrazo(Date createdAt, LocalDate dataSolicitacao) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        LocalDate dataCriacao = createdAt.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate dataAntecedencia = dataSolicitacao.minusDays(30);
        if (dataCriacao.compareTo(dataAntecedencia) <= 0) {
            return "Dentro do prazo";
        } else {
            return "Fora do prazo";
        }
    }

    public AgendaEletivaSolicitacao criaAgendaEletivaSolicitacao(AgendaEletivaSolicitacao agendaEletivaSolicitacao) {
        AgendaEletivaSolicitacaoEntity agendaEletivaSolicitacaoEntity = mapper.map(agendaEletivaSolicitacao, AgendaEletivaSolicitacaoEntity.class);
        // Gera um código aleatório para solicitação
        String codSolicitacao = UUID.randomUUID().toString();
        agendaEletivaSolicitacaoEntity.setCodSolicitacao(codSolicitacao);
        // Verifica se o tipo da solicitação é válida
        if (agendaEletivaSolicitacao.getTipoSolicitacao().equals(TipoSolicitacao.BLOQUEIO.getValue())) {
            // Cria a agenda eletiva bloqueio
            AgendaEletivaBloqueio agendaEletivaBloqueio = agendaEletivaBloqueioService.criaAgendaEletivaBloqueio(agendaEletivaSolicitacao.getAgendaEletivaBloqueio());
            AgendaEletivaBloqueioEntity agendaEletivaBloqueioEntity = mapper.map(agendaEletivaBloqueio, AgendaEletivaBloqueioEntity.class);
            agendaEletivaSolicitacaoEntity.setAgendaEletivaBloqueio(agendaEletivaBloqueioEntity);
        } else if (agendaEletivaSolicitacao.getTipoSolicitacao().equals(TipoSolicitacao.CANCELAMENTO.getValue())) {
            // Cria a agenda eletiva cancelamento
            AgendaEletivaCancelamento agendaEletivaCancelamento = agendaEletivaCancelamentoService.criaAgendaEletivaCancelamento(agendaEletivaSolicitacao.getAgendaEletivaCancelamento());
            AgendaEletivaCancelamentoEntity agendaEletivaCancelamentoEntity = mapper.map(agendaEletivaCancelamento, AgendaEletivaCancelamentoEntity.class);
            agendaEletivaSolicitacaoEntity.setAgendaEletivaCancelamento(agendaEletivaCancelamentoEntity);
        } else {
            throw new ModelException(AgendaEletivaSolicitacaoErrors.NON_EXISTENT_TYPE);
        }
        // Verifica se o código do status existe
        Optional<AgendaEletivaStatusEntity> agendaEletivaStatusExistente = agendaEletivaStatusService.statusExistente(agendaEletivaSolicitacao.getStatus().getCodStatus());
        agendaEletivaSolicitacaoEntity.setStatus(agendaEletivaStatusExistente.get());
        // Verifica se o código do turno existe
        Optional<AgendaEletivaTurno> turno = Optional.ofNullable(agendaEletivaSolicitacao.getTurno());
        if (turno != null && turno.isPresent()) {
            Optional<AgendaEletivaTurnoEntity> agendaEletivaTurnoExistente = agendaEletivaTurnoService.turnoExistente(agendaEletivaSolicitacao.getTurno().getCodTurno());
            agendaEletivaSolicitacaoEntity.setTurno(agendaEletivaTurnoExistente.get());
        } else {
            agendaEletivaSolicitacaoEntity.setTurno(null);
        }
        agendaEletivaSolicitacaoEntity = agendaEletivaSolicitacaoRepository.save(agendaEletivaSolicitacaoEntity);
        return mapper.map(agendaEletivaSolicitacaoEntity, AgendaEletivaSolicitacao.class);
    }

    public AgendaEletivaSolicitacao alteraAgendaEletivaSolicitacao(Long id, AgendaEletivaSolicitacao agendaEletivaSolicitacao) {
        // Verifica se o id da agenda eletiva solicitacao existe
        Optional<AgendaEletivaSolicitacaoEntity> agendaEletivaSolicitacaoExistente = agendaEletivaSolicitacaoRepository.findById(id);
        if (!agendaEletivaSolicitacaoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaSolicitacaoErrors.NOT_FOUND);
        }
        // Localiza o solicitacao para realizar a alteração
        AgendaEletivaSolicitacaoEntity agendaEletivaSolicitacaoEntity = agendaEletivaSolicitacaoExistente.get();
        // Verifica se o tipo da solicitação é válida
        if (agendaEletivaSolicitacao.getTipoSolicitacao().equals(TipoSolicitacao.BLOQUEIO.getValue())) {
            // Altera a agenda eletiva bloqueio
            AgendaEletivaBloqueio agendaEletivaBloqueio = agendaEletivaBloqueioService.alteraAgendaEletivaBloqueio(agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getId(), agendaEletivaSolicitacao.getAgendaEletivaBloqueio());
            AgendaEletivaBloqueioEntity agendaEletivaBloqueioEntity = mapper.map(agendaEletivaBloqueio, AgendaEletivaBloqueioEntity.class);
            agendaEletivaSolicitacaoEntity.setAgendaEletivaBloqueio(agendaEletivaBloqueioEntity);
        } else if (agendaEletivaSolicitacao.getTipoSolicitacao().equals(TipoSolicitacao.CANCELAMENTO.getValue())) {
            // Altera a agenda eletiva cancelamento
            AgendaEletivaCancelamento agendaEletivaCancelamento = agendaEletivaCancelamentoService.alteraAgendaEletivaCancelamento(agendaEletivaSolicitacao.getAgendaEletivaCancelamento().getId(), agendaEletivaSolicitacao.getAgendaEletivaCancelamento());
            AgendaEletivaCancelamentoEntity agendaEletivaCancelamentoEntity = mapper.map(agendaEletivaCancelamento, AgendaEletivaCancelamentoEntity.class);
            agendaEletivaSolicitacaoEntity.setAgendaEletivaCancelamento(agendaEletivaCancelamentoEntity);
        } else {
            throw new ModelException(AgendaEletivaSolicitacaoErrors.NON_EXISTENT_TYPE);
        }
        // Verifica se o código do status existe
        Optional<AgendaEletivaStatusEntity> agendaEletivaStatusExistente = agendaEletivaStatusService.statusExistente(agendaEletivaSolicitacao.getStatus().getCodStatus());
        agendaEletivaSolicitacaoEntity.setStatus(agendaEletivaStatusExistente.get());
        // Verifica se o código do turno existe
        Optional<AgendaEletivaTurno> turno = Optional.ofNullable(agendaEletivaSolicitacao.getTurno());
        if (turno != null && turno.isPresent()) {
            Optional<AgendaEletivaTurnoEntity> agendaEletivaTurnoExistente = agendaEletivaTurnoService.turnoExistente(agendaEletivaSolicitacao.getTurno().getCodTurno());
            agendaEletivaSolicitacaoEntity.setTurno(agendaEletivaTurnoExistente.get());
        } else {
            agendaEletivaSolicitacaoEntity.setTurno(null);
        }
        agendaEletivaSolicitacaoEntity.setTipoSolicitacao(agendaEletivaSolicitacao.getTipoSolicitacao());
        agendaEletivaSolicitacaoEntity.setCodEstabelecimento(agendaEletivaSolicitacao.getCodEstabelecimento());
        agendaEletivaSolicitacaoEntity.setNomeEstabelecimento(agendaEletivaSolicitacao.getNomeEstabelecimento());
        agendaEletivaSolicitacaoEntity.setCodEspecialidade(agendaEletivaSolicitacao.getCodEspecialidade());
        agendaEletivaSolicitacaoEntity.setNomeEspecialidade(agendaEletivaSolicitacao.getNomeEspecialidade());
        agendaEletivaSolicitacaoEntity.setCpfMedico(agendaEletivaSolicitacao.getCpfMedico());
        agendaEletivaSolicitacaoEntity.setNomeMedico(agendaEletivaSolicitacao.getNomeMedico());
        agendaEletivaSolicitacaoEntity.setNomeProCobertura(agendaEletivaSolicitacao.getNomeProCobertura());
        agendaEletivaSolicitacaoEntity = agendaEletivaSolicitacaoRepository.save(agendaEletivaSolicitacaoEntity);
        return mapper.map(agendaEletivaSolicitacaoEntity, AgendaEletivaSolicitacao.class);
    }

    public AgendaEletivaSolicitacao alteraStatusAgendaEletivaSolicitacao(Long id, Integer codStatus, String observacao) {
        // Verifica se o id da agenda eletiva solicitacao existe
        Optional<AgendaEletivaSolicitacaoEntity> agendaEletivaSolicitacaoExistente = agendaEletivaSolicitacaoRepository.findById(id);
        if (!agendaEletivaSolicitacaoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaSolicitacaoErrors.NOT_FOUND);
        }
        // Localiza o solicitacao para realizar a alteração
        AgendaEletivaSolicitacaoEntity agendaEletivaSolicitacaoEntity = agendaEletivaSolicitacaoExistente.get();
        // Verifica se o codStatus existe
        Optional<AgendaEletivaStatusEntity> agendaEletivaStatusExistente = agendaEletivaStatusService.statusExistente(codStatus);
        agendaEletivaSolicitacaoEntity.setStatus(agendaEletivaStatusExistente.get());
        agendaEletivaSolicitacaoEntity.setObservacao(observacao);
        // Verifica se o status é Aprovado ou Rejeitado
        if (codStatus == 2 || codStatus == 3) {
            // Salva a data da execução do Gestor
            agendaEletivaSolicitacaoEntity.setDataExecucaoGestor(LocalDate.now());
            // Se for Aprovado, monta e envia o email
            if (codStatus == 2) {
                Email email = montaEmail(mapper.map(agendaEletivaSolicitacaoEntity, AgendaEletivaSolicitacao.class));
                if (email.getTexto() != null) {
                    emailController.enviaEmail(email);
                }
            }
        } else if (codStatus == 4) {
            // Salva a data da execução do Administrador
            agendaEletivaSolicitacaoEntity.setDataExecucaoAdministrador(LocalDate.now());
        }
        agendaEletivaSolicitacaoEntity = agendaEletivaSolicitacaoRepository.save(agendaEletivaSolicitacaoEntity);
        return mapper.map(agendaEletivaSolicitacaoEntity, AgendaEletivaSolicitacao.class);
    }

    public Email montaEmail(AgendaEletivaSolicitacao agendaEletivaSolicitacao) {
        String corpoEmail = null;
        if (agendaEletivaSolicitacao.getTipoSolicitacao().equals(TipoSolicitacao.BLOQUEIO.getValue())) {
            try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(TEMPLATE_EMAIL_BLOQUEIO)) {
                corpoEmail = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
                corpoEmail = corpoEmail.replace("${TITULO}", tituloAgendaEletivaBloqueio);
                corpoEmail = corpoEmail.replace("${TIPO_SOLICITACAO}", agendaEletivaSolicitacao.getTipoSolicitacao());
                corpoEmail = corpoEmail.replace("${NOME_MEDICO}", agendaEletivaSolicitacao.getNomeMedico());
                corpoEmail = corpoEmail.replace("${NOME_ESTABELECIMENTO}", agendaEletivaSolicitacao.getNomeEstabelecimento());
                Optional<AgendaEletivaTurno> turno = Optional.ofNullable(agendaEletivaSolicitacao.getTurno());
                if (turno != null && turno.isPresent()) {
                    corpoEmail = corpoEmail.replace("${TURNO}", agendaEletivaSolicitacao.getTurno().getTurno());
                } else {
                    corpoEmail = corpoEmail.replace("${TURNO}", "-");
                }
                corpoEmail = corpoEmail.replace("${BLOQUEIO_TIPO}", agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getAgendaEletivaBloqueioTipo().getDescricao());
                if (agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getBloqueioTipoMotivo() != null) {
                    corpoEmail = corpoEmail.replace("${MOTIVO}", agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getBloqueioTipoMotivo());
                } else {
                    corpoEmail = corpoEmail.replace("${MOTIVO}", "-");
                }
                if (agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getAgendaExtra() != null) {
                    String agendaExtra;
                    if (agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getAgendaExtra() == true) {
                        agendaExtra = "Sim";
                    } else {
                        agendaExtra = "Não";
                    }
                    corpoEmail = corpoEmail.replace("${AGENDA_EXTRA}", agendaExtra);
                }
                if (agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getDataAgendaExtra() != null) {
                    corpoEmail = corpoEmail.replace("${DATA_AGENDA_EXTRA}", DateUtils.LocalDateToString(agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getDataAgendaExtra(), "dd/MM/yyyy"));
                } else {
                    corpoEmail = corpoEmail.replace("${DATA_AGENDA_EXTRA}", "-");
                }
                if (agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getHoraInicioAgendaExtra() != null) {
                    corpoEmail = corpoEmail.replace("${HORA_INICIO_AGENDA_EXTRA}", agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getHoraInicioAgendaExtra());
                } else {
                    corpoEmail = corpoEmail.replace("${HORA_INICIO_AGENDA_EXTRA}", "-");
                }
                if (agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getHoraFimAgendaExtra() != null) {
                    corpoEmail = corpoEmail.replace("${HORA_FIM_AGENDA_EXTRA}", agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getHoraFimAgendaExtra());
                } else {
                    corpoEmail = corpoEmail.replace("${HORA_FIM_AGENDA_EXTRA}", "-");
                }
                if (!agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getDatas().isEmpty()) {
                    corpoEmail = corpoEmail.replace("${DATA_BLOQUEIO}", agendaEletivaSolicitacao.getAgendaEletivaBloqueio().getDatas().stream()
                            .map(model -> DateUtils.LocalDateToString(model.getDataBloqueio(), "dd/MM/yyyy")).collect(Collectors.joining(" - ")));
                } else {
                    corpoEmail = corpoEmail.replace("${DATA_BLOQUEIO}", "-");
                }
                if (agendaEletivaSolicitacao.getNomeProCobertura() != null) {
                    corpoEmail = corpoEmail.replace("${NOME_PRO_COBERTURA}", agendaEletivaSolicitacao.getNomeProCobertura());
                } else {
                    corpoEmail = corpoEmail.replace("${NOME_PRO_COBERTURA}", "-");
                }
                if (agendaEletivaSolicitacao.getObservacao() != null) {
                    corpoEmail = corpoEmail.replace("${OBSERVACAO}", agendaEletivaSolicitacao.getObservacao());
                } else {
                    corpoEmail = corpoEmail.replace("${OBSERVACAO}", "-");
                }
                corpoEmail = corpoEmail.replace("${DATA_SOLICITACAO}", DateUtils.DateToString(agendaEletivaSolicitacao.getCreatedAt(), "dd/MM/yyyy"));
            } catch (Exception e) {
                throw new ModelException(AgendaEletivaSolicitacaoErrors.EMAIL_BODY_ERROR);
            }
        } else if (agendaEletivaSolicitacao.getTipoSolicitacao().equals(TipoSolicitacao.CANCELAMENTO.getValue())) {
            try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(TEMPLATE_EMAIL_CANCELAMENTO)) {
                corpoEmail = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
                corpoEmail = corpoEmail.replace("${TITULO}", tituloAgendaEletivaCancelamento);
                corpoEmail = corpoEmail.replace("${TIPO_SOLICITACAO}", agendaEletivaSolicitacao.getTipoSolicitacao());
                corpoEmail = corpoEmail.replace("${NOME_MEDICO}", agendaEletivaSolicitacao.getNomeMedico());
                corpoEmail = corpoEmail.replace("${NOME_ESTABELECIMENTO}", agendaEletivaSolicitacao.getNomeEstabelecimento());
                Optional<AgendaEletivaTurno> turno = Optional.ofNullable(agendaEletivaSolicitacao.getTurno());
                if (turno != null && turno.isPresent()) {
                    corpoEmail = corpoEmail.replace("${TURNO}", agendaEletivaSolicitacao.getTurno().getTurno());
                } else {
                    corpoEmail = corpoEmail.replace("${TURNO}", "-");
                }
                String cancelarAgenda;
                if (agendaEletivaSolicitacao.getAgendaEletivaCancelamento().getCancelarAgenda() == true) {
                    cancelarAgenda = "Sim";
                } else {
                    cancelarAgenda = "Não";
                }
                corpoEmail = corpoEmail.replace("${CANCELAR_AGENDA}", cancelarAgenda);
                corpoEmail = corpoEmail.replace("${MOTIVO}", agendaEletivaSolicitacao.getAgendaEletivaCancelamento().getMotivo());
                corpoEmail = corpoEmail.replace("${DATA_INICIO_CANCELAMENTO}", DateUtils.LocalDateToString(agendaEletivaSolicitacao.getAgendaEletivaCancelamento().getDataInicioCancelamento(), "dd/MM/yyyy"));
                if (agendaEletivaSolicitacao.getAgendaEletivaCancelamento().getDiaCancelamento() != null) {
                    corpoEmail = corpoEmail.replace("${DIA_CANCELAMENTO}", agendaEletivaSolicitacao.getAgendaEletivaCancelamento().getDiaCancelamento());
                } else {
                    corpoEmail = corpoEmail.replace("${DIA_CANCELAMENTO}", "-");
                }
                if (agendaEletivaSolicitacao.getNomeProCobertura() != null) {
                    corpoEmail = corpoEmail.replace("${NOME_PRO_COBERTURA}", agendaEletivaSolicitacao.getNomeProCobertura());
                } else {
                    corpoEmail = corpoEmail.replace("${NOME_PRO_COBERTURA}", "-");
                }
                if (agendaEletivaSolicitacao.getObservacao() != null) {
                    corpoEmail = corpoEmail.replace("${OBSERVACAO}", agendaEletivaSolicitacao.getObservacao());
                } else {
                    corpoEmail = corpoEmail.replace("${OBSERVACAO}", "-");
                }
                corpoEmail = corpoEmail.replace("${DATA_SOLICITACAO}", DateUtils.DateToString(agendaEletivaSolicitacao.getCreatedAt(), "dd/MM/yyyy"));
            } catch (Exception e) {
                throw new ModelException(AgendaEletivaSolicitacaoErrors.EMAIL_BODY_ERROR);
            }
        }
        Email email = new Email();
        email.setEmailDe(emailDe);
        email.setEmailPara(emailPara);
        email.setAssunto("Agenda Eletiva Notremedical");
        email.setTexto(corpoEmail);
        email.setTipo("Agenda Eletiva");
        return email;
    }

    public String excluiAgendaEletivaSolicitacao(Long id) {
        Optional<AgendaEletivaSolicitacaoEntity> agendaEletivaSolicitacaoExistente = agendaEletivaSolicitacaoRepository.findById(id);
        if (!agendaEletivaSolicitacaoExistente.isPresent()) {
            throw new ModelException(AgendaEletivaSolicitacaoErrors.NOT_FOUND);
        }
        try {
            agendaEletivaSolicitacaoRepository.deleteById(id);
        } catch (Exception e) {
            return "Erro ao excluir o solicitação. %s\n" + e;
        }
        return "Solicitação excluída com sucesso";
    }

}
