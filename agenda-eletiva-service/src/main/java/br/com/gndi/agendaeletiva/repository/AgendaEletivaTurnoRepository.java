package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaTurnoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgendaEletivaTurnoRepository extends JpaRepository<AgendaEletivaTurnoEntity, Long> {

    Optional<AgendaEletivaTurnoEntity> findById(Long idAgendaEletivaTurno);

    Optional<AgendaEletivaTurnoEntity> findByCodTurno(Integer codTurno);

}
