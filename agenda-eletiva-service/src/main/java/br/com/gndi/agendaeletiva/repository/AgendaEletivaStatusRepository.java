package br.com.gndi.agendaeletiva.repository;

import br.com.gndi.agendaeletiva.entity.AgendaEletivaStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgendaEletivaStatusRepository extends JpaRepository<AgendaEletivaStatusEntity, Long> {

    Optional<AgendaEletivaStatusEntity> findById(Long idAgendaEletivaStatus);

    Optional<AgendaEletivaStatusEntity> findByCodStatus(Integer codStatus);

}
