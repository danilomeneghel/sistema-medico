package br.com.gndi.agendaeletiva.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "agenda_eletiva_turno")
@Data
public class AgendaEletivaTurnoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codTurno;

    private String turno;

}
