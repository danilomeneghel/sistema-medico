package br.com.gndi.corpoclinico.controller;

import br.com.gndi.corpoclinico.model.CorpoClinico;
import br.com.gndi.corpoclinico.model.filter.CorpoClinicoFilter;
import br.com.gndi.corpoclinico.model.pagination.PaginationResponse;
import br.com.gndi.corpoclinico.service.CorpoClinicoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/dados-basicos")
@Api(value = "Dados Básicos", tags = "Dados Básicos do Corpo Clínico")
public class CorpoClinicoController {

    @Autowired
    private CorpoClinicoService corpoClinicoService;

    @GetMapping("{id}")
    public ResponseEntity<CorpoClinico> localizaCorpoClinico(@PathVariable Long id) {
        return new ResponseEntity<>(corpoClinicoService.localizaCorpoClinico(id), HttpStatus.OK);
    }

    @GetMapping
    public PaginationResponse<CorpoClinico> pesquisaCorpoClinico(@Valid @ModelAttribute CorpoClinicoFilter corpoClinicoFilter) {
        return corpoClinicoService.pesquisaCorpoClinico(corpoClinicoFilter);
    }

    @PostMapping
    public ResponseEntity<CorpoClinico> criaCorpoClinico(@Valid @RequestBody CorpoClinico corpoClinico) {
        return new ResponseEntity<>(corpoClinicoService.criaCorpoClinico(corpoClinico), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<CorpoClinico> alteraCorpoClinico(@PathVariable Long id, @Valid @RequestBody CorpoClinico corpoClinico) {
        return new ResponseEntity<>(corpoClinicoService.alteraCorpoClinico(id, corpoClinico), HttpStatus.OK);
    }

}
