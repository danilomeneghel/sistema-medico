package br.com.gndi.corpoclinico.service;

import br.com.gndi.corpoclinico.entity.ConselhoMedicoEntity;
import br.com.gndi.corpoclinico.exception.ModelException;
import br.com.gndi.corpoclinico.model.ConselhoMedico;
import br.com.gndi.corpoclinico.model.errors.ConselhoMedicoErrors;
import br.com.gndi.corpoclinico.repository.ConselhoMedicoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ConselhoMedicoService {

    @Autowired
    private ConselhoMedicoRepository conselhoMedicoRepository;

    private ModelMapper mapper = new ModelMapper();

    public ConselhoMedico localizaConselhoMedico(Long id) {
        Optional<ConselhoMedicoEntity> conselhoMedicoExistente = conselhoMedicoRepository.findById(id);
        if (!conselhoMedicoExistente.isPresent()) {
            throw new ModelException(ConselhoMedicoErrors.NOT_FOUND);
        }
        return mapper.map(conselhoMedicoExistente.get(), ConselhoMedico.class);
    }

    public List<ConselhoMedico> listaConselhoMedicos() {
        List<ConselhoMedicoEntity> listaConselhoMedicos = conselhoMedicoRepository.findAll();
        return listaConselhoMedicos.stream()
                .map(entity -> mapper.map(entity, ConselhoMedico.class)).collect(Collectors.toList());
    }

    public ConselhoMedico criaConselhoMedico(ConselhoMedico conselhoMedico) {
        Optional<ConselhoMedicoEntity> tipoExistente = conselhoMedicoRepository.findByTipoConselho(conselhoMedico.getTipoConselho());
        if (tipoExistente.isPresent()) {
            throw new ModelException(ConselhoMedicoErrors.DUPLICATED_RECORD);
        }
        ConselhoMedicoEntity conselhoMedicoEntity = mapper.map(conselhoMedico, ConselhoMedicoEntity.class);
        conselhoMedicoEntity = conselhoMedicoRepository.save(conselhoMedicoEntity);
        conselhoMedicoRepository.flush();
        return mapper.map(conselhoMedicoEntity, ConselhoMedico.class);
    }

    public ConselhoMedico alteraConselhoMedico(Long id, ConselhoMedico conselhoMedico) {
        Optional<ConselhoMedicoEntity> conselhoMedicoExistente = conselhoMedicoRepository.findById(id);
        if (!conselhoMedicoExistente.isPresent()) {
            throw new ModelException(ConselhoMedicoErrors.NOT_FOUND);
        }
        ConselhoMedicoEntity conselhoMedicoEntity = conselhoMedicoExistente.get();
        conselhoMedicoEntity.setTipoConselho(conselhoMedico.getTipoConselho());
        conselhoMedicoEntity = conselhoMedicoRepository.save(conselhoMedicoEntity);
        conselhoMedicoRepository.flush();
        return mapper.map(conselhoMedicoEntity, ConselhoMedico.class);
    }

}
