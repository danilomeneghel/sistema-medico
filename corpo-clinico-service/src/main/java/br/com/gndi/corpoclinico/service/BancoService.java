package br.com.gndi.corpoclinico.service;

import br.com.gndi.corpoclinico.entity.BancoEntity;
import br.com.gndi.corpoclinico.exception.ModelException;
import br.com.gndi.corpoclinico.model.Banco;
import br.com.gndi.corpoclinico.model.ConselhoMedico;
import br.com.gndi.corpoclinico.model.errors.BancoErrors;
import br.com.gndi.corpoclinico.repository.BancoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class BancoService {

    @Autowired
    private BancoRepository bancoRepository;

    private ModelMapper mapper = new ModelMapper();

    public Banco localizaBanco(Long id) {
        Optional<BancoEntity> bancoExistente = bancoRepository.findById(id);
        if (!bancoExistente.isPresent()) {
            throw new ModelException(BancoErrors.NOT_FOUND);
        }
        return mapper.map(bancoExistente.get(), Banco.class);
    }

    public List<Banco> listaBancos() {
        List<BancoEntity> listaBancos = bancoRepository.findAll();
        return listaBancos.stream()
                .map(entity -> mapper.map(entity, Banco.class)).collect(Collectors.toList());
    }

    public Banco criaBanco(Banco banco) {
        Optional<BancoEntity> tipoExistente = bancoRepository.findByNomeBanco(banco.getNomeBanco());
        if (tipoExistente.isPresent()) {
            throw new ModelException(BancoErrors.DUPLICATED_RECORD);
        }
        BancoEntity bancoEntity = mapper.map(banco, BancoEntity.class);
        bancoEntity = bancoRepository.save(bancoEntity);
        bancoRepository.flush();
        return mapper.map(bancoEntity, Banco.class);
    }

    public Banco alteraBanco(Long id, Banco banco) {
        Optional<BancoEntity> bancoExistente = bancoRepository.findById(id);
        if (!bancoExistente.isPresent()) {
            throw new ModelException(BancoErrors.NOT_FOUND);
        }
        BancoEntity bancoEntity = bancoExistente.get();
        bancoEntity.setNomeBanco(banco.getNomeBanco());
        bancoEntity = bancoRepository.save(bancoEntity);
        bancoRepository.flush();
        return mapper.map(bancoEntity, Banco.class);
    }

}
