package br.com.gndi.corpoclinico.repository;

import br.com.gndi.corpoclinico.entity.ConselhoMedicoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConselhoMedicoRepository extends JpaRepository<ConselhoMedicoEntity, Long> {

    Optional<ConselhoMedicoEntity> findById(Long id);

    Optional<ConselhoMedicoEntity> findByTipoConselho(String tipoConselho);

}
