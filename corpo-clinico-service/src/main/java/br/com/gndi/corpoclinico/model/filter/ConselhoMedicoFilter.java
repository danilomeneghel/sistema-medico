package br.com.gndi.corpoclinico.model.filter;

import br.com.gndi.corpoclinico.model.ConselhoMedico;
import br.com.gndi.corpoclinico.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class ConselhoMedicoFilter extends PaginationRequest<ConselhoMedico> {

    @ApiModelProperty(value = "Cod Conselho Médico", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codConselho;

    @ApiModelProperty(value = "Tipo Conselho Médico", example = "xxxxxxxxxxxxxxxx")
    private String tipoConselho;

}
