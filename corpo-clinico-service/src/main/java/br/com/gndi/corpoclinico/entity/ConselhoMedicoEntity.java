package br.com.gndi.corpoclinico.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "conselho_medico")
@Data
public class ConselhoMedicoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codConselho;

    private String tipoConselho;

}
