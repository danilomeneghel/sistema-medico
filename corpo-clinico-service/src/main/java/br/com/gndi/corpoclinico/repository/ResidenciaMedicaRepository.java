package br.com.gndi.corpoclinico.repository;

import br.com.gndi.corpoclinico.entity.ResidenciaMedicaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResidenciaMedicaRepository extends JpaRepository<ResidenciaMedicaEntity, Long> {

    Optional<ResidenciaMedicaEntity> findById(Long id);

    Optional<ResidenciaMedicaEntity> findByNomeResidencia(String nomeResidencia);

}
