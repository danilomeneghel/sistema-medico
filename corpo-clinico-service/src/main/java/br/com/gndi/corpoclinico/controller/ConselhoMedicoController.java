package br.com.gndi.corpoclinico.controller;

import br.com.gndi.corpoclinico.model.ConselhoMedico;
import br.com.gndi.corpoclinico.service.ConselhoMedicoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/conselho-medico")
@Api(value = "Conselho Médico", tags = "Tipos de Conselhos Médicos")
public class ConselhoMedicoController {

    @Autowired
    private ConselhoMedicoService conselhoMedicoService;

    @GetMapping("{id}")
    public ResponseEntity<ConselhoMedico> localizaConselhoMedico(@PathVariable Long id) {
        return new ResponseEntity<>(conselhoMedicoService.localizaConselhoMedico(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ConselhoMedico>> listaConselhoMedicos() {
        return new ResponseEntity<>(conselhoMedicoService.listaConselhoMedicos(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ConselhoMedico> criaConselhoMedico(@Valid @RequestBody ConselhoMedico conselhoMedico) {
        return new ResponseEntity<>(conselhoMedicoService.criaConselhoMedico(conselhoMedico), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<ConselhoMedico> alteraConselhoMedico(@PathVariable Long id, @Valid @RequestBody ConselhoMedico conselhoMedico) {
        return new ResponseEntity<>(conselhoMedicoService.alteraConselhoMedico(id, conselhoMedico), HttpStatus.OK);
    }
    
}
