package br.com.gndi.corpoclinico.model.filter;

import br.com.gndi.corpoclinico.model.Banco;
import br.com.gndi.corpoclinico.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class BancoFilter extends PaginationRequest<Banco> {

    @ApiModelProperty(value = "Cod Banco", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codBanco;

    @ApiModelProperty(value = "Nome Banco", example = "xxxxxxxxxxxxxxxx")
    private String nomeBanco;

}
