package br.com.gndi.corpoclinico.service;

import br.com.gndi.corpoclinico.entity.CorpoClinicoEntity;
import br.com.gndi.corpoclinico.exception.ModelException;
import br.com.gndi.corpoclinico.mapper.PaginationMapper;
import br.com.gndi.corpoclinico.model.CorpoClinico;
import br.com.gndi.corpoclinico.model.errors.CorpoClinicoErrors;
import br.com.gndi.corpoclinico.model.filter.CorpoClinicoFilter;
import br.com.gndi.corpoclinico.model.pagination.PaginationResponse;
import br.com.gndi.corpoclinico.repository.CorpoClinicoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CorpoClinicoService {

    @Autowired
    private CorpoClinicoRepository corpoClinicoRepository;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    private PaginationMapper paginationMapper;

    public CorpoClinico localizaCorpoClinico(Long id) {
        Optional<CorpoClinicoEntity> corpoClinicoExistente = corpoClinicoRepository.findById(id);
        if (!corpoClinicoExistente.isPresent()) {
            throw new ModelException(CorpoClinicoErrors.NOT_FOUND);
        }
        return mapper.map(corpoClinicoExistente.get(), CorpoClinico.class);
    }

    public PaginationResponse<CorpoClinico> pesquisaCorpoClinico(CorpoClinicoFilter corpoClinicoFilter) {
        Pageable pageable = paginationMapper.toPageable(corpoClinicoFilter);
        Page<CorpoClinicoEntity> pageResult = corpoClinicoRepository.findAll(CorpoClinicoRepository.filtraCorpoClinico(corpoClinicoFilter), pageable);
        List<CorpoClinico> registros = pageResult.getContent().stream().map(entity -> mapper.map(entity, CorpoClinico.class)).collect(Collectors.toList());
        return new PaginationResponse<>(registros, paginationMapper.toPaginationInfo(pageResult));
    }

    public CorpoClinico criaCorpoClinico(CorpoClinico corpoClinico) {
        Optional<CorpoClinicoEntity> rgExistente = corpoClinicoRepository.findByRg(corpoClinico.getRg());
        if (rgExistente.isPresent()) {
            throw new ModelException(CorpoClinicoErrors.DUPLICATED_RG);
        }
        Optional<CorpoClinicoEntity> cpfExistente = corpoClinicoRepository.findByCpf(corpoClinico.getCpf());
        if (cpfExistente.isPresent()) {
            throw new ModelException(CorpoClinicoErrors.DUPLICATED_CPF);
        }
        CorpoClinicoEntity corpoClinicoEntity = mapper.map(corpoClinico, CorpoClinicoEntity.class);
        corpoClinicoEntity = corpoClinicoRepository.save(corpoClinicoEntity);
        corpoClinicoRepository.flush();
        return mapper.map(corpoClinicoEntity, CorpoClinico.class);
    }

    public CorpoClinico alteraCorpoClinico(Long id, CorpoClinico corpoClinico) {
        Optional<CorpoClinicoEntity> corpoClinicoExistente = corpoClinicoRepository.findById(id);
        if (!corpoClinicoExistente.isPresent()) {
            throw new ModelException(CorpoClinicoErrors.NOT_FOUND);
        }
        CorpoClinicoEntity corpoClinicoEntity = corpoClinicoExistente.get();
        corpoClinicoEntity.setCpf(corpoClinico.getCpf());
        corpoClinicoEntity.setRg(corpoClinico.getRg());
        corpoClinicoEntity.setNome(corpoClinico.getNome());
        corpoClinicoEntity.setSexo(corpoClinico.getSexo());
        corpoClinicoEntity.setDataNascimento(corpoClinico.getDataNascimento());
        corpoClinicoEntity.setNacionalidade(corpoClinico.getNacionalidade());
        corpoClinicoEntity = corpoClinicoRepository.save(corpoClinicoEntity);
        corpoClinicoRepository.flush();
        return mapper.map(corpoClinicoEntity, CorpoClinico.class);
    }

}
