package br.com.gndi.corpoclinico.controller;

import br.com.gndi.corpoclinico.model.Banco;
import br.com.gndi.corpoclinico.service.BancoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/banco")
@Api(value = "Banco", tags = "Nomes dos Bancos")
public class BancoController {

    @Autowired
    private BancoService bancoService;

    @GetMapping("{id}")
    public ResponseEntity<Banco> localizaBanco(@PathVariable Long id) {
        return new ResponseEntity<>(bancoService.localizaBanco(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Banco>> listaBancos() {
        return new ResponseEntity<>(bancoService.listaBancos(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Banco> criaBanco(@Valid @RequestBody Banco banco) {
        return new ResponseEntity<>(bancoService.criaBanco(banco), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<Banco> alteraBanco(@PathVariable Long id, @Valid @RequestBody Banco banco) {
        return new ResponseEntity<>(bancoService.alteraBanco(id, banco), HttpStatus.OK);
    }
    
}
