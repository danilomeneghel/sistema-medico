package br.com.gndi.corpoclinico.model.filter;

import br.com.gndi.corpoclinico.model.ResidenciaMedica;
import br.com.gndi.corpoclinico.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class ResidenciaMedicaFilter extends PaginationRequest<ResidenciaMedica> {

    @ApiModelProperty(value = "Cod Residência Médica", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codResidencia;

    @ApiModelProperty(value = "Nome Residência Médica", example = "xxxxxxxxxxxxxxxx")
    private String nomeResidencia;

}
