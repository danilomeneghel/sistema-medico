package br.com.gndi.corpoclinico.service;

import br.com.gndi.corpoclinico.entity.ResidenciaMedicaEntity;
import br.com.gndi.corpoclinico.exception.ModelException;
import br.com.gndi.corpoclinico.model.ConselhoMedico;
import br.com.gndi.corpoclinico.model.ResidenciaMedica;
import br.com.gndi.corpoclinico.model.errors.ResidenciaMedicaErrors;
import br.com.gndi.corpoclinico.repository.ResidenciaMedicaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ResidenciaMedicaService {

    @Autowired
    private ResidenciaMedicaRepository residenciaMedicaRepository;

    private ModelMapper mapper = new ModelMapper();

    public ResidenciaMedica localizaResidenciaMedica(Long id) {
        Optional<ResidenciaMedicaEntity> residenciaMedicaExistente = residenciaMedicaRepository.findById(id);
        if (!residenciaMedicaExistente.isPresent()) {
            throw new ModelException(ResidenciaMedicaErrors.NOT_FOUND);
        }
        return mapper.map(residenciaMedicaExistente.get(), ResidenciaMedica.class);
    }

    public List<ResidenciaMedica> listaResidenciaMedicas() {
        List<ResidenciaMedicaEntity> listaResidenciaMedicas = residenciaMedicaRepository.findAll();
        return listaResidenciaMedicas.stream()
                .map(entity -> mapper.map(entity, ResidenciaMedica.class)).collect(Collectors.toList());
    }

    public ResidenciaMedica criaResidenciaMedica(ResidenciaMedica residenciaMedica) {
        Optional<ResidenciaMedicaEntity> nomeExistente = residenciaMedicaRepository.findByNomeResidencia(residenciaMedica.getNomeResidencia());
        if (nomeExistente.isPresent()) {
            throw new ModelException(ResidenciaMedicaErrors.DUPLICATED_RECORD);
        }
        ResidenciaMedicaEntity residenciaMedicaEntity = mapper.map(residenciaMedica, ResidenciaMedicaEntity.class);
        residenciaMedicaEntity = residenciaMedicaRepository.save(residenciaMedicaEntity);
        residenciaMedicaRepository.flush();
        return mapper.map(residenciaMedicaEntity, ResidenciaMedica.class);
    }

    public ResidenciaMedica alteraResidenciaMedica(Long id, ResidenciaMedica residenciaMedica) {
        Optional<ResidenciaMedicaEntity> residenciaMedicaExistente = residenciaMedicaRepository.findById(id);
        if (!residenciaMedicaExistente.isPresent()) {
            throw new ModelException(ResidenciaMedicaErrors.NOT_FOUND);
        }
        ResidenciaMedicaEntity residenciaMedicaEntity = residenciaMedicaExistente.get();
        residenciaMedicaEntity.setNomeResidencia(residenciaMedica.getNomeResidencia());
        residenciaMedicaEntity = residenciaMedicaRepository.save(residenciaMedicaEntity);
        residenciaMedicaRepository.flush();
        return mapper.map(residenciaMedicaEntity, ResidenciaMedica.class);
    }

}
