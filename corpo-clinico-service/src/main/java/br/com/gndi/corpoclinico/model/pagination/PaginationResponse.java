package br.com.gndi.corpoclinico.model.pagination;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@ApiModel
@Data
@AllArgsConstructor
public class PaginationResponse<T> {

    @ApiModelProperty(value = "Page content")
    private List<T> content;

    @ApiModelProperty(value = "Pagination info")
    private PaginationInfo pagination;

}
