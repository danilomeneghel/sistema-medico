package br.com.gndi.corpoclinico.model.errors;

import org.springframework.http.HttpStatus;

public class CorpoClinicoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Registro não encontrado");

    public static final ErrorModel DUPLICATED_RG = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "RG já cadastrado");

    public static final ErrorModel DUPLICATED_CPF = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "CPF já cadastrado");

}
