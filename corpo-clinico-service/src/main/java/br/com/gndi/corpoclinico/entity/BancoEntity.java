package br.com.gndi.corpoclinico.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "banco")
@Data
public class BancoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codBanco;

    private String nomeBanco;

}
