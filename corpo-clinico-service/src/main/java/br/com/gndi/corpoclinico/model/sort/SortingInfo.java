package br.com.gndi.corpoclinico.model.sort;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@ApiModel
@Data
@AllArgsConstructor
public class SortingInfo {

    @ApiModelProperty(value = "Sorting field", example = "name")
    private String field;

    @ApiModelProperty(value = "Sorting direction, one of: asc, desc", example = "asc")
    private String direction;

}
