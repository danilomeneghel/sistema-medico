package br.com.gndi.corpoclinico.repository;

import br.com.gndi.corpoclinico.entity.BancoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BancoRepository extends JpaRepository<BancoEntity, Long> {

    Optional<BancoEntity> findById(Long id);

    Optional<BancoEntity> findByNomeBanco(String nomeBanco);

}
