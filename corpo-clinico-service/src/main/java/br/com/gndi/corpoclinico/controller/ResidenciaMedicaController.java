package br.com.gndi.corpoclinico.controller;

import br.com.gndi.corpoclinico.model.ResidenciaMedica;
import br.com.gndi.corpoclinico.service.ResidenciaMedicaService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/residencia-medica")
@Api(value = "Residência Médica", tags = "Nomes das Residências Médicas")
public class ResidenciaMedicaController {

    @Autowired
    private ResidenciaMedicaService residenciaMedicaService;

    @GetMapping("{id}")
    public ResponseEntity<ResidenciaMedica> localizaResidenciaMedica(@PathVariable Long id) {
        return new ResponseEntity<>(residenciaMedicaService.localizaResidenciaMedica(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ResidenciaMedica>> listaResidenciaMedicas() {
        return new ResponseEntity<>(residenciaMedicaService.listaResidenciaMedicas(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ResidenciaMedica> criaResidenciaMedica(@Valid @RequestBody ResidenciaMedica residenciaMedica) {
        return new ResponseEntity<>(residenciaMedicaService.criaResidenciaMedica(residenciaMedica), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<ResidenciaMedica> alteraResidenciaMedica(@PathVariable Long id, @Valid @RequestBody ResidenciaMedica residenciaMedica) {
        return new ResponseEntity<>(residenciaMedicaService.alteraResidenciaMedica(id, residenciaMedica), HttpStatus.OK);
    }
    
}
