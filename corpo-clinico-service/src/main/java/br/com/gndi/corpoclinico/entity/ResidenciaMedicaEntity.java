package br.com.gndi.corpoclinico.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "residencia_medica")
@Data
public class ResidenciaMedicaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer codResidencia;

    private String nomeResidencia;

}
