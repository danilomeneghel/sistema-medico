package br.com.gndi.corpoclinico.repository;

import br.com.gndi.corpoclinico.entity.CorpoClinicoEntity;
import br.com.gndi.corpoclinico.model.CorpoClinico;
import br.com.gndi.corpoclinico.model.filter.CorpoClinicoFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface CorpoClinicoRepository extends JpaRepository<CorpoClinicoEntity, Long> {

    Optional<CorpoClinicoEntity> findById(Long id);

    Optional<CorpoClinicoEntity> findByNome(String nome);

    Optional<CorpoClinicoEntity> findByRg(String rg);

    Optional<CorpoClinicoEntity> findByCpf(String cpf);

    Page<CorpoClinicoEntity> findAll(Specification<CorpoClinico> specification, Pageable pageable);

    static Specification<CorpoClinico> filtraCorpoClinico(CorpoClinicoFilter filter) {
        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (filter.getNome() != null) {
                predicates.add(criteriaBuilder.like(root.get("nome"), filter.getNome()));
            }

            if (filter.getCpf() != null) {
                predicates.add(criteriaBuilder.equal(root.get("cpf"), filter.getCpf()));
            }

            if (filter.getRg() != null) {
                predicates.add(criteriaBuilder.equal(root.get("rg"), filter.getRg()));
            }

            if (filter.getSexo() != null) {
                predicates.add(criteriaBuilder.like(root.get("sexo"), filter.getSexo()));
            }

            if (filter.getNacionalidade() != null) {
                predicates.add(criteriaBuilder.equal(root.get("nacionalidade"), filter.getNacionalidade()));
            }

            if (filter.getDataNascimento() != null) {
                predicates.add(criteriaBuilder.equal(root.get("dataNascimento"), filter.getDataNascimento()));
            }

            query.distinct(true);

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
