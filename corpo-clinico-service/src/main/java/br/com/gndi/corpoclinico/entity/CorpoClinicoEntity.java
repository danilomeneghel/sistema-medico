package br.com.gndi.corpoclinico.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "corpo_clinico")
@Data
public class CorpoClinicoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    private String cpf;

    private String rg;

    private String sexo;

    private String nacionalidade;

    private LocalDate dataNascimento;

}
