package br.com.gndi.corpoclinico.model.pagination;

import br.com.gndi.corpoclinico.model.sort.SortingInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel
@Data
public class PaginationInfo {

    @ApiModelProperty(value = "Page number, starting at 0", example = "0")
    private Integer pageNumber;

    @ApiModelProperty(value = "Page size", example = "20")
    private Integer pageSize;

    @ApiModelProperty(value = "Content size", example = "20")
    private Integer contentSize;

    @ApiModelProperty(value = "Total elements", example = "100")
    private Long totalElements;

    @ApiModelProperty(value = "Total pages", example = "5")
    private Integer totalPages;

    @ApiModelProperty(value = "Has content", example = "true")
    private Boolean hasContent;

    @ApiModelProperty(value = "Has previous page", example = "false")
    private Boolean hasPreviousPage;

    @ApiModelProperty(value = "Has next page", example = "true")
    private Boolean hasNextPage;

    @ApiModelProperty(value = "Sorting info")
    private List<SortingInfo> sorting;

}
