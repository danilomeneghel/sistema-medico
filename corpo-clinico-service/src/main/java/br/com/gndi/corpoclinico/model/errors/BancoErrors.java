package br.com.gndi.corpoclinico.model.errors;

import org.springframework.http.HttpStatus;

public class BancoErrors {

    public static final ErrorModel NOT_FOUND = new ErrorModel(HttpStatus.NOT_FOUND.value(), "404001", "Banco não encontrado");

    public static final ErrorModel DUPLICATED_RECORD = new ErrorModel(HttpStatus.BAD_REQUEST.value(), "400001", "Banco já cadastrado");

}
