package br.com.gndi.corpoclinico.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel
@Data
public class ConselhoMedico {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Conselho Médico", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codConselho;

    @ApiModelProperty(value = "Tipo Conselho Médico", example = "xxxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String tipoConselho;

}
