package br.com.gndi.corpoclinico.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel
@Data
public class ResidenciaMedica {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Cod Residência Médica", example = "9")
    @NotNull(message = "Campo Obrigatório")
    private Integer codResidencia;

    @ApiModelProperty(value = "Nome Residência Médica", example = "xxxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nomeResidencia;

}
