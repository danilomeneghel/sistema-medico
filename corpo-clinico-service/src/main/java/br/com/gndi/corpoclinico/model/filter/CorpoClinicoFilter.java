package br.com.gndi.corpoclinico.model.filter;

import br.com.gndi.corpoclinico.model.CorpoClinico;
import br.com.gndi.corpoclinico.model.pagination.PaginationRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;

import java.time.LocalDate;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class CorpoClinicoFilter extends PaginationRequest<CorpoClinico> {

    @ApiModelProperty(value = "Nome", example = "xxxxxxxxxxxxxxxxxxxxxxxx")
    private String nome;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "RG", example = "9999999999")
    private String rg;

    @ApiModelProperty(value = "Sexo", example = "xxxxxxxxx")
    private String sexo;

    @ApiModelProperty(value = "Nacionalidade", example = "xxxxxxxxxx")
    private String nacionalidade;

    @ApiModelProperty(value = "Data Nascimento", example = "2001-01-01")
    private LocalDate dataNascimento;

}
