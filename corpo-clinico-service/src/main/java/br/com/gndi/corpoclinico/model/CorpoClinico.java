package br.com.gndi.corpoclinico.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@ApiModel
@Data
public class CorpoClinico {

    @ApiModelProperty(value = "ID", example = "9")
    private Long id;

    @ApiModelProperty(value = "Nome", example = "xxxxxxxxxxxxxxxxxxxxxxxx")
    @NotBlank(message = "Campo Obrigatório")
    private String nome;

    @ApiModelProperty(value = "CPF", example = "999.999.999-99")
    @NotBlank(message = "Campo Obrigatório")
    @CPF
    private String cpf;

    @ApiModelProperty(value = "RG", example = "9999999999")
    private String rg;

    @ApiModelProperty(value = "Sexo", example = "xxxxxxxxx")
    private String sexo;

    @ApiModelProperty(value = "Nacionalidade", example = "xxxxxxxxxx")
    private String nacionalidade;

    @ApiModelProperty(value = "Data Nascimento", example = "2001-01-01")
    private LocalDate dataNascimento;

}
